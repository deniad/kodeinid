  var config = {
    apiKey: "AIzaSyCnVIfc-Kf4dWBY4hSLI7F8g2JXYTMhRb8",
    authDomain: "kodein-1479046554109.firebaseapp.com",
    databaseURL: "https://kodein-1479046554109.firebaseio.com",
    storageBucket: "kodein-1479046554109.appspot.com",
    messagingSenderId: "616115798122"
  };
  firebase.initializeApp(config);

function finbook() {
  this.checkSetup();

  this.userPic = document.getElementById('user-pic');
  this.userName = document.getElementById('user-name');
  this.signInButton = document.getElementById('sign-in');
  this.signOutButton = document.getElementById('sign-out');
  this.signInSnackbar = document.getElementById('must-signin-snackbar');

  this.signOutButton.addEventListener('click', this.signOut.bind(this));
  this.signInButton.addEventListener('click', this.signIn.bind(this));

  this.initFirebase();
}


finbook.prototype.signIn = function() {
  var provider = new firebase.auth.GoogleAuthProvider();
  this.auth.signInWithPopup(provider); 

};

// Signs-out of Friendly Chat.
finbook.prototype.signOut = function() {
  // Sign out of Firebase.
  this.auth.signOut();
};

finbook.prototype.initFirebase = function() {
  this.auth = firebase.auth();
  this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this)); 
};



// Checks that the Firebase SDK has been correctly setup and configured.
finbook.prototype.checkSetup = function() {
  if (!window.firebase || !(firebase.app instanceof Function) || !window.config) {
    window.alert('You have not configured and imported the Firebase SDK. ' +
        'Make sure you go through the codelab setup instructions.');
  } else if (config.storageBucket === '') {
    window.alert('Your Firebase Storage bucket has not been enabled. Sorry about that. This is ' +
        'actually a Firebase bug that occurs rarely. ' +
        'Please go and re-generate the Firebase initialisation snippet (step 4 of the codelab) ' +
        'and make sure the storageBucket attribute is not empty. ' +
        'You may also need to visit the Storage tab and paste the name of your bucket which is ' +
        'displayed there.');
  }
};

window.onload = function() {
  window.finbook = new finbook();
};