<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class IbParser
{

    function __construct()
    {
        $this->conf['ip']       = json_decode( file_get_contents( 'http://myjsonip.appspot.com/' ) )->ip;
        $this->conf['time']     = time();
        $this->conf['path']     = dirname( __FILE__ );
    }


    function instantiate( $bank )
    {
        $class = $bank . 'Parser';
        $this->bank = new $class( $this->conf ) or trigger_error( 'Undefined parser: ' . $class, E_USER_ERROR );
    }


    function getBalance( $bank, $username, $password )
    {

        $this->instantiate( $bank );
        $this->bank->login( $username, $password );
        $balance = $this->bank->getBalance();
        $balance = number_format($balance,0,"",".");
        $this->bank->logout();
        return $balance;
    }
/*
    function getBNI( $bank, $username, $password )
    {

        $this->instantiate( $bank );
        $menu=$this->bank->login( $username, $password );
        $balance = $this->bank->getBalance();
        $balance = number_format($balance,0,"",".");
        $this->bank->logout();
        return $balance;
    }
*/
    function getTransactions( $bank, $username, $password )
    {

        $this->instantiate( $bank );
        $this->bank->login( $username, $password );
        $data['transactions'] = $this->bank->getTransactions();
        $saldo = number_format($this->bank->getBalance(),0,"",".");
        $data['balance'] = $saldo;
        $this->bank->logout();
        return $data;

    }

}




class BCAParser
{
    function __construct( $conf )
    {

        $this->conf = $conf;

        $d          = explode( '|', date( 'Y|m|d|H|i|s', $this->conf['time'] ) );
        $start      = mktime( $d[3], $d[4], $d[5], $d[1], ( 0 ), $d[0] );

        $this->post_time['end']['y'] = $d[0];
        $this->post_time['end']['m'] = $d[1];
        $this->post_time['end']['d'] = $d[2];
        $this->post_time['start']['y'] = date( 'Y', $start );
        $this->post_time['start']['m'] = date( 'm', $start );
        $this->post_time['start']['d'] = date( 'd', $start );
    }




    function curlexec()
    {
        curl_setopt( $this->ch, CURLOPT_RETURNTRANSFER, 1 );
        return curl_exec( $this->ch );
    }




    function login( $username, $password )
    {

        $this->ch = curl_init();
        $browser='Mozilla/5.0 (Linux; U; Android 2.3.7; en-us; Nexus One Build/GRK39F) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1';
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt( $this->ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Linux; U; Android 2.3.7; en-us; Nexus One Build/GRK39F) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1' );
        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/login.jsp' );
        curl_setopt( $this->ch, CURLOPT_COOKIEFILE, $this->conf['path'] . '/cookie' );
        curl_setopt( $this->ch, CURLOPT_COOKIEJAR, $this->conf['path'] . '/cookiejar' );

        $this->curlexec();

        $params = implode( '&', array( 'value(user_id)=' . $username, 'value(pswd)=' . $password, 'value(Submit)=LOGIN', 'value(actions)=login', 'value(user_ip)=' . $this->conf['ip'], 'user_ip=' . $this->conf['ip'],'value(browser_info)=' .$browser, 'value(mobile)=false') );

        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/authentication.do' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/login.jsp' );
        curl_setopt( $this->ch, CURLOPT_POSTFIELDS, $params );
        curl_setopt( $this->ch, CURLOPT_POST, 1 );

        $this->curlexec();
    }




    function logout()
    {
        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/authentication.do?value(actions)=logout' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/authentication.do?value(actions)=menu' );
        $this->curlexec();
        return curl_close( $this->ch );
    }




    function getBalance()
    {

        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/nav_bar_indo/account_information_menu.htm' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/authentication.do' );

        $this->curlexec();

        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/balanceinquiry.do' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/accountstmt.do?value(actions)=menu' );

        $src = $this->curlexec();

        $parse = explode( "<td align='right'><font size='1' color='#0000a7'><b>", $src );

        if ( empty( $parse[1] ) )
            return false;

        $parse = explode( '</td>', $parse[1] );

        if ( empty( $parse[0] ) )
            return false;

        $parse = str_replace( ',', '', $parse[0] );

        return ( is_numeric( $parse ) )? $parse: false;

    }




    function getTransactions()
    {

        //curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/accountstmt.do?value(actions)=menu' );
        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/nav_bar_indo/account_information_menu.htm');
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/authentication.do' );

        $this->curlexec();

        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/accountstmt.do?value(actions)=acct_stmt' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/accountstmt.do?value(actions)=menu' );
        
        //curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/accountstmt.do?value(actions)=acct_stmt' );
        //curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/nav_bar_indo/menu_bar.htm');

        //$this->curlexec();

        $params = implode( '&', array( 'r1=1', 'value(D1)=0', 'value(startDt)=' . $this->post_time['start']['d'], 'value(startMt)=' . $this->post_time['start']['m'], 'value(startYr)=' . $this->post_time['start']['y'],'value(endDt)=' . $this->post_time['end']['d'], 'value(endMt)=' . $this->post_time['end']['m'], 'value(endYr)=' . $this->post_time['end']['y'] ) );

        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/accountstmt.do?value(actions)=acctstmtview' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/accountstmt.do?value(actions)=acct_stmt' );
        curl_setopt( $this->ch, CURLOPT_POSTFIELDS, $params );
        curl_setopt( $this->ch, CURLOPT_POST, 1 );

        $src = $this->curlexec();

        $parse = explode( '<table width="100%" class="blue">', $src );

        if ( empty( $parse[1] ) )
            return false;

        $parse = explode( '</table>', $parse[1] );
        $parse = explode( '<tr', $parse[0] );
        $rows = array();

        foreach( $parse as $val )
            if ( substr( $val, 0, 8 ) == ' bgcolor' )
                $rows[] = $val;
        foreach( $rows as $key => $val )
        {
            $rows[$key]     = explode( '</td>', $val );
            $rows[$key][0]  = substr( $rows[$key][0], -5 );
            if ( stristr( $rows[$key][0], 'pend' ) ){
                $rows[$key][0] = 'PEND';
            }else{
                $rows[$key][0]  .='/'.date('Y');
            }
            $detail         = explode( "<td valign='top'>", $rows[$key][1] );
            $rows[$key][2]  = $detail[1];
            $rows[$key][1]  = explode( '<br>', $detail[0] );
            $nominal= explode('.',str_replace( ',', '', $rows[$key][1][count($rows[$key][1])-1] ));
            $rows[$key][3]  = $nominal[0];
            unset( $rows[$key][1][count($rows[$key][1])-1] );
            foreach( $rows[$key][1] as $k => $v )
                $rows[$key][1][$k] = trim( strip_tags( $v ) );
            $rows[$key][1] = implode( " ", $rows[$key][1] );
        }
        //print_r($rows);
        return ( !empty( $rows ) )? $rows: false;

    }
}

class MandiriParser
{
    function __construct( $conf )
    {

        $this->conf = $conf;

        $d          = explode( '|', date( 'Y|m|d|H|i|s', $this->conf['time'] ) );
        $start      = mktime( $d[3], $d[4], $d[5], $d[1], ( $d[2] - 19 ), $d[0] );

        $this->post_time['end']['y'] = $d[0];
        $this->post_time['end']['m'] = $d[1];
        $this->post_time['end']['d'] = $d[2];
        $this->post_time['start']['y'] = date( 'Y', $start );
        $this->post_time['start']['m'] = date( 'm', $start );
        $this->post_time['start']['d'] = date( 'd', $start );
    }




    function curlexec()
    {
        curl_setopt( $this->ch, CURLOPT_RETURNTRANSFER, 1 );
        return curl_exec( $this->ch );
    }




    function login( $username, $password )
    {

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt( $this->ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Linux; U; Android 2.3.7; en-us; Nexus One Build/GRK39F) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1' );
        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/Login.do?action=form' );
        curl_setopt( $this->ch, CURLOPT_COOKIEFILE, $this->conf['path'] . '/cookie' );
        curl_setopt( $this->ch, CURLOPT_COOKIEJAR, $this->conf['path'] . '/cookiejar' );

        $this->curlexec();

        $params = implode( '&', array( 'userID=' . $username, 'password=' . $password, 'action=result') );

        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/Login.do' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/Login.do?action=form');
        curl_setopt( $this->ch, CURLOPT_POSTFIELDS, $params );
        curl_setopt( $this->ch, CURLOPT_POST, 1 );

        $this->curlexec();
    }




    function logout()
    {
        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/Logout.do?action=result' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/common/menu.jsp' );
        $this->curlexec();
        return curl_close( $this->ch );
    }




    function getBalance()
    {

        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/AccountList.do?action=acclist' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/Redirect.do?action=forward' );

        $this->curlexec();

        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/AccountDetail.do?action=result' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/Redirect.do?action=forward' );

        $parse = $this->curlexec();
         $data = explode('<td height="25" width="304" id="accbal">',$parse);
         $data = explode('</td>',$data[1]);
         $data = str_replace(array('Rp.&nbsp;'),'',$data[0]);
         $data = explode(',',$data);
         $saldo = str_replace(array("\t","\r","\n"," ","."),'',$data[0]);

        return $saldo;

    }




    function getTransactions()
    {

        //menu mutasi rekening
        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/TrxHistoryInq.do?action=form' );

        // menu informasi rekening
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/Redirect.do?action=forward' ); 
        
        /*
        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/accountstmt.do?value(actions)=acct_stmt' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/accountstmt.do?value(actions)=menu' );
        */

        $parse=$this->curlexec();

        $data = explode( '<select name="fromAccountID">', $parse );
        $data = explode("<option value=",$data[1]);
        $data = explode('"',$data[2]);
        $fromAccountID=$data[1];
        $data = explode('>',$data[2]);
        $data = explode(' ',$data[1]);
        $norekening=$data[0];
        

        //echo "fromAccountID: $value_rek, $start_day/$start_month/$start_year - $end_day/$end_month/$end_year";
        $params = implode( '&', array( 'action=result', 'fromAccountID='.$fromAccountID , 'searchType=R', 'fromDay=' . $this->post_time['start']['d'], 'fromMonth=' . $this->post_time['start']['m'], 'fromYear=' . $this->post_time['start']['y'],'toDay=' . $this->post_time['end']['d'], 'toMonth=' . $this->post_time['end']['m'], 'toYear=' . $this->post_time['end']['y'] ) );

        //print_r($params);

        // ../TrxHistoryInq.do?action=form

        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/TrxHistoryInq.do' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/Redirect.do?action=forward' );
        curl_setopt( $this->ch, CURLOPT_POSTFIELDS, $params );
        curl_setopt( $this->ch, CURLOPT_POST, 1 );

         $parse = $this->curlexec();
         $data = explode('<table border="0" cellpadding="2" cellspacing="1" width="100%">',$parse);
         $data = explode('</table>',$data[1]);
         $data = explode('<tr height="25">',$data[0]);

         //detail nih
         $jmldata=count($data);

         $rows=array();
         $no=0;
         for($i=1;$i<$jmldata;$i++){
         $data1 = explode('<td height="25" class="tabledata" bgcolor="',$data[$i]); // row
         $rows[$i][1]=str_replace(array('#DDF2FA">','">','</td>'),'',$data1[1]);
         $datadb = explode('<td height="25" class="tabledata" align="right" bgcolor="',$data1[2]);
         $berita = str_replace(array('<br>','</td>'), ' ', $datadb[0]);
         $rows[$i][2]=str_replace(array('#DDF2FA">','">'),'',$berita);
         $data1 = explode('>',$datadb[1]);
         $debet =explode(',', str_replace(array('</td','.'), '', $data1[1]));
         $rows[$i][3]=$debet[0];
         $data1 = explode('>',$datadb[2]);      
         $kredit = explode(',', str_replace(array('</td','.'), '', $data1[1]));
         $rows[$i][4]=$kredit[0];      
        }
        //print_r($data1);
        return ( !empty( $rows ) )? $rows: false;

    }
}

class BNIParser
{
    function __construct( $conf )
    {

        $this->conf = $conf;

        $d          = explode( '|', date( 'Y|m|d|H|i|s', $this->conf['time'] ) );
        $start      = mktime( $d[3], $d[4], $d[5], $d[1], ( $d[2] - 7 ), $d[0] );

        $this->post_time['end']['y'] = $d[0];
        $this->post_time['end']['m'] = $d[1];
        $this->post_time['end']['d'] = $d[2];
        $this->post_time['start']['y'] = date( 'Y', $start );
        $this->post_time['start']['m'] = date( 'm', $start );
        $this->post_time['start']['d'] = date( 'd', $start );
    }




    function curlexec()
    {
        curl_setopt( $this->ch, CURLOPT_RETURNTRANSFER, 1 );
        return curl_exec( $this->ch );
    }




    function login( $username, $password )
    {

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt( $this->ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Linux; U; Android 2.3.7; en-us; Nexus One Build/GRK39F) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1' );
        curl_setopt( $this->ch, CURLOPT_URL, 'https://ibank.bni.co.id/MBAWeb/FMB' );
        curl_setopt( $this->ch, CURLOPT_COOKIEFILE, $this->conf['path'] . '/cookie' );
        curl_setopt( $this->ch, CURLOPT_COOKIEJAR, $this->conf['path'] . '/cookiejar' );

         $parse=$this->curlexec();
         $data = explode('<td id="RetailUser_td"  ><a id="RetailUser" class="lgnaln" href="',$parse);
         $url = explode('"',$data[1]);
         curl_setopt( $this->ch, CURLOPT_URL, $url[0] );
         $parse=$this->curlexec();
         //echo $parse;die;
         $url1= explode('?',$url[0]);
        $params = implode( '&', array( 'Num_Field_Err="Please enter digits only!"','Mand_Field_Err="Mandatory field is empty!"','CorpId=' . $username, 'PassWord=' . $password,'__AUTHENTICATE__=Login', 'CancelPage=HomePage.xml','USER_TYPE=1','MBLocale=bh','language=bh','AUTHENTICATION_REQUEST=True','__JS_ENCRYPT_KEY__=','JavaScriptEnabled=N','deviceID=','machineFingerPrint=','deviceType=','browserType=','uniqueURLStatus=disabled','imc_service_page=SignOnRetRq','Alignment=LEFT','page=SignOnRetRq','locale=en','PageName=Thin_SignOnRetRq.xml','formAction='.$url1,'mConnectUrl=FMB','serviceType=Dynamic') );
       // echo $url[0]."<br>";
       
        
        //echo $params;die;
        curl_setopt( $this->ch, CURLOPT_URL, $url1[0] );
        curl_setopt( $this->ch, CURLOPT_REFERER, $url[0]);
        curl_setopt( $this->ch, CURLOPT_POSTFIELDS, $params );
        curl_setopt( $this->ch, CURLOPT_POST, 1 );

        $parse=$this->curlexec();
        $data = explode('<td id="MBMenuList_td"  ><a id="MBMenuList" class="MainMenuStyle" href="',$parse);
        $data = explode('"',$data[1]);
        $menu_rek=$data[0];
        return $menu_rek;
    }




    function logout()
    {
        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/Logout.do?action=result' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/common/menu.jsp' );
        $this->curlexec();
        return curl_close( $this->ch );
    }




    function getBalance()
    {
        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/AccountList.do?action=acclist' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/Redirect.do?action=forward' );

        $this->curlexec();

        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/AccountDetail.do?action=result' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/Redirect.do?action=forward' );

        $parse = $this->curlexec();
         $data = explode('<td height="25" width="304" id="accbal">',$parse);
         $data = explode('</td>',$data[1]);
         $data = str_replace(array('Rp.&nbsp;'),'',$data[0]);
         $data = explode(',',$data);
         $saldo = str_replace(array("\t","\r","\n"," ","."),'',$data[0]);

        return $saldo;

    }




    function getTransactions()
    {

        //menu mutasi rekening
        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/TrxHistoryInq.do?action=form' );

        // menu informasi rekening
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/Redirect.do?action=forward' ); 
        
        /*
        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/accountstmt.do?value(actions)=acct_stmt' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/accountstmt.do?value(actions)=menu' );
        */

        $parse=$this->curlexec();

        $data = explode( '<select name="fromAccountID">', $parse );
        $data = explode("<option value=",$data[1]);
        $data = explode('"',$data[2]);
        $fromAccountID=$data[1];
        $data = explode('>',$data[2]);
        $data = explode(' ',$data[1]);
        $norekening=$data[0];
        

        //echo "fromAccountID: $value_rek, $start_day/$start_month/$start_year - $end_day/$end_month/$end_year";
        $params = implode( '&', array( 'action=result', 'fromAccountID='.$fromAccountID , 'searchType=R', 'fromDay=' . $this->post_time['start']['d'], 'fromMonth=' . $this->post_time['start']['m'], 'fromYear=' . $this->post_time['start']['y'],'toDay=' . $this->post_time['end']['d'], 'toMonth=' . $this->post_time['end']['m'], 'toYear=' . $this->post_time['end']['y'] ) );

        //print_r($params);

        // ../TrxHistoryInq.do?action=form

        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/TrxHistoryInq.do' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/Redirect.do?action=forward' );
        curl_setopt( $this->ch, CURLOPT_POSTFIELDS, $params );
        curl_setopt( $this->ch, CURLOPT_POST, 1 );

         $parse = $this->curlexec();
         $data = explode('<table border="0" cellpadding="2" cellspacing="1" width="100%">',$parse);
         $data = explode('</table>',$data[1]);
         $data = explode('<tr height="25">',$data[0]);

         //detail nih
         $jmldata=count($data);

         $rows=array();
         $no=0;
         for($i=1;$i<$jmldata;$i++){
         $data1 = explode('<td height="25" class="tabledata" bgcolor="',$data[$i]); // row
         $rows[$i][1]=str_replace(array('#DDF2FA">','">','</td>'),'',$data1[1]);
         $datadb = explode('<td height="25" class="tabledata" align="right" bgcolor="',$data1[2]);
         $berita = str_replace(array('<br>','</td>'), ' ', $datadb[0]);
         $rows[$i][2]=str_replace(array('#DDF2FA">','">'),'',$berita);
         $data1 = explode('>',$datadb[1]);
         $debet =explode(',', str_replace(array('</td','.'), '', $data1[1]));
         $rows[$i][3]=$debet[0];
         $data1 = explode('>',$datadb[2]);      
         $kredit = explode(',', str_replace(array('</td','.'), '', $data1[1]));
         $rows[$i][4]=$kredit[0];      
        }

        return ( !empty( $rows ) )? $rows: false;

    }
}

function cek_BCA($bank,$username,$password){
    $parserbca = new IbParser;
        
    if ( $data = $parserbca->getTransactions( $bank, $username, $password ) )
    {

        // echo '<pre>' . print_r( $transactions, true ) . '</pre>';
        $hasil="<table border=1 cellspacing=2 cellpadding=2>
                    <tr>                          
                        <td align=center>Date</td>
                        <td align=center>Detail</td>
                        <td align=center>Type</td>
                        <td align=center>Nominal</td>
                    </tr>";

        // loop
        $found=0;
        if($data['transactions']){
            foreach( $data['transactions'] as $transaction )
            {
                $tanggal    =$transaction[0];
                $detail     =$transaction[1];
                $type       =$transaction[2];
                $nominal    =$transaction[3];
                $nominal = number_format($transaction[3],0,"",".");
                $hasil.="
                        <tr>                            
                            <td>$transaction[0]</td>
                            <td>$transaction[1]</td>
                            <td>$transaction[2]</td>
                            <td>$nominal</td>
                        </tr>"; 
                $found++; 
                //echo $this->db->last_query();     
            }
            if($found<1){
                echo "Transaksi tidak ditemukan";
            }else{
                echo "</br> $found Transaksi ditemukan </br>";
            }
            $hasil.="</table>";
            echo $hasil;
            echo "saldo anda saat ini: ".$data['balance'];
        }
        else{
            echo "Username or password $bank invalid";                          
        }
    }

}

function cek_mandiri($bank,$user,$pass){
    $parser = new IbParser;
    
    // Ambil transaksi
    if ( $data = $parser->getTransactions( $bank, $user, $pass ) )
    {

        $hasil="<table border=1 cellspacing=2 cellpadding=2>
                    <tr>                          
                        <td align=center>Date</td>
                        <td align=center>Detail</td>
                        <td align=center>Type</td>
                        <td align=center>Kredit</td>
                    </tr>";

        // loop
        $no=1;
        $found=0;
        foreach( $data['transactions'] as $transaction )
        {
            $debet = number_format($transaction[3],0,"",".");
            $kredit = number_format($transaction[4],0,"",".");
            if($debet!=0){$type="DB";$nominal=$debet;}
            if($kredit!=0){$type="CR";$nominal=$kredit;}
                $hasil.="
                        <tr>                            
                            <td>$transaction[1]</td>
                            <td>$transaction[2]</td>
                            <td>$type</td>
                            <td>$nominal</td>
                        </tr>";
                $found++;
                      
            $no++;            
        }
        if($found<1){
            echo "Transaksi tidak ditemukan";
        }else{
            echo "</br> $found Transaksi ditemukan </br>";
        }
        $hasil.="</table>";
        echo $hasil;
        $saldo = $data['balance'];
        echo "saldo anda saaat ini: $saldo";
    }

}

function cek_saldo($bank,$user,$pass){    
    $parser = new IbParser;
    $data= $parser->getBalance( $bank, $user, $pass );
    return $data;
}

function cek_BNI($bank,$user,$pass){    
    $parser = new IbParser;
    $data= $parser->getBalance( $bank, 'Deniaditiya28', 'konichiwa28' );
    return $data;
}

/*
Cara pemanggilan fungsi
mutasi mandiri
    function mandiri(){     
        $this->load->library('ibParser'); // library bank parser
        $bank="Mandiri";
        $data=cek_mandiri($bank,$username,$password);
    }

mutasi BCA
function BCA(){    
        $this->load->library('ibParser'); // library bank parser    
        $bank="BCA";
        $data=cek_BCA($bank,$username,$password);
}

cek_saldo
function cek_saldo(){
        $this->load->library('ibParser'); // library bank parser
        $bank="BCA";
        $data=cek_saldo($bank,$username,$password);
        echo "saldo BCA Saat ini: $data";
}

	function notification(){
    	$this->load->library('aes'); // library kriptografi AES
    	$this->load->library('ibParser'); // library bank parser

    	$data_bank=$this->reservation_model->get_bank('BCA');
    	foreach($data_bank->result_array() as $cetak){
    		$username=kriptografi($cetak['username'],'deskripsi');
    		$password=kriptografi($cetak['password'],'deskripsi');
    	}
    	
    	$bank="BCA";
    	$data=get_transaction($bank,$username,$password);

    	$no=0;
        foreach( $data['transactions'] as $transaction )
        { $no++;

        	$nominal=number_format($transaction[3]);
        	$cetak=join('',explode(',',$nominal));

        	//CEK Di daftar payment dgn nominal sekian
        	$cek=$this->reservation_model->cek_transaksi($cetak,$transaction[1]);
        	
        	// jika ditemukan
        	if($cek->num_rows()>0){
        		//update date kalo ada trx dengan nominal dan detail yg sama dan status pend
        		foreach($cek->result_array() as $value1){
        			//jika statusnya pend dan data baru tidak pend, update data
        			if($value1['transaction_date']=='PEND' && $transaction[0] !='PEND'){
        				$this->reservation_model->update_trx($cetak, $transaction[1], $transaction[0]);
        			}
        		}
        	//jika tidak ditemukan (trx baru), insert data         		
        	}else{             		
        		
        		$date=date('Ymd');
	        	$payment=array(
	        			'bank'					=>$bank,
	        			'transaction_date'		=>$transaction[0],        			
	        			'transaction_detail'	=>$transaction[1],
	        			'transaction_type'		=>$transaction[2],
	        			'gross_amount'			=>$cetak,
	        			'date_create'			=>$date,
	        			'date_update'			=>$date
	        		);
	        	
	        	$pay_id=$this->reservation_model->insert_payment($payment);

	        	//cek apakah ada booking_code dengan nominal tsb dan dalam rentang 2 hari kebelakang
        		$cek_payment=$this->reservation_model->payment_detail($cetak);
        		//kalau ditemukan update status reserve dan payment
        		if($cek_payment->num_rows()>0){
        			foreach ($cek_payment->result_array() as $value) {     			
	        			$this->reservation_model->update_reserve($value['booking_code']);
	        			$this->reservation_model->update_data_payment($pay_id, $value['booking_code']);
	        		}	        		
        		}
	        	//echo json_encode($payment);
	        }
        }
        
        echo "parsing sukses </br>";
        //echo json_encode($payment);
    }
*/
