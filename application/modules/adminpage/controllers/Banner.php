<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
		parent::__construct();
		//load model-model yang dibutuhkan
		$this->load->model('banner_model');
		$this->load->model('user_model');
		$this->load->helper('login');
		if(!cek_login()){
			//set status offline
			$this->user_model->update_status_login('offline');
			redirect(base_url("adminpage/login"));			
		}
	}	
	 
	public function index()
	{
		$data['konten']= "banner_konten"; // nama file view konten		
		$data['header']="Kelola Banner";
		$this->breadcrumbs->push('Home', 'adminpage');
		$this->breadcrumbs->push('Kelola Banner', 'banner');
		$data['banner']=$this->banner_model->daftar_banner("all");
		$data['jumlah']=$this->banner_model->daftar_banner("Y")->num_rows();
		$this->load->view('media_admin',$data);	// media_admin adalah template
	}
	
	public function reload()
	{
		$banner=$this->banner_model->daftar_banner("all");
		$jumlah=$this->banner_model->daftar_banner("Y")->num_rows();
		
		$no=1;
		foreach($banner->result_array() as $cetak){
			$gambar_desktop=base_url("banner/$cetak[gambar_desktop]");
			$gambar_mobile=base_url("banner/$cetak[gambar_mobile]");
			echo "
			<tr>
			  <td align=center>$no</td>
			  <td >$cetak[nama_banner]</td>
			  <td align=center>
				<a href='$gambar_desktop'  data-fancybox-group='banner_image' class='btn btn-default fancybox-buttons'><i class='fa fa-desktop'></i> &nbsp desktop</a>
				<a href='$gambar_mobile'  data-fancybox-group='banner_image' class='btn btn-default fancybox-buttons'><i class='glyphicon glyphicon-phone'></i> &nbsp mobile</a>
			  </td>
			  <td align=center>$cetak[publish]</td>
			  <td align=center>
				<button type=button class='btn btn-primary' style='margin-right:5px' onclick='isi_form_edit($cetak[id_banner])'><i class='fa fa-pencil'></i> &nbsp Edit</button>
				<button type=button class='btn btn-danger' onclick=\"delete_banner('$cetak[id_banner]','$jumlah','$cetak[publish]','$cetak[nama_banner]')\"><i class='fa fa-trash'></i> &nbsp Hapus</button>
			  </td>
			</tr>
			";
			$no++;
		}
	}
	
	public function delete_banner()
	{
		$id_banner=$this->input->post('id');		
		
		//hapus image di folder produk
		$banner=$this->banner_model->banner_by_id($id_banner);		
		foreach($banner->result_array() as $cetak){
			$gambar_desktop=$cetak['gambar_desktop'];	
			$gambar_mobile=$cetak['gambar_mobile'];	
		}
		if($gambar_desktop!=''){
			unlink("banner/".$gambar_desktop);
		}
		if($gambar_mobile!=''){
			unlink("banner/".$gambar_mobile);
		}
		
		
		//hapus dari database
		$this->banner_model->delete_banner($id_banner);
		
		$data['status']=true;
		//output to json format
		echo json_encode($data);		
	}
	
	public function simpan()
	{
		$action=$this->input->post('action');
		$id_banner=$this->input->post('id_banner');
		$gambar=$this->validate_banner($action);
		// siapkan data produk
		$tgl_skrg = date("Ymd");
		
		$publish=$this->input->post('publish');
		if($publish=='on'){$publish='Y';}else{$publish='N';}
		
		$data = array(
			'nama_banner' => ucwords($this->input->post('nama_banner')),
			//'gambar' => $gambar,
			'link' => $this->input->post('link_banner'),
			'publish' => $publish
		);
		
		if(!$_FILES['gambar_desktop']['error']){
			$data['gambar_desktop']=$gambar['gambar_desktop'];
		}
		
		if(!$_FILES['gambar_mobile']['error']){
			$data['gambar_mobile']=$gambar['gambar_mobile'];
		}
		
		if($action=='tambah'){
			$data['created']=$tgl_skrg;
			$data['updated']=$tgl_skrg;
			//simpan ke database
			$this->banner_model->insert($data);
		}
		elseif($action=='edit')
		{
			$data['updated']=$tgl_skrg; 
			//simpan ke database
			$this->banner_model->update($id_banner,$data);
		}
		
		
		$data['status']=TRUE;
		// feedback data ke view via ajax
	    echo json_encode($data);
	}
	
	public function banner_by_id(){
		$id=$this->input->post('id');
		$banner=$this->banner_model->banner_by_id($id);
		foreach($banner->result_array() as $cetak){
			$data = array(
			'id_banner' => $cetak['id_banner'], 
			'nama_banner' => $cetak['nama_banner'], 
			'publish' => $cetak['publish'], 
			'link_banner' => $cetak['link']
			);			
		}
		echo json_encode($data);
	}
	
	//fungsi validasi
	private function validate_banner($action)
	{		
		$this->load->library('form_validation');
		// menentukan rules validasi ubah profil
		$this->form_validation->set_rules('nama_banner', 'Nama Banner', 'required|min_length[3]|max_length[35]|alpha_numeric_spaces');
		$this->form_validation->set_rules('link_banner', 'Link banner', 'required|min_length[3]|valid_url');
				
		//set message error
		$this->form_validation->set_message('min_length', '{field} minimal {param} karakter.');
		$this->form_validation->set_message('max_length', '{field} maksimal {param} karakter.');
		$this->form_validation->set_message('required', '{field} tidak boleh kosong.');
		$this->form_validation->set_message('alpha_numeric_spaces', '{field} hanya boleh diisi huruf dan angka');
		$this->form_validation->set_message('valid_url', '{field} tidak valid.');
		//$this->form_validation->set_message('is_natural', '{field} hanya boleh diisi angka.');
		
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
	
		if($this->form_validation->run()==FALSE){			
			if(form_error('nama_banner')){
				$data['inputerror'][] = 'nama_banner';
				$data['error_string'][] = form_error('nama_banner');
				$data['status'] = FALSE;
			}
			if(form_error('link_banner')){
				$data['inputerror'][] = 'link_banner';
				$data['error_string'][] = form_error('link_banner');
				$data['status'] = FALSE;
			}
		}
		

		$url = $this->input->post('link_banner');

		if (!preg_match('/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i', $url)) {
			$data['inputerror'][] = 'link_banner';
			$data['status'] = FALSE;
			$data['error_string'][] =" $url Pastikan berawalan http:// dan link banner valid ";
			echo json_encode($data);
			exit();
		}
		
		//validasi gambar desktop
		if(!$_FILES['gambar_desktop']['error'])
		{
	
		//validasi gambar
			$config['upload_path']          = 'banner/';
			$config['encrypt_name'] 		= TRUE;
            $config['allowed_types']        = 'bmp|jpg|png';
            $config['max_size']             = 1024;
			$config['overwrite']            = false;
			$config['min_width']            = 1272;
            $config['max_width']            = 1440;
            $config['max_height']           = 588;
			$config['min_height']           = 520;
			$config['file_ext_tolower']  	= TRUE;

            $this->load->library('upload',$config);		
			$this->upload->initialize($config);

			
			$gambar="";			
            if ( ! $this->upload->do_upload('gambar_desktop')){
                $data['inputerror'][] = 'gambar_desktop';
				$data['status'] = FALSE;
				$data['error_string'][] = $this->upload->display_errors();				
            }else{
				$upload_data = $this->upload->data(); 
				$gambar_desktop =   $upload_data['file_name'];
				
				if($action=='edit'){ // kalau actionnya edit hapus gambar lama				
					$id_banner=$this->input->post('id_banner');
					$kategori=$this->banner_model->banner_by_id($id_banner);	
					foreach ($kategori->result_array() as $cetak){
						$gambar_old=$cetak['gambar_desktop'];
					}
					unlink("banner/".$gambar_old);					
				}				
			}
			
			if($data['status'] === FALSE)
			{
				echo json_encode($data);
				exit();
			}
			$data['gambar_desktop']=$gambar_desktop;	
		}
		
		//validasi gambar mobile
		if(!$_FILES['gambar_mobile']['error'])
		{	
			//validasi gambar
			$config1['upload_path']          = 'banner/';
			$config1['encrypt_name'] 		= TRUE;
            $config1['allowed_types']        = 'bmp|jpg|png';
            $config1['max_size']             = 1024;
			$config1['overwrite']            = false;
            $config1['max_width']            = 408;
			$config1['min_width']            = 408;
            $config1['max_height']           = 408;			
            $config1['min_height']           = 408;
			$config1['file_ext_tolower']  	= TRUE;

            $this->load->library('upload',$config1);
			$this->upload->initialize($config1);
			
			$gambar="";			
            if ( ! $this->upload->do_upload('gambar_mobile')){
                $data['inputerror'][] = 'gambar_mobile';
				$data['status'] = FALSE;
				$data['error_string'][] = $this->upload->display_errors();				
            }else{
				$upload_data = $this->upload->data(); 
				$gambar_mobile =   $upload_data['file_name'];
				
				if($action=='edit'){ // kalau actionnya edit hapus gambar lama				
					$id_banner=$this->input->post('id_banner');
					$kategori=$this->banner_model->banner_by_id($id_banner);	
					foreach ($kategori->result_array() as $cetak){
						$gambar_old=$cetak['gambar_mobile'];
					}
					unlink("banner/".$gambar_old);					
				}				
			}
			
			if($data['status'] === FALSE)
			{
				echo json_encode($data);
				exit();
			}
			$data['gambar_mobile']=$gambar_mobile;	
		}
		return $data;
	}
}
