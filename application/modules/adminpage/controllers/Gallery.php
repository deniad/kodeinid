<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
		parent::__construct();
		//load model-model yang dibutuhkan
		$this->load->model('user_model');	
		$this->load->model('gallery_model');
		$this->load->helper('login');
		if(!cek_login()){
			//set status offline
			$this->user_model->update_status_login('offline');
			redirect(base_url("adminpage/login"));			
		}
	}	
	 
	public function index()
	{
		$data['konten']= "gallery"; // nama file view konten		
		$data['header']="Kelola Gallery";
		$data['gallery']=$this->gallery_model->daftar_gallery();
		$this->breadcrumbs->push('Home', 'adminpage');
		$this->breadcrumbs->push('Gallery Homepage', 'adminpage/gallery');
		$this->load->view('media_admin',$data);	// media_admin adalah template
	}
	
	public function gallery_by_id(){
		$id=$this->input->post('id');
		$gallery=$this->gallery_model->gallery_by_id($id);
		foreach($gallery->result_array() as $cetak){
			$data = array(
			'id_gallery' => $cetak['id_gallery'], 
			'judul' => $cetak['judul'], 
			'link_gallery' => $cetak['link_gallery'] 
			);			
		}
		//pesan rules
		if($id=='1' || $id=='2' || $id=='6' || $id=='7'){
			$data['rules']="
				<li><b>Ukuran Dimensi gambar: 610 x 320 pixel</b></li>
			";
		}
		elseif($id=='3'){
			$data['rules']="
				<li><b>Ukuran Dimensi gambar Desktop: 290 x 665 pixel</b></li>
				<li><b>Ukuran Dimensi gambar Mobile: 610 x 320 pixel</b></li>
			";
		}
		elseif($id=='4'){
			$data['rules']="
				<li><b>Ukuran Dimensi gambar Desktop: 290 x 320 pixel</b></li>
				<li><b>Ukuran Dimensi gambar Mobile: 610 x 320 pixel</b></li>
			";
		}
		echo json_encode($data);
	}
	
	public function simpan()
	{
		$id_gallery=$this->input->post('id_gallery');
		$gambar=$this->validate_gallery($id_gallery);
		// siapkan data produk
		$tgl_skrg = date("Ymd");
		
		$data = array(
			'updated' => $tgl_skrg,
			'link_gallery' => $this->input->post('link_gallery')
		);
		
		if(!$_FILES['gambar_desktop']['error']){
			$data['gambar_desktop']=$gambar['gambar_desktop'];
		}
		
		
		if(!$_FILES['gambar_mobile']['error']){
			$data['gambar_mobile']=$gambar['gambar_mobile'];
		}
		
		
		//$data['updated']=$tgl_skrg; 
		//simpan ke database
		$this->gallery_model->update($id_gallery,$data);
		
		
		$data['status']=TRUE;
		// feedback data ke view via ajax
	    echo json_encode($data);
	}
	
	public function reload()
	{
		$gallery=$this->gallery_model->daftar_gallery();
		
		foreach($gallery->result_array() as $cetak){					
			$mobile="";
			if($cetak['gambar_desktop']==''){
				$gambar_desktop=base_url("banner/no-image.jpg");
			}else{
				$gambar_desktop=base_url("banner/$cetak[gambar_desktop]");
			}
			
			if ($cetak['id_gallery']=='4' || $cetak['id_gallery']=='3'){														
				if($cetak['gambar_mobile']==''){
					$gambar_mobile=base_url("banner/no-image.jpg");
				}else{
					$gambar_mobile=base_url("banner/$cetak[gambar_mobile]");
				}
				$mobile="<a href='$gambar_mobile'  data-fancybox-group='banner_mobile' class='btn btn-default fancybox-buttons'><i class='glyphicon glyphicon-phone'></i> &nbsp mobile</a>";
			}
			echo "
			<tr>
				<td>$cetak[judul]</td>
				<td align=center>
					<a href='$gambar_desktop'  data-fancybox-group='banner_image' class='btn btn-default fancybox-buttons'><i class='fa fa-desktop'></i> &nbsp desktop</a>
					$mobile
				</td>
				<td align=center>
					<button type=button class='btn btn-primary' style='margin-right:5px' onclick='isi_form_edit($cetak[id_gallery])'><i class='fa fa-pencil'></i> &nbsp Edit</button>
				</td>							
			</tr>
			";
		}
	}
	
	//fungsi validasi
	private function validate_gallery($id_gallery)
	{	
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
		
		$url = $this->input->post('link_gallery');

		if (!preg_match('/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i', $url)) {
			$data['inputerror'][] = 'link_gallery';
			$data['status'] = FALSE;
			$data['error_string'][] =" $url Pastikan berawalan http:// dan link banner valid ";
			echo json_encode($data);
			exit();
		}
		
		//validasi gambar desktop
		if(!$_FILES['gambar_desktop']['error'])
		{
	
		//validasi gambar
			$config['upload_path']          = 'banner/';
			$config['encrypt_name'] 		= TRUE;
            $config['allowed_types']        = 'bmp|jpg|png|jpeg';
            $config['max_size']             = 1024;
			$config['overwrite']            = false;
			$config['file_ext_tolower']  	= TRUE;
			
			if($id_gallery=='1' || $id_gallery=='2' || $id_gallery=='6' || $id_gallery=='7'){
				$config['min_width']            = 610;
				$config['max_width']            = 610;
				$config['max_height']           = 320;
				$config['min_height']           = 320;
			}
			if($id_gallery=='3'){
				$config['min_width']            = 290;
				$config['max_width']            = 290;
				$config['max_height']           = 665;
				$config['min_height']           = 665;
			}
			if($id_gallery=='4'){
				$config['min_width']            = 290;
				$config['max_width']            = 290;
				$config['max_height']           = 320;
				$config['min_height']           = 320;
			}
			

            $this->load->library('upload',$config);		
			$this->upload->initialize($config);

			$gambar="";			
            if ( ! $this->upload->do_upload('gambar_desktop')){
                $data['inputerror'][] = 'gambar_desktop';
				$data['status'] = FALSE;
				$data['error_string'][] = $this->upload->display_errors();				
            }else{
				$upload_data = $this->upload->data(); 
				$gambar_desktop =   $upload_data['file_name'];
				$id_gallery=$this->input->post('id_gallery');
				$gallery=$this->gallery_model->gallery_by_id($id_gallery);	
				foreach ($gallery->result_array() as $cetak){
					$gambar_old=$cetak['gambar_desktop'];
				}
				unlink("banner/".$gambar_old);				
			}
			
			if($data['status'] === FALSE)
			{
				echo json_encode($data);
				exit();
			}
			$data['gambar_desktop']=$gambar_desktop;	
		}
		
		//validasi gambar mobile
		if(!$_FILES['gambar_mobile']['error'])
		{	
			//validasi gambar
			$config1['upload_path']          = 'banner/';
			$config1['encrypt_name'] 		= TRUE;
            $config1['allowed_types']        = 'bmp|jpg|png|jpeg';
            $config1['max_size']             = 1024;
			$config1['overwrite']            = false;
            $config1['max_width']            = 610;
			$config1['min_width']            = 610;
            $config1['max_height']           = 320;			
            $config1['min_height']           = 320;
			$config1['file_ext_tolower']  	= TRUE;

            $this->load->library('upload',$config1);
			$this->upload->initialize($config1);
			
			$gambar="";			
            if ( ! $this->upload->do_upload('gambar_mobile')){
                $data['inputerror'][] = 'gambar_mobile';
				$data['status'] = FALSE;
				$data['error_string'][] = $this->upload->display_errors();				
            }else{
				$upload_data = $this->upload->data(); 
				$gambar_mobile =   $upload_data['file_name'];			
				$id_gallery=$this->input->post('id_gallery');
				$gallery=$this->gallery_model->gallery_by_id($id_gallery);		
				foreach ($gallery->result_array() as $cetak){
					$gambar_old=$cetak['gambar_mobile'];
				}
				unlink("banner/".$gambar_old);				
			}
			
			if($data['status'] === FALSE)
			{
				echo json_encode($data);
				exit();
			}
			$data['gambar_mobile']=$gambar_mobile;	
		}
		
		return $data;
	}
	
}
