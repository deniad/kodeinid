<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_homepage extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
		parent::__construct();
		//load model-model yang dibutuhkan
		$this->load->model('user_model');	
		$this->load->model('produk_model');	
		$this->load->model('kategori_model');
		$this->load->helper('login');
		if(!cek_login()){
			//set status offline
			$this->user_model->update_status_login('offline');
			redirect(base_url("login"));			
		}
	}	
	 
	public function index()
	{
		$data['konten']= "produk_konten"; // nama file view konten		
		$data['header']="Kelola Produk";
		$this->breadcrumbs->push('Home', 'adminpage');
		$this->breadcrumbs->push('Kelola Produk', 'produk');
		$this->load->view('media_admin',$data);	// media_admin adalah template
	}
	
	public function tambah_produk()
	{
		$data['konten']= "tambah_produk_konten"; // nama file view konten		
		$data['header']="Tambah Produk";
		$data['kategori']=$this->kategori_model->daftar_kategori();
		$this->breadcrumbs->push('Home', 'adminpage');
		$this->breadcrumbs->push('Kelola Produk', 'produk');
		$this->breadcrumbs->push('Tambah Produk', 'tambah_produk');
		$this->load->view('media_admin',$data);	// media_admin adalah template
	}
	
	public function simpan_produk()
	{
		$this->validate_produk();		
		$publish=$this->input->post('publish');
		if($publish=='on'){$publish='Y';}else{$publish='N';}
		$tgl_skrg = date("Ymd");
		// siapkan data produk
		$data = array(
			'pcode' => $this->input->post('pcode'),
			'nama_produk' => $this->input->post('nama_produk'),
			'kategori' => $this->input->post('kategori'),
            'publish'=>$publish,
			'kandungan' => $this->input->post('kandungan'),
			'carapakai' => $this->input->post('carapakai'),
			'gambar' => $gambar,
			'foto2' => $foto2,
			'foto3' => $foto3,
			'deskripsi' => $this->input->post('deskripsi'),
			'created' => $tgl_skrg
        );	
		
		//simpan data melalui model
		$this->produk_model->insert_produk($data);
		
		//siapkan data feedback yang akan ditampilkan
		$data = array(
			'status' => TRUE 
        );
		// feedback data ke view via ajax
	    echo json_encode($data);
	}
		
		public function do_upload()
        {
            $config['upload_path']          = '../banner/';
			$config['encrypt_name'] 		= TRUE;
            $config['allowed_types']        = 'bmp|jpg|png';
            $config['max_size']             = 2048;
			$config['overwrite']            = false;
            $config['max_width']            = 0;
            $config['max_height']           = 0;
			$config['file_ext_tolower']  	= TRUE;

            $this->load->library('upload',$config);
			$this->upload->initialize($config);
			$pesan="";
			
            if ( ! $this->upload->do_upload('gambar'))
            {
                $error = array('error' => $this->upload->display_errors());
                $pesan.= $error['error'];
            }
			$upload_data = $this->upload->data(); 
			$gambar_name =   $upload_data['file_name'];
			
			if ( ! $this->upload->do_upload('foto2'))
            {
                $error = array('error' => $this->upload->display_errors());
                $pesan.= $error['error'];
            }
			
			$upload_data = $this->upload->data(); 
			$foto2_name =   $upload_data['file_name'];
			
			if ( ! $this->upload->do_upload('foto3'))
            {
                $error = array('error' => $this->upload->display_errors());
               $pesan.= $error['error'];
            }
			
			$upload_data = $this->upload->data(); 
			$foto3_name =   $upload_data['file_name'];	
			
			echo "$gambar_name, $foto2_name, $foto3_name, $pesan";
			
        }
	
	//fungsi validasi
	private function validate_produk()
	{		
		$this->load->library('form_validation');
		// menentukan rules validasi ubah profil
		$this->form_validation->set_rules('nama_produk', 'Nama Produk', 'required|min_length[3]|max_length[25]|alpha_numeric_spaces');
		$this->form_validation->set_rules('pcode', 'Product Code', 'min_length[3]|max_length[10]|required|is_natural');
		$this->form_validation->set_rules('kategori', 'Kategori Produk', 'required');
		$this->form_validation->set_rules('kandungan', 'Ingredients', 'required|min_length[3]');
		$this->form_validation->set_rules('carapakai', 'Cara Pakai', 'required|min_length[3]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi Produk', 'required|min_length[3]');
				
		//set message error
		$this->form_validation->set_message('min_length', '{field} minimal {param} karakter.');
		$this->form_validation->set_message('max_length', '{field} maksimal {param} karakter.');
		$this->form_validation->set_message('required', '{field} tidak boleh kosong.');
		$this->form_validation->set_message('alpha_numeric_spaces', '{field} hanya boleh diisi huruf dan angka');
		$this->form_validation->set_message('is_natural', '{field} hanya boleh diisi angka.');
		
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
	
		if($this->form_validation->run()==FALSE){			
			if(form_error('nama_produk')){
				$data['inputerror'][] = 'nama_produk';
				$data['error_string'][] = form_error('nama_produk');
				$data['status'] = FALSE;
			}
			if(form_error('pcode')){
				$data['inputerror'][] = 'pcode';
				$data['error_string'][] = form_error('pcode');
				$data['status'] = FALSE;
			}
			if(form_error('kategori')){
				$data['inputerror'][] = 'kategori';
				$data['error_string'][] = form_error('kategori');
				$data['status'] = FALSE;
			}
			if(form_error('kandungan')){
				$data['inputerror'][] = 'kandungan';
				$data['error_string'][] = form_error('kandungan');
				$data['status'] = FALSE;
			}
			if(form_error('carapakai')){
				$data['inputerror'][] = 'carapakai';
				$data['error_string'][] = form_error('carapakai');
				$data['status'] = FALSE;
			}
			if(form_error('deskripsi')){
				$data['inputerror'][] = 'deskripsi';
				$data['error_string'][] = form_error('deskripsi');
				$data['status'] = FALSE;
			}
		}
		
		//validasi gambar
			$config['upload_path']          = '../produk/';
			$config['encrypt_name'] 		= TRUE;
            $config['allowed_types']        = 'bmp|jpg|png';
            $config['max_size']             = 2048;
			$config['overwrite']            = false;
            $config['max_width']            = 0;
            $config['max_height']           = 0;
			$config['file_ext_tolower']  	= TRUE;

            $this->load->library('upload',$config);
			$this->upload->initialize($config);

            if ( ! $this->upload->do_upload('gambar')){
                $data['inputerror'][] = 'gambar';
				$data['status'] = FALSE;
            }else{
				$upload_data = $this->upload->data(); 
				$gambar =   $upload_data['file_name'];
			}
			
			if ( ! $this->upload->do_upload('foto2')){
                $data['inputerror'][] = 'foto2';
				$data['status'] = FALSE;
            }else{
				$upload_data = $this->upload->data(); 
				$foto2 =   $upload_data['file_name'];
			}
			
			if ( ! $this->upload->do_upload('foto3')){
                $data['inputerror'][] = 'foto3';				
				$data['status'] = FALSE;
            }else{
				$upload_data = $this->upload->data(); 
				$foto3 =   $upload_data['file_name'];
			}
			$data['error_string'][] = $this->upload->display_errors();
		
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
	
}
