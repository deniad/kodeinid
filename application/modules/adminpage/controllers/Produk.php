<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
		parent::__construct();
		//load model-model yang dibutuhkan
		$this->load->model('user_model');	
		$this->load->model('produk_model');	
		$this->load->model('subkategori_model');
		$this->load->helper('login');
		if(!cek_login()){
			//set status offline
			$this->user_model->update_status_login('offline');
			redirect(base_url("adminpage/login"));			
		}
	}	
	 
	public function index()
	{
		$data['konten']= "produk_konten"; // nama file view konten		
		$data['header']="Kelola Produk";
		$this->breadcrumbs->push('Home', 'adminpage');
		$this->breadcrumbs->push('Kelola Produk', 'produk');
		$this->load->view('media_admin',$data);	// media_admin adalah template
	}
	
	public function produk_list()
	{
		$list = $this->produk_model->produk_list();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $produk) {
			$no++;
			$row = array();
			$row[] = $produk->pcode;
			$row[] = $produk->nama_produk;
			$row[] = $produk->nama_kategori;
			$row[] = $produk->nama_subkategori;
			$row[] = $produk->viewer;
			$row[] = $produk->publish;

			//add html for action																						
			$row[] = '
					<a class="btn btn-sm btn-success" href="javascript:void(0)" title="Detail" onclick="detail_produk('."'".$produk->id_produk."'".')" style="margin-right:5px;"><i class="fa fa-search"></i></a>
					<a class="btn btn-sm btn-primary" href="edit_produk/'.$produk->id_produk.'" title="Edit"  style="margin-right:5px;"><i class="fa fa-pencil"></i></a>
					<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_produk('."'".$produk->id_produk."','".$produk->nama_produk."'".')"><i class="fa fa-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->produk_model->count_all(),
						"recordsFiltered" => $this->produk_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	public function tambah_produk()
	{
		$data['konten']= "tambah_produk_konten"; // nama file view konten		
		$data['header']="Tambah Produk";
		$data['subkategori']=$this->subkategori_model->daftar_subkategori();
		$this->breadcrumbs->push('Home', 'adminpage');
		$this->breadcrumbs->push('Kelola Produk', 'produk');
		$this->breadcrumbs->push('Tambah Produk', 'tambah_produk');
		$this->load->view('media_admin',$data);	// media_admin adalah template
	}
	
	public function edit_produk($id_produk)
	{
		$data['konten']= "edit_produk_konten"; // nama file view konten		
		$data['header']="Edit Produk";
		$data['cek']=$this->produk_model->produk_by_id($id_produk)->num_rows();
		
		if($data['cek']>0){
			$data['produk']=$this->produk_model->produk_by_id($id_produk);
			$data['kategori']=$this->subkategori_model->daftar_subkategori();
		}
		
		$this->breadcrumbs->push('Home', 'adminpage');
		$this->breadcrumbs->push('Kelola Produk', 'produk');
		$this->breadcrumbs->push('Edit Produk', 'edit_produk');
		$this->load->view('media_admin',$data);	// media_admin adalah template
	}
	
	public function ajax_edit()
	{
		$id_produk=$this->input->post('id');
		$data=$this->produk_model->produk_by_id($id_produk)->row();
		
		echo json_encode($data);
	}
	
	public function ajax_detail()
	{
		$id_produk=$this->input->post('id');
		$produk=$this->produk_model->produk_by_id($id_produk);
		foreach($produk->result() as $cetak){
			$gambar=$cetak->gambar;
			$foto2=$cetak->foto2;
			$foto3=$cetak->foto3;
			$nama_produk=$cetak->nama_produk;
			$kandungan=$cetak->kandungan;
			$carapakai=$cetak->cara_pakai;
			$nama_kategori=$cetak->nama_kategori;
			$deskripsi=$cetak->deskripsi_produk;
		}
		
		echo "<h3 align=center style='margin-bottom:20px;color:red;'>$nama_produk</h3>";
	?>
		<table class='table table-bordered' style="font-size:large">
			<tr>
				<td align=center class=active> Primary </td>
				<td align=center class=active> Foto 2 </td>
				<td align=center class=active> Foto 3 </td>
			</tr>		
			<tr>
				<td align=center> <a href="<?php echo  base_url("produk/$gambar")?>" data-fancybox-group="editproduk" data-title="Gambar Primary" class="fancybox-buttons"><img class="img-thumbnail" src="<?php echo  base_url("produk/$gambar")?>" width=150px /> </a></td>
				<td align=center> <a href="<?php echo  base_url("produk/$foto2")?>" data-fancybox-group="editproduk" data-title="Foto 2" class="fancybox-buttons"><img class="img-thumbnail" src="<?php echo  base_url("produk/$foto2")?>" width=150px /> </a></td>
				<td align=center> <a href="<?php echo  base_url("produk/$foto3")?>" data-fancybox-group="editproduk" data-title="Foto 3" class="fancybox-buttons"><img class="img-thumbnail" src="<?php echo  base_url("produk/$foto3")?>" width=150px /> </a></td>
			</tr>
		</table>
		
            <dl class="dl-horizontal">
                <dt>Kategori</dt>
					<dd><?php echo $nama_kategori; ?></dd>
                <dt>Kandungan</dt>
					<dd><?php echo $kandungan; ?></dd>
                <dt>Cara Pakai</dt>
					<dd><?php echo $carapakai; ?></dd>
                <dt>Deskripsi</dt>
					<dd><?php echo $deskripsi; ?></dd>
            </dl>
	<?php	
	}
	
	public function delete_produk()
	{
		$id_produk=$this->input->post('id');
		
		//hapus image di folder produk
		$produk=$this->produk_model->produk_by_id($id_produk);
		$file="";		
		
		foreach($produk->result_array() as $cetak){
			unlink("produk/".$cetak['gambar']);
			unlink("produk/".$cetak['foto2']);
			unlink("produk/".$cetak['foto3']);
		}
		$this->produk_model->delete_produk($id_produk);
		
		$data = array(
					"status" => true
				);
		//output to json format
		echo json_encode($data);
	}
		
	public function simpan_produk()
	{
		$this->validate_produk();
		$produk=$this->validate_gambar();			
		$publish=$this->input->post('publish');
		if($publish=='on'){$publish='Y';}else{$publish='N';}
		$tgl_skrg = date("Ymd");
		
		// siapkan data produk
		$data = array(
			'pcode' => $this->input->post('pcode'),
			'nama_produk' => ucwords($this->input->post('nama_produk')),
			'id_subkategori' => $this->input->post('kategori'),
            'publish'=>$publish,
			'kandungan' => $this->input->post('kandungan'),
			'cara_pakai' => $this->input->post('carapakai'),
			'gambar' => $produk['gambar'],
			'foto2' => $produk['foto2'],
			'foto3' => $produk['foto3'],
			'deskripsi_produk' => $this->input->post('deskripsi'),
			'created' => $tgl_skrg
        );	
		
		//simpan data melalui model
		$this->produk_model->insert_produk($data);
		
		//siapkan data feedback yang akan ditampilkan
		$data = array(
			'status' => TRUE 
        );
		// feedback data ke view via ajax
	    echo json_encode($data);
	}
	
	public function simpan_update_produk()
	{	
		$data['status']=TRUE;
		$this->validate_produk();		
		$publish=$this->input->post('publish');
		$id=$this->input->post('id_produk');
		if($publish=='on'){$publish='Y';}else{$publish='N';}
		$tgl_skrg = date("Ymd");
		
		
		//validasi gambar
			$config['upload_path']          = 'produk/';
			$config['encrypt_name'] 		= TRUE;
            $config['allowed_types']        = 'bmp|jpg|png';
            $config['max_size']             = 2048;
			$config['overwrite']            = false;
            $config['max_width']            = 0;
            $config['max_height']           = 0;
			$config['file_ext_tolower']  	= TRUE;

            $this->load->library('upload',$config);
			
			$gambar="";
			$foto2="";
			$foto3="";
			
			$produk=$this->produk_model->produk_by_id($id);
			foreach ($produk->result_array() as $cetak){
				$gambar_old=$cetak['gambar'];
				$foto2_old=$cetak['foto2'];
				$foto3_old=$cetak['foto3'];
			}
			
			if(!$_FILES['gambar']['error']){
				if ( ! $this->upload->do_upload('gambar')){
                $data['inputerror'][] = 'gambar';
				$data['status'] = FALSE;
				}else{
					$upload_data = $this->upload->data(); 
					$gambar =   $upload_data['file_name'];
					unlink("produk/".$gambar_old); // delete gambar lama difolder
				}
			}
			
            if(!$_FILES['foto2']['error']){
				if ( ! $this->upload->do_upload('foto2')){
                $data['inputerror'][] = 'foto2';
				$data['status'] = FALSE;
				}else{
					$upload_data = $this->upload->data(); 
					$foto2 =   $upload_data['file_name'];
					unlink("produk/".$foto2_old); // delete gambar lama difolder
				}
			}
			if(!$_FILES['foto3']['error']){
				if ( ! $this->upload->do_upload('foto3')){
                $data['inputerror'][] = 'foto3';
				$data['status'] = FALSE;
				}else{
					$upload_data = $this->upload->data(); 
					$foto3 =   $upload_data['file_name'];
					unlink("produk/".$foto3_old); // delete gambar lama difolder
				}
			}
			$data['error_string'][] = $this->upload->display_errors();
			
			if($data['status'] === FALSE)
			{
				echo json_encode($data);
				exit();
			}
		
		
		// siapkan data produk
		$data = array(
			'pcode' => $this->input->post('pcode'),
			'nama_produk' => ucwords($this->input->post('nama_produk')) ,
			'id_subkategori' => $this->input->post('kategori'),
            'publish'=>$publish,
			'kandungan' => $this->input->post('kandungan'),
			'cara_pakai' => $this->input->post('carapakai'),						
			'deskripsi_produk' => $this->input->post('deskripsi'),
			'updated' => $tgl_skrg
        );

		if($gambar!=""){
			$data['gambar']=$gambar;
		}
		if($foto2!=""){
			$data['foto2']=$foto2;
		}
		if($foto3!=""){
			$data['foto3']=$foto3;
		}
		
		//simpan data melalui model		
		$this->produk_model->update_produk($id,$data);
		
		/*siapkan data feedback yang akan ditampilkan
		$data = array(
			'status' => TRUE 
        );
		*/
		
		$data['status']=TRUE;
		
		// feedback data ke view via ajax
	    echo json_encode($data);
	}
			
	//fungsi validasi
	private function validate_produk()
	{		
		$this->load->library('form_validation');
		// menentukan rules validasi ubah profil
		$this->form_validation->set_rules('nama_produk', 'Nama Produk', 'required|min_length[3]|max_length[35]');
		$this->form_validation->set_rules('pcode', 'Product Code', 'min_length[3]|max_length[10]|required|is_natural');
		$this->form_validation->set_rules('kandungan', 'Ingredients', 'required|min_length[3]');
		$this->form_validation->set_rules('carapakai', 'Cara Pakai', 'required|min_length[3]');
		$this->form_validation->set_rules('kategori', 'Cara Pakai', 'required');
				
		//set message error
		$this->form_validation->set_message('min_length', '{field} minimal {param} karakter.');
		$this->form_validation->set_message('max_length', '{field} maksimal {param} karakter.');
		$this->form_validation->set_message('required', '{field} tidak boleh kosong.');
		//$this->form_validation->set_message('alpha_numeric_spaces', '{field} hanya boleh diisi huruf dan angka');
		$this->form_validation->set_message('is_natural', '{field} hanya boleh diisi angka.');
		
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
	
		if($this->form_validation->run()==FALSE){			
			if(form_error('nama_produk')){
				$data['inputerror'][] = 'nama_produk';
				$data['error_string'][] = form_error('nama_produk');
				$data['status'] = FALSE;
			}
			if(form_error('pcode')){
				$data['inputerror'][] = 'pcode';
				$data['error_string'][] = form_error('pcode');
				$data['status'] = FALSE;
			}
			if(form_error('kandungan')){
				$data['inputerror'][] = 'kandungan';
				$data['error_string'][] = form_error('kandungan');
				$data['status'] = FALSE;
			}
			if(form_error('carapakai')){
				$data['inputerror'][] = 'carapakai';
				$data['error_string'][] = form_error('carapakai');
				$data['status'] = FALSE;
			}
			if(form_error('kategori')){
				$data['inputerror'][] = 'kategori';
				$data['error_string'][] = form_error('kategori');
				$data['status'] = FALSE;
			}
		}
		
			
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}		
	}
	
	private function validate_gambar()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
		
			//validasi gambar
			$config['upload_path']          = 'produk/';
			$config['encrypt_name'] 		= TRUE;
            $config['allowed_types']        = 'bmp|jpg|png';
            $config['max_size']             = 2048;
			$config['overwrite']            = false;
            $config['max_width']            = 0;
            $config['max_height']           = 0;
			$config['file_ext_tolower']  	= TRUE;

            $this->load->library('upload',$config);

            if ( ! $this->upload->do_upload('gambar')){
                $data['inputerror'][] = 'gambar';
				$data['status'] = FALSE;
            }else{
				$upload_data = $this->upload->data(); 
				$gambar =   $upload_data['file_name'];
			}
			
			if ( ! $this->upload->do_upload('foto2')){
                $data['inputerror'][] = 'foto2';
				$data['status'] = FALSE;
            }else{
				$upload_data = $this->upload->data(); 
				$foto2 =   $upload_data['file_name'];
			}
			
			if ( ! $this->upload->do_upload('foto3')){
                $data['inputerror'][] = 'foto3';				
				$data['status'] = FALSE;
            }else{
				$upload_data = $this->upload->data(); 
				$foto3 =   $upload_data['file_name'];
			}
			$data['error_string'][] = $this->upload->display_errors();
			
			if($data['status'] === FALSE)
			{
				echo json_encode($data);
				exit();
			}
			
			return array('gambar' => $gambar, 'foto2' => $foto2, 'foto3' => $foto3);
	}
}
