<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
		parent::__construct();
		//load model-model yang dibutuhkan
		$this->load->model('user_model');
		$this->load->model('kategori_model');
		$this->load->model('subkategori_model');
		$this->load->helper('login');
		if(!cek_login()){
			//set status offline
			$this->user_model->update_status_login('offline');
			redirect(base_url("adminpage/login"));			
		}
	}	
	 
	public function index()
	{
		$data['konten']= "kategori_konten"; // nama file view konten		
		$data['header']="Kelola Kategori Produk";
		$this->breadcrumbs->push('Home', 'adminpage');
		$this->breadcrumbs->push('Kelola Kategori', 'kategori');
		$data['kategori']=$this->kategori_model->daftar_kategori_detail();
		$data['subkategori']=$this->subkategori_model->daftar_subkategori_detail();
		$this->load->view('media_admin',$data);	// media_admin adalah template
	}
	
	public function reload_subkategori()
	{
		$subkategori=$this->subkategori_model->daftar_subkategori_detail();
		$no=1;
		foreach($subkategori->result_array() as $cetak){
			$gambar=base_url("kategori/$cetak[gambar]");
		echo "
			<tr>
			  <td align=center>$no</td>
			  <td >$cetak[nama]</td>							  
			  <td align=center><a href='$gambar'  data-fancybox-group='kategori_image' class='btn btn-default fancybox-buttons' title='$cetak[deskripsi]'><i class='fa fa-image'></i> &nbsp view</a></td>
			  <td >$cetak[nama_kategori]</td>
			  <td align=center>$cetak[jumlah_produk]</td>
			  <td align=center>
				<button type=button class='btn btn-primary btn-sm' style='margin-right:5px' onclick='isi_form_edit_subkategori($cetak[id_subkategori])'><i class='fa fa-pencil'></i> &nbsp Edit</button>
				<button type=button class='btn btn-danger btn-sm' onclick=\"delete_subkategori('$cetak[id_subkategori]','$cetak[nama]','$cetak[jumlah_produk]')\"><i class='fa fa-trash'></i> &nbsp Hapus</button>
			  </td>
			</tr>
			";
			$no++;
		}
	}
	
	public function reload_kategori()
	{
		$kategori=$this->kategori_model->daftar_kategori_detail();
		$no=1;
		foreach($kategori->result_array() as $cetak){
			echo "
			<tr>
			  <td align=center>$no</td>
			  <td >$cetak[nama]</td>				
			  <td align=center>$cetak[jumlah_subkategori]</td>
			  <td align=center>$cetak[jumlah_produk]</td>
			  <td align=center>
				<button type=button class='btn btn-primary btn-sm' style='margin-right:5px' onclick=\"isi_form_edit_kategori($cetak[id_kategori],'$cetak[nama]')\"><i class='fa fa-pencil'></i> &nbsp Edit</button>
				<button type=button class='btn btn-danger btn-sm' onclick=\"delete_kategori('$cetak[id_kategori]','$cetak[nama]','$cetak[jumlah_subkategori]')\"><i class='fa fa-trash'></i> &nbsp Hapus</button>
			  </td>
			</tr>
			";
			$no++;
		}
	}
	
	public function subkategori_by_id(){
		$id=$this->input->post('id');
		$kategori=$this->subkategori_model->subkategori_by_id($id);
		foreach($kategori->result() as $cetak){
			$data = array(
			'id_subkategori' => $cetak->id_subkategori, 
			'nama_kategori' => $cetak->nama, 
			'id_kategori' => $cetak->id_kategori, 
			'deskripsi' => $cetak->deskripsi 
			);
			echo json_encode($data);
		}
		
	}
	
	public function simpan_kategori()
	{
		$action=$this->input->post('action');
		$id_kategori=$this->input->post('id_kategori');
		$this->validate_kategori();
		// siapkan data produk
		$tgl_skrg = date("Ymd");
		if($action=='tambah'){
			$data = array(
				'nama' => ucwords($this->input->post('nama_kategori')),
				'created' => $tgl_skrg, 
				'updated' => $tgl_skrg 
			);
			//simpan ke database
			$this->kategori_model->insert_kategori($data);
		}
		elseif($action=='edit')
		{
			$data = array(
				'nama' => ucwords($this->input->post('nama_kategori')),
				'updated' => $tgl_skrg 
			);
			//simpan ke database
			$this->kategori_model->update($id_kategori,$data);
		}
		
		
		$data['status']=TRUE;
		// feedback data ke view via ajax
	    echo json_encode($data);
	}
	
	public function simpan_subkategori()
	{
		$action=$this->input->post('action');
		$id_subkategori=$this->input->post('id_subkategori');
		$this->validate_subkategori();		
		
		// siapkan data produk
		$tgl_skrg = date("Ymd");
		$data = array(
			'nama' => ucwords($this->input->post('nama_kategori')),
			'id_kategori' => $this->input->post('id_kategori'),
			'deskripsi' => $this->input->post('deskripsi_sub')
		);
		
		if(!$_FILES['gambar']['error'] && ($action=='tambah' || $action=='edit'))
		{
			$gambar=$this->validate_gambar($action);
			$data['gambar']=$gambar;
			if($action=='tambah'){
				$data['created']=$tgl_skrg;
				$data['updated']=$tgl_skrg;
				//simpan data melalui model
				$this->subkategori_model->insert($data);
			}
			if($action=='edit'){
				$data['updated']=$tgl_skrg;
				//update data melalui model
				$this->subkategori_model->update($id_subkategori,$data);
			}
		}
		elseif($_FILES['gambar']['error'] && $action=='tambah'){ // gak ada gambar action tambah
			$data['inputerror'][] = 'gambar';
			$data['error_string'][] = 'Gambar tidak Boleh Kosong';
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		elseif($_FILES['gambar']['error'] && $action=='edit'){ // gak ada gambar action edit
			$data['updated']=$tgl_skrg;
			//update data melalui model
			$this->subkategori_model->update($id_subkategori,$data);
		}	
		
		//siapkan data feedback yang akan ditampilkan
		$data = array(
			'status' => TRUE 
        );
		// feedback data ke view via ajax
	    echo json_encode($data);
	}
	
	public function delete_subkategori()
	{
		$id_subkategori=$this->input->post('id');		
			
		//hapus image di folder produk
		$kategori=$this->subkategori_model->subkategori_by_id($id_subkategori);		
		foreach($kategori->result_array() as $cetak){
			$gambar=$cetak['gambar'];			
		}		
		//hapus dari database
		$this->subkategori_model->delete_subkategori($id_subkategori);
		$status_delete=unlink("kategori/".$gambar);	
		
		$data = array(
					"status" => true
			);
			//output to json format
			echo json_encode($data);		
	}
	
	public function delete_kategori()
	{
		$id_kategori=$this->input->post('id');		
			
		//hapus dari database
		$this->kategori_model->delete_kategori($id_kategori);
		
		$data = array(
					"status" => true
			);
			//output to json format
			echo json_encode($data);		
	}
			
	//fungsi validasi
	private function validate_subkategori()
	{		
		$this->load->library('form_validation');
		// menentukan rules validasi ubah profil
		$this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'required|min_length[3]|max_length[35]');
		$this->form_validation->set_rules('deskripsi_sub', 'Deskripsi Kategori', 'required|min_length[3]');
		$this->form_validation->set_rules('id_kategori', 'Kategori', 'required|numeric');
				
		//set message error
		$this->form_validation->set_message('min_length', '{field} minimal {param} karakter.');
		$this->form_validation->set_message('max_length', '{field} maksimal {param} karakter.');
		$this->form_validation->set_message('required', '{field} tidak boleh kosong.');
		//$this->form_validation->set_message('alpha_numeric_spaces', '{field} hanya boleh diisi huruf dan angka');
		$this->form_validation->set_message('numeric', '{field} hanya boleh diisi angka');
		//$this->form_validation->set_message('is_natural', '{field} hanya boleh diisi angka.');
		
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
	
		if($this->form_validation->run()==FALSE){			
			if(form_error('nama_kategori')){
				$data['inputerror'][] = 'nama_kategori';
				$data['error_string'][] = form_error('nama_kategori');
				$data['status'] = FALSE;
			}
			if(form_error('deskripsi')){
				$data['inputerror'][] = 'deskripsi';
				$data['error_string'][] = form_error('deskripsi');
				$data['status'] = FALSE;
			}
			if(form_error('id_kategori')){
				$data['inputerror'][] = 'id_kategori';
				$data['error_string'][] = form_error('id_kategori');
				$data['status'] = FALSE;
			}
		}
		
			
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}		
	}
	
	private function validate_kategori()
	{		
		$this->load->library('form_validation');
		// menentukan rules validasi ubah profil
		$this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'required|min_length[3]|max_length[35]');
				
		//set message error
		$this->form_validation->set_message('min_length', '{field} minimal {param} karakter.');
		$this->form_validation->set_message('max_length', '{field} maksimal {param} karakter.');
		$this->form_validation->set_message('required', '{field} tidak boleh kosong.');
		//$this->form_validation->set_message('is_natural', '{field} hanya boleh diisi angka.');
		
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
	
		if($this->form_validation->run()==FALSE){			
			if(form_error('nama_kategori')){
				$data['inputerror'][] = 'nama_kategori';
				$data['error_string'][] = form_error('nama_kategori');
				$data['status'] = FALSE;
			}
		}			
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}		
	}
	
	private function validate_gambar($action)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
		
				
		//validasi gambar
			$config['upload_path']          = 'kategori/';
			$config['encrypt_name'] 		= TRUE;
            $config['allowed_types']        = 'bmp|jpg|png';
            $config['max_size']             = 2048;
			$config['overwrite']            = false;
            $config['max_width']            = 0;
            $config['max_height']           = 0;
			$config['file_ext_tolower']  	= TRUE;

            $this->load->library('upload',$config);						

			
			$gambar="";			
            if ( ! $this->upload->do_upload('gambar')){
                $data['inputerror'][] = 'gambar';
				$data['status'] = FALSE;
				$data['error_string'][] = $this->upload->display_errors();				
            }else{
				$upload_data = $this->upload->data(); 
				$gambar =   $upload_data['file_name'];
				
				if($action=='edit'){ // kalau actionnya edit hapus gambar lama				
					$id_subkategori=$this->input->post('id_subkategori');
					$kategori=$this->subkategori_model->subkategori_by_id($id_subkategori);	
					foreach ($kategori->result_array() as $cetak){
						$gambar_old=$cetak['gambar'];
					}
					unlink("kategori/".$gambar_old);					
				}				
			}			
			
			if($data['status'] === FALSE)
			{
				echo json_encode($data);
				exit();
			}
			
			return $gambar;
	}
}
