<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct() {
		parent::__construct();
		//load model-model yang dibutuhkan
		$this->load->model('user_model');	
	}	
	 
	public function index()
	{
		$this->load->view('login');
	}
	public function aksi_login()
	{
		//validasi data
		$this->_validate();				
		$user_email = $this->input->post('user_email');
		$password = md5('victoria'.$this->input->post('password')); //tambahin kata victoria untuk keamanan (salt md5)
		
		//cek di database
		$query=$this->user_model->cek_login($user_email,$password);
		if($query->num_rows()>0){			
			//ubah status di db jd login
			$this->user_model->update_status_login('online');
			foreach ($query->result() as $cetak){
				$id_user= $cetak->id_user;
				$email=$cetak->email;
				$nama= $cetak->nama;
				$foto= $cetak->foto;
				$status= $cetak->status;
			}
			
			//set session
			$time=5000; // 30 menit
			$data_session = array(
				'id_user' => $id_user,
				'email' => $email,
				'nama' => $nama,
				'foto' => $foto,
				'timeout' => time()+$time,
				'status' => 'online'
			);				
			$this->session->set_userdata($data_session);
			
			$data = array(
				'status' => TRUE 
			);
		}else{
			$data = array(
				'status' => 'wrong_username_pass' 
			);	
		}
		//simpan data melalui model
		//$this->hubungi_kami_model->input_pesan($data);
		
		//siapkan data feedback yang akan ditampilkan
		// feedback data ke view via ajax
	    echo json_encode($data);
	}
	public function logout()
	{
		$this->session->sess_destroy();
		$this->index();
	}
	
	//fungsi validasi
	private function _validate()
	{
		// menentukan rules validasi
		$this->load->library('form_validation');
		$this->form_validation->set_rules('password', 'Password', 'required|alpha_numeric_spaces|min_length[3]|max_length[25]');
		$this->form_validation->set_rules('user_email', 'Username atau Email', 'required|min_length[3]|max_length[50]');
				
		//set message error
		$this->form_validation->set_message('min_length', '{field} minimal {param} karakter.');
		$this->form_validation->set_message('max_length', '{field} maksimal {param} karakter.');
		$this->form_validation->set_message('required', '{field} tidak boleh kosong.');
		$this->form_validation->set_message('alpha_numeric_spaces', '{field} hanya boleh diisi huruf dan angka');
		
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
	
		if($this->form_validation->run()==FALSE){
			if(form_error('user_email')){
				$data['inputerror'][] = 'user_email';
				$data['error_string'][] = form_error('user_email');
				$data['status'] = FALSE;
			}
			if(form_error('password')){
				$data['inputerror'][] = 'password';
				$data['error_string'][] = form_error('password');
				$data['status'] = FALSE;
			}
		}		
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function login_sosmed()
	{
		$user_email = $this->input->post('user_email');
		$uid = $this->input->post('uid');
		$provider = $this->input->post('provider');
		//$password = md5('kodein'.$this->input->post('password')); //tambahin salt
		//echo"select p.*,u.* from user u inner join provider p on p.user_id=u.user_id where p.provider_uid='$uid' and u.email='$user_email' and p.nama_provider='$provider'"; die;
		//cek di database
		$query=$this->user_model->cek_login_sosmed($user_email,$uid,$provider);
		if($query->num_rows()>0){			
			//ubah status di db jd login
			//$this->user_model->update_status_login('online');
			foreach ($query->result() as $cetak){
				$id_user= $cetak->user_id;
				$email=$cetak->email;
				$nama= $cetak->nama;
				$foto= $cetak->foto;
			}
			
			//set session
			$time=5000; // 30 menit
			$data_session = array(
				'id_user' => $id_user,
				'email' => $email,
				'nama' => $nama,
				'foto' => $foto,
				'timeout' => time()+$time,
				'status' => 'online'
			);				
			$this->session->set_userdata($data_session);
			
			$data = array(
				'status' => TRUE 
			);
		}else{
			$data = array(
				'status' => 'not_found' 
			);	
		}
		//simpan data melalui model
		//$this->hubungi_kami_model->input_pesan($data);
		
		//siapkan data feedback yang akan ditampilkan
		// feedback data ke view via ajax
	    echo json_encode($data);
	}
}
