<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edit_akun extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct() {
		parent::__construct();
		//load model-model yang dibutuhkan
		$this->load->model('user_model');	
		$this->load->helper('login');
		if(!cek_login()){
			//set status offline
			$this->user_model->update_status_login('offline');
			redirect(base_url("adminpage/login"));			
		}
	}	
	 
	public function index()
	{
		$data['konten']= "edit_akun_konten"; // nama file view konten
		$data['header']="Edit Akun Profil";
		$this->breadcrumbs->push('Home', 'adminpage');
		$this->breadcrumbs->push('Edit Akun', 'Edit Akun');
		$this->load->view('media_admin',$data);	// media_admin adalah template
	}
	
	public function ubah_password()
	{
		//validasi data
		$this->validate_password();				
		$password_lama = md5('victoria'.$this->input->post('password_lama')); //tambahin kata victoria untuk keamanan (salt md5)
		$password_baru = md5('victoria'.$this->input->post('password_baru')); //tambahin kata victoria untuk keamanan (salt md5)
		$id_user=$this->session->userdata('id_user');
		$query=$this->user_model->cek_user($id_user,$password_lama);
		if($query->num_rows()>0){
			$this->user_model->update_password($password_baru); //update password ke yang baru
			$data = array(
				'status' => TRUE 
			);
		}else{
			$data = array(
				'status' => 'password_lama_salah'
			);
		}		
		echo json_encode($data);
	}
	
	public function ubah_profil()
	{
		//validasi data
		$this->validate_profil();				
		$password = md5('victoria'.$this->input->post('password')); //tambahin kata victoria untuk keamanan (salt md5)
		$id_user=$this->session->userdata('id_user');		
		$query=$this->user_model->cek_user($id_user,$password);
		$nama=$this->input->post('nama');
		$email=$this->input->post('email');
		if($query->num_rows()>0){
			$data_baru = array(
				'nama' => ucwords($nama),
				'email' => $email
			);
			if(!$_FILES['gambar']['error']){
				$gambar=$this->validate_gambar($id_user,$password); // update gambar profil
				$data_baru['foto']=$gambar;
				$this->session->set_userdata('foto', $gambar); // update session nama dan email
				$data['reload']=true;
				$this->user_model->update_profil($id_user, $data_baru); //update profil ke yang baru
			}else{
				$this->user_model->update_profil($id_user, $data_baru); //update profil ke yang baru
			}						
			$this->session->set_userdata('nama', $nama); // update session nama dan email
			$this->session->set_userdata('email', $email); // update session nama dan email
			
			$data['status'] = true;
		}else{
			$data['status'] = 'password_salah';
		}		
		echo json_encode($data);
	}
	
	//fungsi validasi
	private function validate_password()
	{
		// menentukan rules validasi ganti password
		$this->load->library('form_validation');
		$this->form_validation->set_rules('password_lama', 'Password Lama', 'required|alpha_numeric_spaces|min_length[3]|max_length[25]');
		$this->form_validation->set_rules('password_baru', 'Password Baru', 'required|alpha_numeric_spaces|min_length[3]|max_length[25]');
		$this->form_validation->set_rules('password_confirm', 'Konfirmasi Password Baru', 'required|alpha_numeric_spaces|min_length[3]|max_length[25]');
		$this->form_validation->set_rules('user_email', 'Username atau Email', 'required|min_length[3]|max_length[50]');		
				
		//set message error
		$this->form_validation->set_message('min_length', '{field} minimal {param} karakter.');
		$this->form_validation->set_message('max_length', '{field} maksimal {param} karakter.');
		$this->form_validation->set_message('required', '{field} tidak boleh kosong.');
		$this->form_validation->set_message('alpha_numeric_spaces', '{field} hanya boleh diisi huruf dan angka');
		
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
	
		if($this->form_validation->run()==FALSE){
			if(form_error('password_lama')){
				$data['inputerror'][] = 'password_lama';
				$data['error_string'][] = form_error('password_lama');
				$data['status'] = FALSE;
			}
			if(form_error('password_baru')){
				$data['inputerror'][] = 'password_baru';
				$data['error_string'][] = form_error('password_baru');
				$data['status'] = FALSE;
			}
			if(form_error('password_confirm')){
				$data['inputerror'][] = 'password_confirm';
				$data['error_string'][] = form_error('password_confirm');
				$data['status'] = FALSE;
			}
		}		
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
	
	//fungsi validasi
	private function validate_profil()
	{		
		$this->load->library('form_validation');
		// menentukan rules validasi ubah profil
		$this->form_validation->set_rules('nama', 'Nama', 'required|min_length[3]|max_length[25]|alpha_numeric_spaces');
		$this->form_validation->set_rules('email', 'Alamat Email', 'min_length[10]|max_length[50]|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required|alpha_numeric_spaces|min_length[3]|max_length[25]');
				
		//set message error
		$this->form_validation->set_message('min_length', '{field} minimal {param} karakter.');
		$this->form_validation->set_message('max_length', '{field} maksimal {param} karakter.');
		$this->form_validation->set_message('required', '{field} tidak boleh kosong.');
		$this->form_validation->set_message('alpha_numeric_spaces', '{field} hanya boleh diisi huruf dan angka');
		$this->form_validation->set_message('valid_email', '{field} tidak valid.');
		
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
	
		if($this->form_validation->run()==FALSE){			
			if(form_error('nama')){
				$data['inputerror'][] = 'nama';
				$data['error_string'][] = form_error('nama');
				$data['status'] = FALSE;
			}
			if(form_error('email')){
				$data['inputerror'][] = 'email';
				$data['error_string'][] = form_error('email');
				$data['status'] = FALSE;
			}
			if(form_error('password')){
				$data['inputerror'][] = 'password';
				$data['error_string'][] = form_error('password');
				$data['status'] = FALSE;
			}
		}		
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
	
	private function validate_gambar($id_user,$password)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
		
				
		//validasi gambar
			$config['upload_path']          = 'images/';
			$config['encrypt_name'] 		= TRUE;
            $config['allowed_types']        = 'bmp|jpg|png';
            $config['max_size']             = 2048;
			$config['overwrite']            = false;
            $config['min_width']            = 128;
            $config['min_height']           = 128;
			$config['file_ext_tolower']  	= TRUE;

            $this->load->library('upload',$config);						

			
			$gambar="";			
            if ( ! $this->upload->do_upload('gambar')){
                $data['inputerror'][] = 'gambar';
				$data['status'] = FALSE;
				$data['error_string'][] = $this->upload->display_errors();				
            }else{
				$upload_data = $this->upload->data(); 
				$gambar =   $upload_data['file_name'];
				$query=$this->user_model->cek_user($id_user,$password);	
				foreach ($query->result_array() as $cetak){
					$gambar_old=$cetak['foto'];
				}
				unlink("images/".$gambar_old);				
			}			
			
			if($data['status'] === FALSE)
			{
				echo json_encode($data);
				exit();
			}
			
			return $gambar;
	}
}
