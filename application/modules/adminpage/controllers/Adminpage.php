<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminpage extends Login_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
		parent::__construct();
	}	
	 
	public function index()
	{
		$data['konten']= "adminpage_konten"; // nama file view konten		
		$data['header']="Dashboard";
		$this->breadcrumbs->push('Home', 'adminpage');
		$this->load->view('media_admin',$data);	// media_admin adalah template
	}


	public function BCA(){    
        $this->load->library('ibParser'); // library bank parser    
        $this->load->model('butab_model');
        $id_butab="1";
        $data=$this->butab_model->butab_byid($id_butab);
        foreach ($data->result_array() as $cetak) {
        	$username=$cetak['username'];
        	$password=$cetak['password'];
        	$bank=$cetak['nama_bank'];
        	$id_user=$cetak['id_user'];
        }        
        //$data=simpan_BCA($bank,$username,$password,$id_user,$id_butab);

        $parser = new IbParser;
    
	    // Ambil transaksi
	    if ( $data = $parser->getTransactions( $bank, $username, $password ) )
	    {

	        // echo '<pre>' . print_r( $transactions, true ) . '</pre>';
	        $hasil="<table border=1 cellspacing=2 cellpadding=2>
	                    <tr>                          
	                        <td align=center>Date</td>
	                        <td align=center>Detail</td>
	                        <td align=center>Type</td>
	                        <td align=center>Nominal</td>
	                    </tr>";

	        // loop
	        $found=0;
	        foreach( $data['transactions'] as $transaction )
	        {
	        	$tanggal	=$transaction[0];
	        	$detail		=$transaction[1];
	        	$type		=$transaction[2];
	        	$nominal 	=$transaction[3];
	        	$trx = array(
						'tanggal' => $transaction[0], 
						'detail' => $transaction[1], 
						'type' => $transaction[2], 
						'nominal' => $transaction[3],
						'id_butab' => $id_butab,
						'id_user' => $id_user,
						'date_create' => date('Y-m-d h:i:s')
						);	
            	$nominal = number_format($transaction[3],0,"",".");
                $hasil.="
                        <tr>                            
                            <td>$transaction[0]</td>
                            <td>$transaction[1]</td>
                            <td>$transaction[2]</td>
                            <td>$nominal</td>
                        </tr>"; 
	            $found++; 
	            $this->butab_model->insert_trx($trx); 
	            //echo $this->db->last_query();     
	        }
	        if($found<1){
	            echo "Transaksi tidak ditemukan";
	        }else{
	            echo "</br> $found Transaksi ditemukan </br>";
	        }
	        $hasil.="</table>";
	        echo $hasil;
	        echo "saldo anda saat ini: ".$data['balance'];
	    }
	}
	function test(){    
        $this->load->library('ibParser'); // library bank parser    
        $bank="BCA";
        $username="DENIADIT0528";
        $password="106948";
        $data=cek_BCA($bank,$username,$password);
	}
}
