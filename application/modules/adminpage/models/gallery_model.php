<?php 
class Gallery_model extends CI_Model {
		
		private $table='gallery';
		
        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function daftar_gallery()
        {	
			$query=$this->db
						->get($this->table);      
            return $query;
        }		
		public function gallery_by_id($id)
        {	
			$query=$this->db
						->where('id_gallery',$id)
						->get($this->table);      
            return $query;
        }		
		public function update($id,$data)
        {	
            $this->db->where('id_gallery',$id)->update($this->table,$data);
        }
}