<?php 
class User_model extends CI_Model {
		
		private $table_user='user';
		
        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function cek_login($user_email,$password)
        {	
            $query=$this->db
						->where('password',$password)
						->where('email',$user_email)						
						->get($this->table_user);
            return $query;
        }
		
		public function update_status_login($status)
        {	
            $query=$this->db
						->set('status',$status)
						->update($this->table_user);
        }
		public function update_password($password)
        {	
            $query=$this->db
						->set('password',$password)
						->update($this->table_user);
        }
		
		public function cek_user($id_user,$password)
        {	
            $query=$this->db
						->where('id_user',$id_user)
						->where('password',$password)
						->get($this->table_user);
            return $query;
        }
		
		public function update_profil($id_user, $data)
        {	
			$this->db->where('id_user',$id_user)->update($this->table_user,$data);
        }

        public function cek_login_sosmed($user_email,$uid,$provider)
        {	
            $query=$this->db
						->select('p.*, u.*')
						->from('provider p')
						->join('user u','u.user_id=p.user_id','inner')
						->where('p.provider_uid',$uid)
						->where('u.email',$user_email)
						->where('p.nama_provider',$provider)
						->get();
            return $query;
        }
}