<?php 
class Subkategori_model extends CI_Model {
		
		
		private $table_subkategori='subkategori';
		
        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function daftar_subkategori()
        {	
            $query=$this->db
						->select('subkategori.* , (select nama from kategori where id_kategori=subkategori.id_kategori) as nama_kategori')
						->get($this->table_subkategori);
            return $query;
        }
		
		public function daftar_subkategori_detail()
        {	
            $query=$this->db
						->order_by('id_subkategori','desc')
						->select('subkategori.*, (select nama from kategori where id_kategori=subkategori.id_kategori) as nama_kategori, (select count(*) from produk where id_subkategori=subkategori.id_subkategori) as jumlah_produk')
						->get($this->table_subkategori);
            return $query;
        }
		
		public function insert($data)
        {	
            $this->db->insert($this->table_subkategori,$data);
        }
		
		public function subkategori_by_id($id)
        {	
            $query=$this->db->where('id_subkategori',$id)->get($this->table_subkategori);
            return $query;
        }
		
		public function delete_subkategori($id_subkategori)
		{
			$this->db->where('id_subkategori',$id_subkategori)->delete($this->table_subkategori);
		}
		
		public function update($id,$data)
        {	
            $this->db->where('id_subkategori',$id)->update($this->table_subkategori,$data);
        }
}