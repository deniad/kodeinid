<?php 
class butab_model extends CI_Model {
		
        public function __construct()
        {
                // Call the CI_Model constructor
        	/*
				ketika digunakan online
				1. where tambahkan id_user untuk memastikan
        	*/
                parent::__construct();

        }
		
		public function butab_byid($id_butab)
        {	
            $query=$this->db
            			->select('butab.*, bank.nama_bank')
            			->from('butab')
            			->join('bank','bank.id_bank=butab.id_bank')
            			->where('butab.id_butab',$id_butab)
            			->get();
            return $query;
        }

		public function insert_trx($data)
        {	
            $query=$this->cek_trx($data['id_butab'],$data['detail'],$data['nominal'],$data['type']);
        	$cek=$query->num_rows();
        	if($cek==0){
            	$this->db->insert('transaksi',$data);
        	}else{
                foreach ($query->result_array() as $cetak) {
                    if($cetak['tanggal']=='PEND'){
                        $this->update_trx_pend($cetak['id_trx'],$data['tanggal']);
                    }       
                }                
            }
        	//echo $this->db->last_query();
        }

        private function cek_trx($id_butab,$detail,$nominal,$type){
        	$query=$this->db
	        			->where('id_butab',$id_butab)
	        			->where('detail',$detail)
                        ->where('type',$type)
	        			->where('nominal',$nominal)
	        			->get('transaksi');
	        return $query;
        }

        private function update_trx_pend($id_trx,$tanggal){
            $this->db   ->set('tanggal',$tanggal)
                        ->where('id_trx',$id_trx)
                        ->update('transaksi');
        }
		
}