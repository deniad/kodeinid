<?php 
class Kategori_model extends CI_Model {
		
		private $table_kategori='kategori';
		
        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function daftar_kategori_detail()
        {	
            $query=$this->db
						->order_by('id_kategori','desc')
						->select("kategori.*, (select count(*) from subkategori where id_kategori = kategori.id_kategori)as jumlah_subkategori, 
									(SELECT COUNT( * ) 
									FROM produk
									JOIN subkategori ON subkategori.id_subkategori = produk.id_subkategori
									JOIN kategori k ON k.id_kategori = subkategori.id_kategori
									WHERE subkategori.id_kategori = kategori.id_kategori
									) AS jumlah_produk ")
						->from($this->table_kategori)
						->get();
			
            return $query;
        }
		public function insert_kategori($data)
        {	
            $this->db->insert($this->table_kategori,$data);
        }
		
		public function delete_kategori($id_kategori)
		{
			$this->db->where('id_kategori',$id_kategori)->delete($this->table_kategori);
		}
		
		public function update($id,$data)
        {	
            $this->db->where('id_kategori',$id)->update($this->table_kategori,$data);
        }
}