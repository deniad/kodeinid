	<link href="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.css');?>" type='text/css' rel="stylesheet">
	<link href="<?php echo base_url('assets/css/button-switch.css');?>" type='text/css' rel="stylesheet">
	<!-- DataTables -->
	<script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
	
	<!-- Default box -->
	<div class="col-md-10 col-md-offset-1 col-xs-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<i class="fa fa-tag"></i> &nbsp
				<h3 class="box-title">Daftar Kategori Produk</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
					<i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
					<i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">				
				<div class="col-xs-12" style="margin-bottom:10px">
					<button class="btn btn-default" style="margin-right:10px;" onclick="form_tambah_kategori()"><i class="fa fa-plus"></i> &nbsp Tambah Kategori</button>
					<button class="btn btn-default pull-right" data-loading-text="Please wait..." id="tombol_reload_kategori" onclick="reload_kategori()"><i class="fa fa-refresh"></i> &nbsp Reload Data</button>
				</div>
				<div class="col-xs-12">
					<table id="table-kategori" class="table table-bordered table-hover dt-responsive nowrap" style="width:100%">
						<thead>
						<tr class=active >
						  <td align=center>No &nbsp </th>
						  <td align=center>Nama Kategori</th>
						  <td align=center>Subkategori</th>
						  <td align=center>Produk</th>
						  <td align=center>Action</th>
						</tr>
						</thead>
						<tbody id="isi_tabel_kategori">
							<?php	
								$no=1;
								foreach($kategori->result_array() as $cetak){
									echo "
									<tr>
									  <td align=center>$no</td>
									  <td >$cetak[nama]</td>				
									  <td align=center>$cetak[jumlah_subkategori]</td>
									  <td align=center>$cetak[jumlah_produk]</td>
									  <td align=center>
											<button type=button class='btn btn-primary btn-sm' style='margin-right:5px' onclick=\"isi_form_edit_kategori($cetak[id_kategori],'$cetak[nama]')\"><i class='fa fa-pencil'></i> &nbsp Edit</button>
											<button type=button class='btn btn-danger btn-sm' onclick=\"delete_kategori('$cetak[id_kategori]','$cetak[nama]','$cetak[jumlah_subkategori]')\"><i class='fa fa-trash'></i> &nbsp Hapus</button>
									  </td>
									</tr>
									";
									$no++;
								}
							?>			
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		<!-- tabel sub kategori -->
		<div class="box box-primary">
			<div class="box-header with-border">
				<i class="fa fa-tags"></i> &nbsp
				<h3 class="box-title">Daftar Sub Kategori Produk</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
					<i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
					<i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">				
				<div class="col-xs-12" style="margin-bottom:10px">					
					<button class="btn btn-default" onclick="form_tambah_subkategori()"><i class="fa fa-plus"></i> &nbsp Tambah Sub Kategori</button>
					<button class="btn btn-default pull-right" id="tombol_reload_subkategori" data-loading-text="Please wait..." onclick="reload_subkategori()"><i class="fa fa-refresh"></i> &nbsp Reload Data</button>
				</div>
				<div class="col-xs-12">
					<table id="table-subkategori" class="table table-bordered table-hover dt-responsive nowrap" style="width:100%">
						<thead>
						<tr class=active >
						  <td align=center>No <?php echo "&nbsp "; ?></th>
						  <td align=center>Nama SubKategori</th>
						  <td align=center>Gambar</th>
						  <td align=center>Nama Kategori</th>						  
						  <td align=center>Jml Produk</th>
						  <td align=center>Action</th>
						</tr>
						</thead>
						<tbody id="isi_tabel_subkategori">
					<?php	
						$no=1;
						foreach($subkategori->result_array() as $cetak){
							$gambar=base_url("kategori/$cetak[gambar]");
							echo "
							<tr>
							  <td align=center>$no</td>
							  <td >$cetak[nama]</td>							  
							  <td align=center><a href='$gambar'  data-fancybox-group='kategori_image' class='btn btn-default fancybox-buttons' title='$cetak[deskripsi]'><i class='fa fa-image'></i> &nbsp view</a></td>
							  <td >$cetak[nama_kategori]</td>
							  <td align=center>$cetak[jumlah_produk]</td>
							  <td align=center>
									<button type=button class='btn btn-primary btn-sm' style='margin-right:5px' onclick='isi_form_edit_subkategori($cetak[id_subkategori])'><i class='fa fa-pencil'></i> &nbsp Edit</button>
									<button type=button class='btn btn-danger btn-sm' onclick=\"delete_subkategori('$cetak[id_subkategori]','$cetak[nama]','$cetak[jumlah_produk]')\"><i class='fa fa-trash'></i> &nbsp Hapus</button>
							  </td>
							</tr>
							";
							$no++;
						}
					?>							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- modal edit dan tambah subkategori -->
	<div class="modal fade" id="modal_subkategori" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header bg-primary" align=center>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="modal_header">Tambah Sub Kategori</h4>
				</div>
				<form id="form_subkategori" method="POST" enctype="multipart/form-data" novalidate>
				<input type="hidden" id="id_subkategori" name="id_subkategori"/>
				<div class="modal-body">
					<input type="hidden" id="action_subkategori" name="action" value="" />
					<div class="row">									
						<div class="col-xs-12 col-sm-10 col-sm-offset-1 ">
							<div class="alert alert-info alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<ul style="text-align:justify; margin-left:-20px;">
									<li>File yang dapat diupload adalah image dengan ekstensi <b>jpg | jpeg | png | bmp </b></li>
									<li>Ukuran file image yang akan disimpan sistem berukuran maksimum <b>2MB</b></li>
									<li>Kosongkan gambar jika tidak merubah foto</li>
								</ul>
							</div>
							
							<div class="text-danger error_message" align=center id="nama_kategori_error"></div>
							<div class="form-group has-feedback">
								<input type="text" id="nama_kategori" name="nama_kategori" class="form-control data-subkategori" placeholder="Nama Kategori" maxlength="35" required />
								<span class="fa fa-tag form-control-feedback"></span>
							</div>
							
							<div class="text-danger error_message" align=center id="id_kategori_error"></div>
							<div class="form-group">
								<label>Kategori</label>
								<select class="form-control" name="id_kategori" id="id_kategori_onsub" required>
									<option value="">--Pilih Kategori --</option>
								<?php
								foreach($kategori->result_array() as $cetak){
									
								echo "
									<option value='$cetak[id_kategori]'>$cetak[nama]</option>										
								";
								}
								?>
								</select>
							</div>
							
							<div class="text-danger error_message" align=center id="gambar_error"></div>
							<div class='form-group input-group'>
								<span class='input-group-addon'><b>Gambar</b></span>
								<input type='file' class="form-control data-subkategori" accept="image/*" name='gambar' id="gambar" onChange='checkExtension(this.value,this.id)' />
							</div>
						</div>
						<div class="col-xs-12">
							<div class="text-danger error_message" align=center id="deskripsi_sub_error" style="margin-top:10px"></div>
							<div class="form-group">
								<textarea name="deskripsi_sub" id="deskripsi_sub" class="form-control data-subkategori" rows="3" placeholder="Belum Diisi" required ></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" id="submit_subkategori" class="btn btn-primary btn-block btn-lg" style="font-size:large" data-loading-text="Please wait..." ><i class="fa fa-check"></i> &nbsp Submit</button>
				</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- modal edit dan tambah kategori -->
	<div class="modal fade" id="modal_kategori" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header" align=center>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="modal_header">Tambah Kategori</h4>
				</div>
				<form id="form_kategori" method="POST">				
				<input type="hidden" id="id_kategori" name="id_kategori"/>
				<div class="modal-body">
					<input type="hidden" id="action_kategori" name="action" value="" />
					<div class="row">									
						<div class="col-xs-12">					
							<div class="text-danger error_message" align=center id="nama_kategori1_error"></div>
							<div class="form-group has-feedback">
								<input type="text" id="nama_kategori1" name="nama_kategori" class="form-control data-kategori" placeholder="Nama Kategori" maxlength="35" required />
								<span class="fa fa-tag form-control-feedback"></span>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" id="submit_kategori" class="btn btn-primary btn-block" style="font-size:large" data-loading-text="Please wait..." ><i class="fa fa-check"></i> &nbsp Submit</button>
				</div>
				</form>
			</div>
		</div>
	</div>
	
	
	<!--<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script> 
	<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>-->

	
	<script type="text/javascript">    
		//reload data tables
        function reload_subkategori(){
			$.ajax({
					url: "<?php echo base_url('adminpage/kategori/reload_subkategori');?>",
					type: "POST",
					dataType: 'html',
					beforeSend : function()
					{	
						$('#tombol_reload_subkategori').button('loading');	
					},
					success: function(res)
					{
						$('#isi_tabel_subkategori').html(res);
						$('#tombol_reload_subkategori').button('reset');
					},
					error: function(e) 
					{
						alert(e);
					} 	        
			   });
		}
		
		function reload_kategori(){
			$.ajax({
					url: "<?php echo base_url('adminpage/kategori/reload_kategori');?>",
					type: "POST",
					dataType: 'html',
					beforeSend : function()
					{	
						$('#tombol_reload_kategori').button('loading');	
					},
					success: function(res)
					{
						$('#isi_tabel_kategori').html(res);
						$('#tombol_reload_kategori').button('reset');
					},
					error: function(e) 
					{
						alert(e);
					} 	        
			   });
		}
		
		function form_tambah_subkategori(){
			$('#action_subkategori').val('tambah');// set action form add kategori
			$('#modal_header').html('Tambah  Subkategori');
			$('#modal_subkategori').modal('show'); // show modal
			
			//clear modal
			//CKupdate();
			$('#id_subkategori').val('');
			$('#nama_kategori').val('');
			$('#gambar').val('');
			tinyMCE.activeEditor.setContent('');
			//CKEDITOR.instances[instance].setData('');
			$("#gambar").prop('required',true);
			$('.form-group').removeClass('has-error');	
			$('.error_message').css('display','none');
		}
		
		function form_tambah_kategori(){
			$('#action_kategori').val('tambah');// set action form add kategori
			$('#modal_kategori').modal('show'); // show modal
			
			$('.form-group').removeClass('has-error');	
			$('.error_message').css('display','none');
		}
		
		function isi_form_edit_subkategori(id){
			$('.form-group').removeClass('has-error');	
			$('.error_message').css('display','none');
			$("#gambar").prop('required',false);
			$('#action_subkategori').val('edit');// set action form add kategori
			$.ajax({
					url: "<?php echo base_url('adminpage/kategori/subkategori_by_id');?>",
					type: "POST",
					dataType: 'json',
					data:  {id:id},
					success: function(res)
					{
						//CKupdate();
						$('#modal_header').html('Update Kategori '+res.nama_kategori);
						$('#id_subkategori').val(res.id_subkategori);
						$('#nama_kategori').val(res.nama_kategori);
						$('#id_kategori_onsub').val(res.id_kategori);
						tinyMCE.activeEditor.setContent(res.deskripsi);
						//CKEDITOR.instances[instance].setData(res.deskripsi);
						$('#modal_subkategori').modal('show');
					},
					error: function(e) 
					{
						alert(e);
					} 	        
			});
		}
		
		function isi_form_edit_kategori($id_kategori,$nama_kategori){
			$('.form-group').removeClass('has-error');	
			$('.error_message').css('display','none');
			$('#action_kategori').val('edit');// set action form add kategori	
			$('#id_kategori').val($id_kategori);			
			$('#nama_kategori1').val($nama_kategori);
			$('#modal_kategori').modal('show');
		}
		
		// allow all extensions
		var exts = "jpg|jpeg|png|bmp";
		// only allow specific extensions
		// var exts = "jpg|jpeg|gif|png|bmp|tiff|pdf";
		function checkExtension(value,id)
		{
			if(value=="")return true;
			var re = new RegExp("^.+\.("+exts+")$","i");
			if(!re.test(value))
			{
				$.alert({
					title: 'Ekstensi file tidak di izinkan',
					icon: 'fa fa-warning',
					theme: 'black',
					content: 'Jenis file yang di izinkan: &nbsp <b>'+exts.replace(/\|/g,',')+'</b>',
					backgroundDismiss: true,
					confirmButton: false,
					cancelButton: false
				});
				
				$('#'+id).val('');
				$('#'+id).parent().addClass('has-error');
			}else{
				$('#'+id).parent().removeClass('has-error');
			}
		}   
        
        // Ajax post, submit_hubungi_kami diroute ke produk/submit_hubungi_kami
        $(document).ready(function() {
			
			$("#form_kategori").on('submit',(function(e) {

				e.preventDefault();
				$.ajax({
					url: "<?php echo base_url('adminpage/kategori/simpan_kategori');?>",
					type: "POST",
					dataType: 'JSON',
					data:  new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					beforeSend : function()
					{
						$('.form-group').removeClass('has-error');	
						$('.error_message').css('display','none');	
						$('#submit_kategori').button('loading');	
					},
					success: function(res)
					{
						if (res)
						{
							if(res.status)
							{
								reload_kategori();
								$('#submit_kategori').button('reset'); //set button enable
								$('.data-kategori').val(''); //kosongin form
								$('#modal_kategori').modal('hide');
								$.alert({
									icon: 'fa fa-thumbs-o-up',
									title: 'Data Kategori berhasil disimpan',
									theme: 'black',
									content: false,
								});
							}
							else if(!res.status){
								for (var i = 0; i < res.inputerror.length; i++) 
								{
									$('[name="'+res.inputerror[i]+'"]').parent().addClass('has-error'); //parent div ditambahin class has error
									$('[id="'+res.inputerror[i]+'1_error"]').html(res.error_string[i]); // tulis pesan error											
									$('#'+res.inputerror[i]+'1_error').css("display","block"); //tampilkan pesan error									
								}
								$('#submit_kategori').button('reset'); //set button enable								
							}								 
						}
					},
					error: function(e) 
					{
						alert(e);
					} 	        
			   });
			}));
			
			//CKEDITOR.replace('deskripsi');		
			$("#form_subkategori").on('submit',(function(e) {
				//CKupdate();
				var deskripsi_sub = tinyMCE.get('deskripsi_sub').getContent();

				e.preventDefault();
				$.ajax({
					url: "<?php echo base_url('adminpage/kategori/simpan_subkategori');?>",
					type: "POST",
					dataType: 'JSON',
					data:  new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					beforeSend : function()
					{
						$('.form-group').removeClass('has-error');	
						$('.error_message').css('display','none');	
						$('#submit_subkategori').button('loading');	
					},
					success: function(res)
					{
						if (res)
						{
							if(res.status)
							{
								$('#submit_subkategori').button('reset'); //set button enable
								$('.data-subkategori').val(''); //kosongin form
								//CKEDITOR.instances[instance].setData('');
								reload_subkategori();								
								$('#modal_subkategori').modal('hide');
								$.alert({
									icon: 'fa fa-thumbs-o-up',
									title: 'Data Sub Kategoti berhasil disimpan',
									theme: 'black',
									content: false,
								});
							}
							else if(!res.status){
								for (var i = 0; i < res.inputerror.length; i++) 
								{
									$('[name="'+res.inputerror[i]+'"]').parent().addClass('has-error'); //parent div ditambahin class has error
									$('[id="'+res.inputerror[i]+'_error"]').html(res.error_string[i]); // tulis pesan error											
									$('#'+res.inputerror[i]+'_error').css("display","block"); //tampilkan pesan error									
								}
								$('#submit_subkategori').button('reset'); //set button enable								
							}								 
						}
					},
					error: function(e) 
					{
						alert(e);
					} 	        
			   });
			}));
		});
		/*
		function CKupdate(){
			for ( instance in CKEDITOR.instances )
				CKEDITOR.instances[instance].updateElement();
		}
		
		$(function () {
			tinymce.init({
			    selector: '#deskripsi_sub',
			    height: '350px',
			    theme: 'modern',
			    setup: function (editor) {
	            editor.on('change', function () {
	                tinymce.triggerSave();
		            });
		        },
				toolbar:'preview bold, italic, underline, alignleft, aligncenter, alignright, alignjustify, fontselect, fontsizeselect, cut, copy, paste, bullist, numlist, outdent, indent, undo, redo',
			    plugins: [
			      ' preview advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
			      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
			      'save table contextmenu directionality emoticons template paste textcolor'
			    ]
		    
			 });
		});
		*/
		    
		
		$('.fancybox-buttons').fancybox({
				openEffect  : 'elastic',
				closeEffect : 'fade',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
		});
		
		function delete_subkategori(id,nama_kategori,jumlah_produk)
		{
			if(jumlah_produk>0){
				$.alert({
					icon: 'fa fa-warning',
					title: 'Warning !',
					content: '<h4 align=justify>Masih terdapat <font color=red>'+jumlah_produk+' produk </font>  dengan subkategori ini, pastikan jumlah produk kosong untuk menghapus</h4>',
					theme: 'material',
				});
			}else{
				$.confirm({
					title: 'Yakin hapus <font color=red>'+nama_kategori+'</font> ?',
					content: false,
					confirmButtonClass: 'btn-danger',
					cancelButtonClass: 'btn-success',
					confirmButton: 'Ya',
					cancelButton: 'Cancel',
					theme: 'material',
					confirm: function(){
						//ajax delete data to database
						$.ajax({
							url : "<?php echo site_url('adminpage/kategori/delete_subkategori')?>",
							type: "POST",
							dataType: "JSON",
							data: {id: id},
							success: function(res)
							{
								if(res.status){
									reload_subkategori();
								}									
							},
							error: function (jqXHR, textStatus, errorThrown)
							{
								alert('Error deleting data');
						}
						});
					}
				});
			}
		}
		
		function delete_kategori(id,nama_kategori,jumlah_subkategori)
		{
			if(jumlah_subkategori>0){
				$.alert({
					icon: 'fa fa-warning',
					title: 'Warning !',
					content: '<h4 align=justify>Masih terdapat <font color=red>'+jumlah_subkategori+' Sub Kategori </font>  dengan Kategori ini, pastikan jumlah Sub Kategori kosong untuk menghapus</h4>',
					theme: 'material',
				});
			}else{
				$.confirm({
					title: 'Yakin hapus <font color=red>'+nama_kategori+'</font> ?',
					content: false,
					confirmButtonClass: 'btn-danger',
					cancelButtonClass: 'btn-success',
					confirmButton: 'Ya',
					cancelButton: 'Cancel',
					theme: 'material',
					confirm: function(){
						//ajax delete data to database
						$.ajax({
							url : "<?php echo site_url('adminpage/kategori/delete_kategori')?>",
							type: "POST",
							dataType: "JSON",
							data: {id: id},
							success: function(res)
							{
								if(res.status){
									reload_kategori();
								}									
							},
							error: function (jqXHR, textStatus, errorThrown)
							{
								alert('Error deleting data');
						}
						});
					}
				});
			}
		}
			
			$(function () {
				$('#table-subkategori').DataTable({
					"columnDefs": [
					{ "orderable": false, "targets": 2 },
					{ "orderable": false, "targets": -1 },
				  ]
				});
				
				$('#table-kategori').DataTable({
					"columnDefs": [
					{ "orderable": false, "targets": -1 },
				  ]
				});

				
		  });
	</script>
	