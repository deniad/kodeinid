	<!-- Default box -->
	<div class="col-md-4 col-sm-6 col-xs-12">
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<i class="fa fa-lock"></i> &nbsp
				<h3 class="box-title">Ganti Password</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
					<i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
					<i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">	
				<div id="ubah_pass_sukses" class="alert alert-success alert-dismissible pesan-sukses">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					Password berhasil diubah
				</div>
				<form id="form_ubah_password">
					<div class="text-danger error_message" align=center id="password_lama_error"></div>
					<div class="form-group has-feedback">
						<input type="password" id="password_lama" name="password_lama" class="form-control" placeholder="Passsword Lama" maxlength="25" required1>
						<span class="fa fa-key form-control-feedback"></span>
					</div>
					<div class="text-danger error_message" align=center id="password_baru_error"></div>
					<div class="form-group has-feedback">
						<input type="password" id="password_baru" name="password_baru" class="form-control" placeholder="Password Baru" maxlength="25" required1>
						<span class="fa fa-key form-control-feedback"></span>
					</div>
					<div class="text-danger error_message" align=center id="password_confirm_error"></div>
					<div class="form-group has-feedback">
						<input type="password" id="password_confirm" name="password_confirm" class="form-control" placeholder="Konfirmasi password baru" maxlength="25" required1>
						<span class="fa fa-key form-control-feedback"></span>
					</div>
					<button type="submit" id="submit_ganti_password"  data-loading-text='Please Wait...' class="btn btn-primary btn-block btn-flat">Submit &nbsp <i class="fa fa-check"></i></button>
				</form>		
			</div>
		</div>
	</div>
	
	<div class="col-md-4 col-sm-6 col-xs-12">
		<div class="box box-success box-solid">
			<div class="box-header with-border">
				<i class="fa fa-user"></i> &nbsp
				<h3 class="box-title">Edit Profil</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
					<i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
					<i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body" align=center>
				<img class="img-circle" width="150px" src="<?php echo base_url("images/$foto");?>" style="margin-bottom:10px;" />
				<div id="ubah_profil_sukses" class="alert alert-success alert-dismissible pesan-sukses">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					Profil Berhasil dirubah
				</div>
				<form id="form_ubah_profil" method="POST" enctype="multipart/form-data">
					<div class="form-group">
					  <label for="exampleInputFile">Profile Picture</label>
					  <input type="file" name="gambar" accept="image/*" id="gambar" onChange='checkExtension(this.value,this.id)'>
					</div>
					<div class="text-danger error_message" align=center id="nama_error"></div>
					<div class="form-group has-feedback">
						<input type="text" id="nama" name="nama" class="form-control" placeholder="Nama Lengkap" value="<?php echo $nama; ?>" maxlength="25" required1>
						<span class="fa fa-user form-control-feedback"></span>
					</div>
					<div class="text-danger error_message" align=center id="email_error"></div>
					<div class="form-group has-feedback">
						<input type="text" id="email" name="email" class="form-control" placeholder="Email" value="<?php echo $email; ?>" maxlength="50" required1>
						<span class="fa fa-envelope form-control-feedback"></span>
					</div>
					<div class="text-danger error_message" align=center id="password_error"></div>
					<div class="form-group has-feedback">
						<input type="password" id="password" name="password" class="form-control" placeholder="Password" maxlength="25" required1>
						<span class="fa fa-key form-control-feedback"></span>
					</div>
					<button type="submit" id="submit_profil"  data-loading-text='Please Wait...' class="btn btn-success btn-block btn-flat">Submit &nbsp <i class="fa fa-check"></i></button>
				</form>		
			</div>
		</div>
	</div>
	<script type="text/javascript">            
            // Ajax post, submit_hubungi_kami diroute ke produk/submit_hubungi_kami
            $(document).ready(function() {				
				function error(status, exception){
					if (status === 0) {
						alert('Timeout. Periksa koneksi anda.');
					} else if (status == 404) {
						alert('Requested page not found. [404]');
					} else if (status == 500) {
						alert('Internal Server Error [500].');
					} else if (exception === 'parsererror') {
						alert('Requested JSON parse failed.');
					} else if (exception === 'timeout') {
						alert('Time out error.');
					} else if (exception === 'abort') {
						alert('Ajax request aborted.');
					} else {
						alert('Uncaught Error.\n' + jqXHR.responseText);
					}
					$('#submit_ganti_password').button('reset'); // reset tombol submit
				}				
				
				$("#form_ubah_password").submit(function(event) {
					event.preventDefault();
					$('.error_message').css("display","none");
					$('#ubah_pass_sukses').css("display","none");
					$('#password_lama').parent().removeClass('has-error');
					$('#password_baru').parent().removeClass('has-error');
					$('#password_confirm').parent().removeClass('has-error');
					var password_lama = $('#password_lama').val();
					var password_baru = $('#password_baru').val();
					var password_confirm = $('#password_confirm').val();
					if(password_baru!="" && password_lama!="" && password_baru==password_lama){
						$('#password_lama_error').html("Password baru dan lama sama");
						$('#password_baru').parent().addClass('has-error');
						$('#password_lama').parent().addClass('has-error');
						$('#password_lama_error').css("display","block");
						$('#submit_ganti_password').button('reset'); //set button enable
					}else if(password_baru!=password_confirm){
						$('#password_lama_error').html("Password baru dan konfirmasi tidak cocok");
						$('#password_baru').parent().addClass('has-error');
						$('#password_confirm').parent().addClass('has-error');
						$('#password_lama_error').css("display","block");
						$('#submit_ganti_password').button('reset'); //set button enable
					}else if(password_baru==password_confirm){
						jQuery.ajax({
							type: "POST",
							url: "<?php echo base_url(); ?>" + "adminpage/edit_akun/ubah_password",
							dataType: 'JSON',
							data: {password_lama: password_lama, password_baru: password_baru, password_confirm: password_confirm},
							beforeSend: function() {
								$('#submit_ganti_password').button('loading');								
							},
							success: function(res) {
								if (res)
								{
									if(res.status === true) //if success tampilkan alert sukses
									{
										// munculkan pesan sukses
										$('#ubah_pass_sukses').css("display","block");										
										//kosongin form
										$('#password_lama').val('');
										$('#password_baru').val('');
										$('#password_confirm').val('');
										$('#submit_ganti_password').button('reset'); //set button enable
									}
									else if(res.status === false){
										for (var i = 0; i < res.inputerror.length; i++) 
										{
											$('[name="'+res.inputerror[i]+'"]').parent().addClass('has-error'); //parent div ditambahin class has error
											$('[id="'+res.inputerror[i]+'_error"]').html(res.error_string[i]); // tulis pesan error											
											$('#'+res.inputerror[i]+'_error').css("display","block"); //tampilkan pesan error
											$('#submit_ganti_password').button('reset'); //set button enable
										}										
									}else if(res.status == 'password_lama_salah'){
										$('#password_lama').parent().addClass('has-error');
										$('#password_lama_error').html("Password lama salah");
										$('#password_lama_error').css("display","block");
										$('#submit_ganti_password').button('reset'); //set button enable
									}								 
								}									
							}									
						});
					}
				});	

				$("#form_ubah_profil").submit(function(event) {
					event.preventDefault();
					$('.error_message').css("display","none");
					$('#ubah_profil_sukses').css("display","none");
					$('#nama').parent().removeClass('has-error');
					$('#email').parent().removeClass('has-error');
					$('#password').parent().removeClass('has-error');
					var nama = $('#nama').val();
					var email = $('#email').val();
					var password = $('#password').val();
						jQuery.ajax({
							type: "POST",
							url: "<?php echo base_url(); ?>" + "adminpage/edit_akun/ubah_profil",
							type: "POST",
							dataType: 'JSON',
							data:  new FormData(this),
							contentType: false,
							cache: false,
							processData:false,
							beforeSend: function() {
								$('#submit_profil').button('loading');								
							},
							success: function(res) {
								if (res)
								{
									if(res.status === true) //if success tampilkan alert sukses
									{
										// munculkan pesan sukses
										$('#ubah_profil_sukses').css("display","block");										
										//kosongin form
										$('#password').val('');
										$('#submit_profil').button('reset'); //set button enable
										if(res.reload == true){
											document.location.reload(true);
										}										
									}
									else if(res.status === false){
										for (var i = 0; i < res.inputerror.length; i++) 
										{
											$('[name="'+res.inputerror[i]+'"]').parent().addClass('has-error'); //parent div ditambahin class has error
											$('[id="'+res.inputerror[i]+'_error"]').html(res.error_string[i]); // tulis pesan error											
											$('#'+res.inputerror[i]+'_error').css("display","block"); //tampilkan pesan error
											$('#submit_profil').button('reset'); //set button enable
										}										
									}else if(res.status == 'password_salah'){
										$('#password').parent().addClass('has-error');
										$('#nama_error').html("Password anda salah");
										$('#nama_error').css("display","block");
										$('#submit_profil').button('reset'); //set button enable
									}								 
								}									
							}									
						});
				});
            });
			// allow all extensions
		var exts = "jpg|jpeg|png|bmp";
		// only allow specific extensions
		// var exts = "jpg|jpeg|gif|png|bmp|tiff|pdf";
		function checkExtension(value,id)
		{
			if(value=="")return true;
			var re = new RegExp("^.+\.("+exts+")$","i");
			if(!re.test(value))
			{
				$.alert({
					title: 'Ekstensi file tidak di izinkan',
					icon: 'fa fa-warning',
					theme: 'black',
					content: 'Jenis file yang di izinkan: &nbsp <b>'+exts.replace(/\|/g,',')+'</b>',
					backgroundDismiss: true,
					confirmButton: false,
					cancelButton: false
				});
				
				$('#'+id).val('');
				$('#'+id).parent().addClass('has-error');
			}else{
				$('#'+id).parent().removeClass('has-error');
			}
		}
	</script>
      <!-- /.box -->