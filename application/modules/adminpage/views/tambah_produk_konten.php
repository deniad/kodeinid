	<!-- Butoon switch -->
	<link href="<?php echo base_url('assets/css/button-switch.css');?>" type='text/css' rel="stylesheet">
	
	<script type="text/javascript">		
		
		// allow all extensions
var exts = "jpg|jpeg|png|bmp";
var next_id=0;
var id_file=0;
// only allow specific extensions
// var exts = "jpg|jpeg|gif|png|bmp|tiff|pdf";

function checkExtension(value,id)
{
    if(value=="")return true;
    var re = new RegExp("^.+\.("+exts+")$","i");
    if(!re.test(value))
    {
        $.alert({
			title: 'Ekstensi file tidak di izinkan',
			icon: 'fa fa-warning',
			theme: 'black',
			content: 'Jenis file yang di izinkan: &nbsp <b>'+exts.replace(/\|/g,',')+'</b>',
			backgroundDismiss: true,
			confirmButton: false,
			cancelButton: false
		});
		
		$('#'+id).val('');
		$('#'+id).parent().addClass('has-error');
    }else{
		$('#'+id).parent().removeClass('has-error');
	}
}


	function validate(f){
		var chkFlg = false;
		for(var i=0; i < f.length; i++) {
			if(f.elements[i].type=="file" && f.elements[i].value != "") {
				chkFlg = true;
			}
		}
		if(!chkFlg) {
			alert('Please browse/choose at least one file');
			return false;
		}
		f.pgaction.value='upload';
		return true;
	}
</script>
	
	
	<!-- Default box -->
	<div class="col-md-10 col-md-offset-1 col-xs-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<i class="fa fa-cube"></i> &nbsp
				<h3 class="box-title">Input Data Produk</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
					<i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
						<i class="fa fa-times"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<!-- success report -->
				<div id="tambah_produk_sukses" class="alert alert-success alert-dismissible pesan-sukses">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					Produk Berhasil disimpan
				</div>
				
		<form id="form_produk" method="post" enctype="multipart/form-data">			
					<div class="col-xs-12 col-md-6">
						<div class="text-danger error_message" align=center id="nama_produk_error"></div>
						<div class="form-group has-feedback">
							<input type="text" id="nama_produk" name="nama_produk" class="form-control data-produk" placeholder="Nama Produk" maxlength="35" required>
							<span class="fa fa-tag form-control-feedback"></span>
						</div>
						
						<div class="text-danger error_message" align=center id="pcode_error"></div>
						<div class="form-group has-feedback">
							<input type="text" id="pcode" name="pcode" class="form-control data-produk" placeholder="Product Code" maxlength="10" required>
							<span class="fa fa-barcode form-control-feedback"></span>
						</div>
						
						<div class="text-danger error_message" align=center id="kategori_error"></div>	
							<label>Sub kategori Produk (Kategori)</label>	
							<select name="kategori" id="kategori" class="form-control" required="true">
								<option value="">-- Pilih Kategori Produk --</option>
								<?php
									foreach($subkategori->result_array() as $cetak){
										echo "<option value='$cetak[id_subkategori]'>$cetak[nama] &nbsp ($cetak[nama_kategori])</option>";
									}
								?>
							</select>	
						
						<div class='form-group has-feedback' style="margin-top:20px">
							<input name=publish class='form-control' type='checkbox' data-onstyle=success data-offstyle=danger data-toggle=toggle data-on="<i class='fa fa-eye'></i> &nbsp Publish" data-off="<i class='fa fa-eye-slash'></i> &nbsp Unpublish">
							<span class='fa fa-globe form-control-feedback'></span>
						</div>
						
						<div class="text-danger error_message" align=center id="kandungan_error"></div>
						<div class="form-group has-feedback">
							<label>Ingredients / Kandungan Produk</label>
							<textarea name="kandungan" id="kandungan" class="form-control data-produk" rows="3" placeholder="Belum Diisi"></textarea>
							<span class='fa fa-tint form-control-feedback'></span>
						</div>
						
						<div class="text-danger error_message" align=center id="carapakai_error"></div>
						<div class="form-group has-feedback">
							<label>How to use / Cara Pakai</label>
							<textarea name="carapakai" class="form-control data-produk" rows="3" placeholder="Belum Diisi"></textarea>
							<span class='fa fa-question form-control-feedback'></span>
						</div>					
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="box box-success box-solid">
							<div class="box-header with-border">
								<i class="fa fa-image"></i> &nbsp
								<h3 class="box-title">Upload gambar produk</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
									<i class="fa fa-minus"></i></button>
									<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
									<i class="fa fa-times"></i></button>
								</div>
							</div>
							<div class="box-body">
								<div class="col-xs-12">
									<div class="alert alert-info alert-dismissible">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<h4><i class="icon fa fa-info"></i> Info</h4>
										<ul style="text-align:justify; margin-left:-20px;">
											<li>File yang dapat diupload adalah image dengan ekstensi <b>jpg | jpeg | png | bmp </b></li>
											<li>Ukuran file image yang akan disimpan sistem berukuran maksimum <b>2MB</b></li>
											<li>Rasio gambar 1:1 atau persegi</li>
										</ul>
									</div>
									<table class='table table-bordered' style='margin-top:5px;'>
										<tr>
											<td>
												<div class="text-danger error_message" align=center id="gambar_error"></div>
												<div class='input-group'>
													<span class='input-group-addon'><b>Foto 1</b></span>
													<input type='file' class="form-control data-produk" accept="image/*" name='gambar' id="gambar" onChange='checkExtension(this.value,this.id)' required>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<div class="text-danger error_message" align=center id="foto2_error"></div>
												<div class='input-group'>
													<span class='input-group-addon'><b>Foto 2</b></span>
													<input type='file' class="form-control data-produk" accept="image/*" name='foto2' id="foto2" onChange='checkExtension(this.value,this.id)' required>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<div class="text-danger error_message" align=center id="foto3_error"></div>
												<div class='input-group'>
													<span class='input-group-addon'><b>Foto 3</b></span>
													<input type='file' class="form-control data-produk" accept="image/*" name='foto3' id="foto3" onChange='checkExtension(this.value,this.id)' required>
												</div>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="text-danger error_message" align=center id="deskripsi_error"></div>
						<div class="form-group">
							<label>Deskripsi Produk</label>
							<textarea name="deskripsi" id="deskripsi" class="form-control data-produk" rows="3" placeholder="Belum Diisi"></textarea>
						</div>
					</div>
				
			</div>
			<div class="box-footer">
				<button type="submit" id="submit_produk" data-loading-text="Please wait..." class="btn btn-lg btn-primary btn-block">Submit &nbsp <i class="fa fa-check"></i></button>
			</div>
		</div>
		</form>	
	<script src="<?php echo base_url('assets/js/button-switch.js');?>"></script>
	<!-- CK Editor -->
	<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
	<script type="text/javascript">            
            // Ajax post, submit_hubungi_kami diroute ke produk/submit_hubungi_kami
        $(document).ready(function() {
			CKEDITOR.replace('deskripsi');
		
			$("#form_produk").on('submit',(function(e) {
				CKupdate();
				e.preventDefault();
				$.ajax({
					url: "<?php echo base_url('adminpage/produk/simpan_produk');?>",
					type: "POST",
					dataType: 'JSON',
					data:  new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					beforeSend : function()
					{
						$('.form-group').removeClass('has-error');	
						$('.error_message').css('display','none');	
						$('#submit_produk').button('loading');	
					},
					success: function(res)
					{
						if (res)
						{
							if(res.status) //if success tampilkan alert sukses
							{
								// munculkan pesan sukses
								$('#tambah_produk_sukses').css("display","block");
								$('#submit_produk').button('reset'); //set button enable
								$('.data-produk').val(''); //kosongin form
								
								$.alert({
									icon: 'fa fa-thumbs-o-up',
									title: 'Data produk berhasil disimpan',
									theme: 'black',
									content: false,
								});
							}
							else if(!res.status){
								for (var i = 0; i < res.inputerror.length; i++) 
								{
									$('[name="'+res.inputerror[i]+'"]').parent().addClass('has-error'); //parent div ditambahin class has error
									$('[id="'+res.inputerror[i]+'_error"]').html(res.error_string[i]); // tulis pesan error											
									$('#'+res.inputerror[i]+'_error').css("display","block"); //tampilkan pesan error									
								}
								$('#submit_produk').button('reset'); //set button enable								
							}								 
						}
					},
					error: function(e) 
					{
						$('#tambah_produk_sukses').css("display","block");
						$("#tambah_produk_sukses").html(e).fadeIn();
					} 	        
			   });
			}));
		});
		
		function CKupdate(){
			for ( instance in CKEDITOR.instances )
				CKEDITOR.instances[instance].updateElement();
		}
	</script>
      <!-- /.box -->