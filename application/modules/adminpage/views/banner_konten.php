	<link href="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.css');?>" type='text/css' rel="stylesheet">
	<link href="<?php echo base_url('assets/css/button-switch.css');?>" type='text/css' rel="stylesheet">
	<!-- DataTables -->
	<script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
	<!-- Default box -->
	<div class="col-md-10 col-md-offset-1 col-xs-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<i class="fa fa-file-image-o"></i> &nbsp
				<h3 class="box-title">Daftar Banner Web</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
					<i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
					<i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">				
				<div class="col-xs-12" style="margin-bottom:10px">
					<button class="btn btn-default" onclick="form_tambah()"><i class="fa fa-plus"></i> &nbsp Tambah Banner</button>
					<button class="btn btn-default pull-right" id="tombol_reload" onclick="reload()"><i class="fa fa-refresh"></i> &nbsp Reload Data</button>
				</div>
				<div class="col-xs-12">				
				<table id="table-banner" class="table table-bordered table-hover dt-responsive nowrap" style="width:100%">
					<thead>
					<tr class=active >
					  <td align=center>No <?php echo "&nbsp "; ?></th>
					  <td align=center>Nama Banner</th>
					  <td align=center>Gambar</th>
					  <td align=center>Publish </th>
					  <td align=center>Action</th>
					</tr>
					</thead>
					<tbody id="isi_tabel">
				<?php	
					$no=1;
					foreach($banner->result_array() as $cetak){
						$gambar_desktop=base_url("banner/$cetak[gambar_desktop]");
						$gambar_mobile=base_url("banner/$cetak[gambar_mobile]");
						echo "
						<tr>
						  <td align=center>$no</td>
						  <td >$cetak[nama_banner]</td>
						  <td align=center>
							<a href='$gambar_desktop'  data-fancybox-group='banner_image' class='btn btn-default fancybox-buttons'><i class='fa fa-desktop'></i> &nbsp desktop</a>
							<a href='$gambar_mobile'  data-fancybox-group='banner_image' class='btn btn-default fancybox-buttons'><i class='glyphicon glyphicon-phone'></i> &nbsp mobile</a>
						  </td>
						  <td align=center>$cetak[publish]</td>
						  <td align=center>
							<button type=button class='btn btn-primary' style='margin-right:5px' onclick='isi_form_edit($cetak[id_banner])'><i class='fa fa-pencil'></i> &nbsp Edit</button>
							<button type=button class='btn btn-danger' onclick=\"delete_banner('$cetak[id_banner]','$jumlah','$cetak[publish]','$cetak[nama_banner]')\"><i class='fa fa-trash'></i> &nbsp Hapus</button>
						  </td>
						</tr>
						";
						$no++;
					}
				?>
					
					</tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- modal edit dan tambah kategori -->
	<div class="modal fade" id="modal_banner" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-primary" align=center>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="modal_header">Tambah Banner</h4>
				</div>
				<form id="form_banner" method="POST" enctype="multipart/form-data">
					<input type="hidden" name="action" id="action" />
					<input type="hidden" name="id_banner" id="id_banner" />
					<div class="modal-body">
						<div class="row">									
							<div class="col-xs-12 col-md-10 col-md-offset-1">
								<div class="alert alert-info alert-dismissible">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<ul style="text-align:justify; margin-left:-20px;">
										<li>File yang dapat diupload adalah image dengan ekstensi <b>jpg | jpeg | png | bmp </b></li>
										<li>Ukuran file image yang akan disimpan sistem berukuran maksimum <b>1MB</b></li>
										<li>Dalam satu banner terdapat dua versi yaitu desktop dan mobile.</li>
										<li>Banner Desktop berukuran 1440 x 588 pixel</li>
										<li>Banner Mobile berukuran 408 x 408 pixel</li>
										<li>Kosongkan gambar jika tidak merubah foto</li>
									</ul>
								</div>
								
								<div class="text-danger error_message" align=center id="nama_banner_error"></div>
								<div class="form-group has-feedback">
									<input type="text" id="nama_banner" name="nama_banner" class="form-control data-banner" placeholder="Nama Banner" maxlength="35" required />
									<span class="fa fa-file-image-o form-control-feedback"></span>
								</div>
								
								<label id=status_publish style="margin-top:20px"> </label>
								<div class='form-group has-feedback' id="status_publish">
									<input name="publish" class='form-control' type='checkbox' data-onstyle=success data-offstyle=danger data-toggle=toggle data-on="<i class='fa fa-eye'></i> &nbsp Publish" data-off="<i class='fa fa-eye-slash'></i> &nbsp Unpublish">
									<span class='fa fa-globe form-control-feedback'></span>
								</div>
								
								<div class="text-danger error_message" align=center id="gambar_desktop_error"></div>
								<div class='form-group input-group'>
									<span class='input-group-addon'><b>Desktop</b></span>
									<input type='file' class="form-control data-banner" accept="image/*" name='gambar_desktop' id="gambar" onChange='checkExtension(this.value,this.id)' />
								</div>
								
								<div class="text-danger error_message" align=center id="gambar_mobile_error"></div>
								<div class='form-group input-group'>
									<span class='input-group-addon'><b>Mobile</b></span>
									<input type='file' class="form-control data-banner" accept="image/*" name='gambar_mobile' id="gambar" onChange='checkExtension(this.value,this.id)' />
								</div>
								
								<div class="text-danger error_message" align=center id="link_banner_error" style="margin-top:10px"></div>
								<div class="form-group has-feedback">
									<input type="text" id="link_banner" name="link_banner" class="form-control data-banner" placeholder="Link Banner" value="http://" required />
									<span class="fa fa-link form-control-feedback"></span>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" id="submit_banner" class="btn btn-primary btn-block btn-lg" style="font-size:large" data-loading-text="Uploading Please wait..." ><i class="fa fa-check"></i> &nbsp Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script src="<?php echo base_url('assets/js/button-switch.js');?>"></script>
	<script type="text/javascript">    
		//reload data tables
        function reload(){
			$.ajax({
					url: "<?php echo base_url('adminpage/banner/reload');?>",
					type: "POST",
					dataType: 'html',
					beforeSend : function()
					{	
						$('#tombol_reload').button('loading');	
					},
					success: function(res)
					{
						$('#isi_tabel').html(res);
						$('#tombol_reload').button('reset');
					},
					error: function(e) 
					{
						alert(e);
					} 	        
			   });
		}
		
		function form_tambah(){
			$('#action').val('tambah');// set action form add kategori
			$('#status_publish').css('display','none');
			
			//clear modal
			$('#id_banner').val('');
			$('#nama_banner').val('');
			$('#link_banner').val('');
			$('#gambar').val('');
			$("#gambar").prop('required',true);
			$('.form-group').removeClass('has-error');	
			$('.error_message').css('display','none');
			$('#modal_banner').modal('show');
		}
		
		function isi_form_edit(id){
			$('.form-group').removeClass('has-error');	
			$('.error_message').css('display','none');
			$("#gambar").prop('required',false);
			$('#action').val('edit');// set action form add kategori
			$('#status_publish').css('display','block');
			$.ajax({
					url: "<?php echo base_url('adminpage/banner/banner_by_id');?>",
					type: "POST",
					dataType: 'json',
					data:  {id:id},
					success: function(res)
					{
						$('#modal_header').html('Update banner '+res.nama_banner);
						$('#id_banner').val(res.id_banner);
						$('#nama_banner').val(res.nama_banner);
						$('#link_banner').val(res.link_banner);
						if(res.publish=='Y'){
							$('#status_publish').html('Periksa kembali status publish, data sebelumnya adalah <font color=green>'+res.publish+'</font>');
						}else{
							$('#status_publish').html('Periksa kembali status publish, data sebelumnya adalah <font color=red>'+res.publish+'</font>');
						}
						
						$('#modal_banner').modal('show');
					},
					error: function(e) 
					{
						alert(e);
					} 	        
			});
		}
		
		// allow all extensions
		var exts = "jpg|jpeg|png|bmp";
		// only allow specific extensions
		// var exts = "jpg|jpeg|gif|png|bmp|tiff|pdf";
		function checkExtension(value,id)
		{
			if(value=="")return true;
			var re = new RegExp("^.+\.("+exts+")$","i");
			if(!re.test(value))
			{
				$.alert({
					title: 'Ekstensi file tidak di izinkan',
					icon: 'fa fa-warning',
					theme: 'black',
					content: 'Jenis file yang di izinkan: &nbsp <b>'+exts.replace(/\|/g,',')+'</b>',
					backgroundDismiss: true,
					confirmButton: false,
					cancelButton: false
				});
				
				$('#'+id).val('');
				$('#'+id).parent().addClass('has-error');
			}else{
				$('#'+id).parent().removeClass('has-error');
			}
		}           
            // Ajax post, submit_hubungi_kami diroute ke produk/submit_hubungi_kami
        $(document).ready(function() {
		
			$("#form_banner").on('submit',(function(e) {
				var action=$('#action').val();
				var jml=$('#jumlah_banner_aktif').val();
				if(action=='edit' && jml<=2){
					$.alert({
						title: 'Ekstensi file tidak di izinkan',
						icon: 'fa fa-warning',
						theme: 'black',
						content: 'Jenis file yang di izinkan: &nbsp <b>'+exts.replace(/\|/g,',')+'</b>',
						backgroundDismiss: true,
						confirmButton: false,
						cancelButton: false
					});
				}
				e.preventDefault();
				$.ajax({
					url: "<?php echo base_url('adminpage/banner/simpan');?>",
					type: "POST",
					dataType: 'JSON',
					data:  new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					beforeSend : function()
					{
						$('.form-group').removeClass('has-error');	
						$('.error_message').css('display','none');	
						$('#submit_banner').button('loading');	
					},
					success: function(res)
					{
						if (res)
						{
							if(res.status) //if success tampilkan alert sukses
							{
								$('#submit_banner').button('reset'); //set button enable
								$('.data-banner').val(''); //kosongin form
								reload();
								$('#modal_banner').modal('hide');
								$.alert({
									icon: 'fa fa-thumbs-o-up',
									title: 'Data banner berhasil disimpan',
									theme: 'black',
									content: false,
								});
							}
							else if(!res.status){
								for (var i = 0; i < res.inputerror.length; i++) 
								{
									$('[name="'+res.inputerror[i]+'"]').parent().addClass('has-error'); //parent div ditambahin class has error
									$('[id="'+res.inputerror[i]+'_error"]').html(res.error_string[i]); // tulis pesan error											
									$('#'+res.inputerror[i]+'_error').css("display","block"); //tampilkan pesan error									
								}
								$('#submit_banner').button('reset'); //set button enable								
							}								 
						}
					},
					error: function(e) 
					{
						alert(e);
					} 	        
			   });
			}));
		});
		
		$('.fancybox-buttons').fancybox({
				openEffect  : 'elastic',
				closeEffect : 'fade',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
		});
		
		function delete_banner(id,jumlah_banner,publish, nama_banner)
		{
			if(jumlah_banner<=2 && publish=='Y'){
				$.alert({
					icon: 'fa fa-warning',
					title: 'Warning !',
					content: '<h4 align=justify>Jumlah Banner yang terpublish <font color=red> minimum 2 banner </font></h4>',
					theme: 'material',
				});
			}else{
				$.confirm({
					title: 'Yakin hapus banner <font color=red>'+nama_banner+'</font> ?',
					content: false,
					confirmButtonClass: 'btn-danger',
					cancelButtonClass: 'btn-success',
					confirmButton: 'Ya',
					cancelButton: 'Cancel',
					theme: 'material',
					confirm: function(){
						//ajax delete data to database
						$.ajax({
							url : "<?php echo site_url('adminpage/banner/delete_banner')?>",
							type: "POST",
							dataType: "JSON",
							data: {id: id},
							success: function(res)
							{
								if(res.status){
									reload();
								}									
							},
							error: function (jqXHR, textStatus, errorThrown)
							{
								alert('Error deleting data');
							}
						});
					}
				});
			}
		}
			
			$(function () {
			$('#table-banner').DataTable({
				"columnDefs": [
				{ "orderable": false, "targets": 2 },
				{ "orderable": false, "targets": 4 },
			  ]
			});
				
		  });
	</script>
	