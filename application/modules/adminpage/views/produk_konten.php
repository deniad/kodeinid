<link href="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.css');?>" type='text/css' rel="stylesheet">
<link href="<?php echo base_url('assets/css/button-switch.css');?>" type='text/css' rel="stylesheet">
<!--
<link href="<?php echo base_url('assets/css/responsive.bootstrap.min.css');?>" type='text/css' rel="stylesheet">
-->
		
	<!-- Default box -->
	<div class="col-xs-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<i class="fa fa-list"></i> &nbsp
				<h3 class="box-title">Daftar Produk Herborist</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
					<i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
					<i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">				
				<div class="col-xs-12" style="margin-bottom:10px">
					<h4 class="pull-left">	
						<i class="fa fa-globe"></i> : Publish status &nbsp; &nbsp; 
						<i class="fa fa-eye"></i> : Viewer &nbsp; &nbsp; 						
						<i class="fa fa-search text-success"></i> : Detail &nbsp; &nbsp; 
						<i class="fa fa-pencil text-primary"></i> : Edit &nbsp; &nbsp;
						<i class="fa fa-trash text-danger"></i> : Hapus &nbsp; &nbsp;
					</h4>
					<button class="btn btn-info pull-right" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> &nbsp Reload Data</button>
				</div>
				<div class="col-xs-12">
				<table id="table-produk" class="table table-bordered table-hover dt-responsive nowrap" style="width:100%">
					<thead>
					<tr>
					  <th>PCode</th>
					  <th>Nama Produk</th>
					  <th>Kategori</th>
					  <th>Sub Kategori</th>
					  <th><i class="fa fa-eye"></i></th>
					  <th><i class="fa fa-globe"></i></th>
					  <th>Action</th>
					</tr>
					</thead>
					<tbody>
					
					</tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
	
	
	<script type="text/javascript">          
			var table;
			function reload_table()
			{
				table.ajax.reload(null,false); //reload datatable ajax 
			}
			
            // Ajax post, submit_hubungi_kami diroute ke produk/submit_hubungi_kami
            $(document).ready(function() {
				//CKEDITOR.replace('deskripsi');
				
				//datatables ajax
				table = $('#table-produk').DataTable({ 
				//	"responsive": true,
					"processing": true, //Feature control the processing indicator.
					"serverSide": true, //Feature control DataTables' server-side processing mode.
					"order": [], //Initial no order.
			 
					// Load data for the table's content from an Ajax source
					"ajax": {
						"url": "<?php echo site_url('adminpage/produk/produk_list')?>",
						"type": "POST"
					},
			 
					//Set column definition initialisation properties.
					"columnDefs": [
					{ 
						"targets": [ -1 ], //last column
						"orderable": false, //set not orderable
					},
					],
					"order": [[ 0, "desc" ]],
				});				
            });
			
			function delete_produk(id,nama_produk)
			{
					$.confirm({
						title: 'Yakin hapus <font color=red>'+nama_produk+'</font> ?',
						content: false,
						confirmButtonClass: 'btn-danger',
						cancelButtonClass: 'btn-success',
						confirmButton: 'Ya',
						cancelButton: 'Cancel',
						theme: 'material',
						confirm: function(){
							// ajax delete data to database
							$.ajax({
								url : "<?php echo site_url('adminpage/produk/delete_produk')?>",
								type: "POST",
								dataType: "JSON",
								data: {id: id},
								success: function(res)
								{
									if(res.status){
										reload_table();
									}									
								},
								error: function (jqXHR, textStatus, errorThrown)
								{
									alert('Error deleting data');
								}
							});
						}
					});
			}
			
			function edit_produk(id)
			{						
				//Ajax Load data from ajax untuk mengisi modal edit dgn data di database
				$.ajax({
					url : "<?php echo site_url('adminpage/produk/ajax_edit/')?>",
					type: "POST",
					dataType: "JSON",
					data: {id: id},
					success: function(res)
					{
						$('[name="id_produk"]').val(res.id_produk);
						$('[name="nama_produk"]').val(res.nama_produk);
						$('[name="pcode"]').val(res.pcode);
						
												
						$('[name="kandungan"]').val(res.kandungan);
						$('[name="carapakai"]').val(res.cara_pakai);
						CKEDITOR.replace('deskripsi');
						$('[name="deskripsi"]').val(res.deskripsi_produk);
						$('#modal_form').modal('show'); // show bootstrap modal when complete loaded
						$('.modal-title').text('Edit '+res.nama_produk); // Set title to Bootstrap modal title

					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						alert('Error get data from ajax');
					}
				});
			}
			
			function detail_produk(id)
			{						
				//Ajax Load data from ajax untuk mengisi modal edit dgn data di database
				$.ajax({
					url : "<?php echo site_url('adminpage/produk/ajax_detail/')?>",
					type: "POST",
					dataType: "html",
					data: {id: id},
					success: function(res)
					{
						$('#body-modal-detail').html(res);
						$('#modal_detail').modal('show');
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						alert('Error get data from ajax');
					}
				});
			}
			
			$('.fancybox-buttons').fancybox({
				openEffect  : 'elastic',
				closeEffect : 'fade',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});
			
	</script>
	
	<!-- DataTables -->
	<script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/button-switch.js');?>"></script>
	<!-- CK Editor -->
	<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<!--	
	<script src="<?php echo base_url('assets/js/dataTables.responsive.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/responsive.bootstrap.min.js');?>"></script>
-->
	
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_detail" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="title_detail" align=center><i class="fa fa-search"></i> &nbsp Detail Produk</h3>
            </div>
            <div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-md-10 col-md-offset-1" id="body-modal-detail">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>