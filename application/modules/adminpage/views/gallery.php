	<link href="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.css');?>" type='text/css' rel="stylesheet">
	<link href="<?php echo base_url('assets/css/button-switch.css');?>" type='text/css' rel="stylesheet">
	<!-- DataTables -->
	<script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
	<!-- Default box -->
	<div class="col-md-10 col-md-offset-1 col-xs-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<i class="fa fa-image"></i> &nbsp
				<h3 class="box-title">Manage Konten Homepage</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
					<i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
					<i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">				
				<div class="col-xs-12" style="margin-bottom:10px">
					<img class="img-rounded" src='http://localhost/ci_herborist/images/layout-gallery.jpg' />
				</div>
				<div class="col-xs-12">	
				<div class="alert alert-info alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<ul style="text-align:justify; margin-left:-20px;">
						<li>File harus ber ekstensi <b>jpg | jpeg | png | bmp </b></li>
						<li>Ukuran file image yang akan disimpan sistem berukuran maksimum <b>1MB</b></li>
						<li>Kosongkan gambar jika tidak merubah foto</li>
					</ul>
				</div>
				<table id="table-gallery" class="table table-bordered table-hover dt-responsive nowrap" style="width:100%">
					<thead>
					<tr class=active >
					  <td align=center>Judul Gallery</th>
					  <td align=center>Gambar</th>
					  <td align=center>Action</th>
					</tr>
					</thead>
					<tbody id="isi_tabel">
					<?php
					foreach($gallery->result_array() as $cetak){					
						$mobile="";
						if($cetak['gambar_desktop']==''){
							$gambar_desktop=base_url("banner/no-image.jpg");
						}else{
							$gambar_desktop=base_url("banner/$cetak[gambar_desktop]");
						}
						
						if ($cetak['id_gallery']=='4' || $cetak['id_gallery']=='3'){														
							if($cetak['gambar_mobile']==''){
								$gambar_mobile=base_url("banner/no-image.jpg");
							}else{
								$gambar_mobile=base_url("banner/$cetak[gambar_mobile]");
							}
							$mobile="<a href='$gambar_mobile'  data-fancybox-group='banner_mobile' class='btn btn-default fancybox-buttons'><i class='glyphicon glyphicon-phone'></i> &nbsp mobile</a>";
						}
						echo "
						<tr>
							<td>$cetak[judul]</td>
							<td align=center>
								<a href='$gambar_desktop'  data-fancybox-group='banner_image' class='btn btn-default fancybox-buttons'><i class='fa fa-desktop'></i> &nbsp desktop</a>
								$mobile
							</td>
							<td align=center>
								<button type=button class='btn btn-primary' style='margin-right:5px' onclick='isi_form_edit($cetak[id_gallery])'><i class='fa fa-pencil'></i> &nbsp Edit</button>
							</td>							
						</tr>
						";
					}
					?>
					</tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- modal edit dan tambah kategori -->
	<div class="modal fade" id="modal_gallery" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-primary" align=center>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="modal_header"></h4>
				</div>
				<form id="form_banner" method="POST" enctype="multipart/form-data">
					<input type="hidden" name="id_gallery" id="id_gallery" />
					<div class="modal-body">
						<div class="row">									
							<div class="col-xs-12 col-md-10 col-md-offset-1">
								<div class="alert alert-info alert-dismissible">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<ul style="text-align:justify; margin-left:-20px;">
										<li>File harus ber ekstensi <b>jpg | jpeg | png | bmp </b></li>
										<li>Ukuran file image yang akan disimpan sistem berukuran maksimum <b>1MB</b></li>
										<li>Kosongkan gambar jika tidak merubah foto</li>
										<div id="rules"></div>
									</ul>
								</div>
								
								<div class="form-group has-feedback">
									<input type="text" id="judul" name="judul" class="form-control data-gallery" placeholder="Judul" maxlength="25" disabled=true />
									<span class="fa fa-file-image-o form-control-feedback"></span>
								</div>								
								
								<div class="text-danger error_message" align=center id="gambar_desktop_error"></div>
								<div class='form-group input-group'>
									<span class='input-group-addon'><b>Desktop</b></span>
									<input type='file' class="form-control data-gallery" accept="image/*" name='gambar_desktop' onChange='checkExtension(this.value,this.id)' />
								</div>
								
								<div id='file-mobile'>
									<div class="text-danger error_message" align=center id="gambar_mobile_error"></div>
									<div class='form-group input-group'>
										<span class='input-group-addon'><b>Mobile</b></span>
										<input type='file' class="form-control data-gallery" accept="image/*" name='gambar_mobile' id="gambar" onChange='checkExtension(this.value,this.id)' />
									</div>
								</div>
								
								<div class="text-danger error_message" align=center id="link_gallery_error" style="margin-top:10px"></div>
								<div class="form-group has-feedback">
									<input type="text" id="link_gallery" name="link_gallery" class="form-control data-gallery" placeholder="Link Banner contoh http://vci.co.id" value="http://" required />
									<span class="fa fa-link form-control-feedback"></span>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" id="submit_gallery" class="btn btn-primary btn-block btn-lg" style="font-size:large" data-loading-text="Please wait..." ><i class="fa fa-check"></i> &nbsp Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script src="<?php echo base_url('assets/js/button-switch.js');?>"></script>
	<script type="text/javascript">    
		//reload data tables
        function reload(){
			$.ajax({
					url: "<?php echo base_url('adminpage/gallery/reload');?>",
					type: "POST",
					dataType: 'html',
					beforeSend : function()
					{	
						$('#tombol_reload').button('loading');	
					},
					success: function(res)
					{
						$('#isi_tabel').html(res);
						$('#tombol_reload').button('reset');
					},
					error: function(e) 
					{
						alert(e);
					} 	        
			   });
		}
		
		
		function isi_form_edit(id){
			$('.form-group').removeClass('has-error');	
			$('.error_message').css('display','none');
			$("#gambar").prop('required',false);
			$('#action').val('edit');// set action form add kategori
			$('#status_publish').css('display','block');
			if(id =='3'|| id =='4'){				
				$('#file-mobile').css('display','block');
			}else{
				$('#file-mobile').css('display','none');
			}
			$.ajax({
				url: "<?php echo base_url('adminpage/gallery/gallery_by_id');?>",
				type: "POST",
				dataType: 'json',
				data:  {id:id},
				success: function(res)
				{
					$('#modal_header').html('Update Gallery '+res.judul);
					$('#id_gallery').val(res.id_gallery);
					$('#judul').val(res.judul);	
					$('#link_gallery').val(res.link_gallery);
					$('#rules').html(res.rules);						
					$('#modal_gallery').modal('show');
				},
				error: function(e) 
				{
					alert(e);
				} 	        
			});
		}
		
		// allow all extensions
		var exts = "jpg|jpeg|png|bmp";
		// only allow specific extensions
		// var exts = "jpg|jpeg|gif|png|bmp|tiff|pdf";
		function checkExtension(value,id)
		{
			if(value=="")return true;
			var re = new RegExp("^.+\.("+exts+")$","i");
			if(!re.test(value))
			{
				$.alert({
					title: 'Ekstensi file tidak di izinkan',
					icon: 'fa fa-warning',
					theme: 'black',
					content: 'Jenis file yang di izinkan: &nbsp <b>'+exts.replace(/\|/g,',')+'</b>',
					backgroundDismiss: true,
					confirmButton: false,
					cancelButton: false
				});
				
				$('#'+id).val('');
				$('#'+id).parent().addClass('has-error');
			}else{
				$('#'+id).parent().removeClass('has-error');
			}
		}           
            // Ajax post, submit_hubungi_kami diroute ke produk/submit_hubungi_kami
        $(document).ready(function() {
		
			$("#form_banner").on('submit',(function(e) {
				var action=$('#action').val();
				var jml=$('#jumlah_banner_aktif').val();
				if(action=='edit' && jml<=2){
					$.alert({
						title: 'Ekstensi file tidak di izinkan',
						icon: 'fa fa-warning',
						theme: 'black',
						content: 'Jenis file yang di izinkan: &nbsp <b>'+exts.replace(/\|/g,',')+'</b>',
						backgroundDismiss: true,
						confirmButton: false,
						cancelButton: false
					});
				}
				e.preventDefault();
				$.ajax({
					url: "<?php echo base_url('adminpage/gallery/simpan');?>",
					type: "POST",
					dataType: 'JSON',
					data:  new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					beforeSend : function()
					{
						$('.form-group').removeClass('has-error');	
						$('.error_message').css('display','none');	
						$('#submit_gallery').button('loading');	
					},
					success: function(res)
					{
						if (res)
						{
							if(res.status) //if success tampilkan alert sukses
							{
								$('#submit_gallery').button('reset'); //set button enable
								$('.data-gallery').val(''); //kosongin form
								reload();
								$('#modal_gallery').modal('hide');
								$.alert({
									icon: 'fa fa-thumbs-o-up',
									title: 'Data banner berhasil disimpan',
									theme: 'black',
									content: false,
								});
							}
							else if(!res.status){
								for (var i = 0; i < res.inputerror.length; i++) 
								{
									$('[name="'+res.inputerror[i]+'"]').parent().addClass('has-error'); //parent div ditambahin class has error
									$('[id="'+res.inputerror[i]+'_error"]').html(res.error_string[i]); // tulis pesan error											
									$('#'+res.inputerror[i]+'_error').css("display","block"); //tampilkan pesan error									
								}
								$('#submit_gallery').button('reset'); //set button enable								
							}								 
						}
					},
					error: function(e) 
					{
						alert(e);
					} 	        
			   });
			}));
		});
		
		$('.fancybox-buttons').fancybox({
				openEffect  : 'elastic',
				closeEffect : 'fade',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
		});
		
				
			$(function () {
			$('#table-gallery').DataTable({
				"searching": false,
				"paginate": false,
				"columnDefs": [
					{ "orderable": false, "targets": 2 },
					{ "orderable": false, "targets": 1 }
				]
			});
				
		  });
	</script>
	