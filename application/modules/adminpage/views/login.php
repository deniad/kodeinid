<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">-->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style
  <link rel="stylesheet" href="../../css/AdminLTE.min.css"> -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/AdminLTE.css'); ?>">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  .login-box{
	  width:300px;
  }
  </style>
</head>

<body class="hold-transition login-page">
<script src="https://www.gstatic.com/firebasejs/3.6.4/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyCnVIfc-Kf4dWBY4hSLI7F8g2JXYTMhRb8",
    authDomain: "kodein-1479046554109.firebaseapp.com",
    databaseURL: "https://kodein-1479046554109.firebaseio.com",
    storageBucket: "kodein-1479046554109.appspot.com",
    messagingSenderId: "616115798122"
  };
  firebase.initializeApp(config);
</script>


<div class="login-box">
  <div class="login-logo">
    <a href="http://herborist.co.id/">Login <b>Admin</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
  
  <form id="form_login">
    <div class="text-danger error_message" align=center id="user_email_error" style="display:none;"></div>
      <div class="form-group has-feedback">
        <input type="text" id="user_email" name="user_email" class="form-control" placeholder="Username or Email" maxlength="50">
        <span class="fa fa-user form-control-feedback"></span>
      </div>
    <div class="text-danger error_message" align=center id="password_error" style="display:none;"></div>
      <div class="form-group has-feedback">
        <input type="password" id="password" name="password" class="form-control" placeholder="Password" maxlength="25">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-12">
          <button type="submit" id="tombol_login" data-loading-text='Please Wait...' class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div> </br>
   </form>

    <a href="#">I forgot my password</a>

  </div>
  <!-- /.login-box-body -->
</div>


<!--
<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo base_url(); ?>">Login <b>Admin</b></a>
  </div>
  <!-- /.login-logo 
  	<div class="login-box-body">
  		<div class="text-danger error_message" align=center id="error_message" style="display:none;"></div>
   	 	<p class="login-box-msg">Select which ones you like to sign in</p>	
    	<button id="facebook_button" class="btn btn-block btn-social btn-facebook" onclick="toggleSignIn('facebook')">
            <i class="fa fa-facebook"></i> Sign in with Facebook
        </button>
        </br>
      	<button id="google_button" class="btn btn-block btn-social btn-google" onclick="toggleSignIn('google')">
            <i class="fa fa-google-plus"></i> Sign in with Google
        </button>
        </br>
        <button disabled=true class="btn btn-block btn-social btn-twitter" id="google-signin-button" onclick="toggleSignIn('twitter')">
            <i class="fa fa-twitter"></i> Sign in with Twitter
        </button>
        </br>
        <button class="btn btn-block btn-social btn-github" id="google-signin-button" onclick="toggleSignIn('github')">
            <i class="fa fa-github"></i> Sign in with Github
        </button>
        </br>
        <button class="btn btn-block btn-social btn-success" onclick="toggleSignIn('email')">
            <i class="fa fa-envelope"></i> Sign in with Email
        </button>
    </div>
    </br>
	 

    <button disabled class="mdl-button mdl-js-button mdl-button--raised" id="quickstart-sign-in">Sign in with Google</button>
    <!-- Container where we'll display the user details 
        <div class="quickstart-user-details-container">
            Firebase sign-in status: <span id="quickstart-sign-in-status">Unknown</span>
            <div>Firebase auth <code>currentUser</code> object value:</div>
            <pre><code id="quickstart-account-details">null</code></pre>
            <div>Google OAuth Access Token:</div>
            <pre><code id="quickstart-oauthtoken">null</code></pre>
  		</div>
  <!-- /.login-box-body
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>	
<!-- Bootstrap 3.3.6
<script src="../../bootstrap/js/bootstrap.min.js"></script> -->
<script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>	
<!-- iCheck 
<script src="../../plugins/iCheck/icheck.min.js"></script>-->

<script type="text/javascript">
            
            // Ajax post, submit_hubungi_kami diroute ke produk/submit_hubungi_kami
            $(document).ready(function() {
				
							
				$( "#form_login" ).submit(function( event ) {
					event.preventDefault();
					var user_email = $('#user_email').val();
					var password = $('#password').val();
					jQuery.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>" + "adminpage/login/aksi_login",
						dataType: 'JSON',
						data: {user_email: user_email, password: password},
						beforeSend: function() {
							$('#tombol_login').button('loading');
							$('.error_message').css("display","none");
							$('#user_email').parent().removeClass('has-error');
							$('#password').parent().removeClass('has-error');
						},
						success: function(res) {
							if (res)
							{
								if(res.status === true) //if success tampilkan alert sukses
								{
									// redirect ke halaman admin
									window.location.replace("<?php echo base_url('adminpage');?>");
								}
								else if(res.status === false){
									for (var i = 0; i < res.inputerror.length; i++) 
									{
										$('[name="'+res.inputerror[i]+'"]').parent().addClass('has-error'); //parent div ditambahin class has error
										$('[id="'+res.inputerror[i]+'_error"]').html(res.error_string[i]); // tulis pesan error											
										$('#'+res.inputerror[i]+'_error').css("display","block"); //tampilkan pesan error
									}
									$('#tombol_login').button('reset'); //set button enable
								}else if(res.status == 'wrong_username_pass'){
									$('#user_email_error').html("Username/email atau password salah");
									$('#user_email').parent().addClass('has-error');
									$('#password').parent().addClass('has-error');
									$('#user_email_error').css("display","block");
									$('#tombol_login').button('reset'); //set button enable
								}								 
							}							
						}									
					});
				});				
            });
</script>


<script type="text/javascript">
	
    /**
     * Function called when clicking the Login/Logout button.
     */
    // [START buttoncallback]
    function toggleSignIn(provider) {
      if (!firebase.auth().currentUser) {
        // [START createprovider]
        if(provider=='google'){
	        var provider = new firebase.auth.GoogleAuthProvider();
	        // [END createprovider]
	        // [START addscopes]
	        provider.addScope('https://www.googleapis.com/auth/plus.login');
    	}
    	else if(provider=='facebook'){
	    	var provider = new firebase.auth.FacebookAuthProvider();
	        // [END createprovider]
	        // [START addscopes]
	        provider.addScope('public_profile');        
    	}
    	else if(provider=='twitter'){
	    	var provider = new firebase.auth.FacebookAuthProvider();
	        // [END createprovider]
	        // [START addscopes]
	        provider.addScope('public_profile');        
    	}
    	else if(provider=='github'){
	    	var provider = new firebase.auth.GithubAuthProvider();
	        // [END createprovider]
	        // [START addscopes]
	        provider.addScope('user:email');      
    	}
        // [END addscopes]
        // [START signin]
        firebase.auth().signInWithPopup(provider).then(function(result) {
        // This gives you a Google Access Token. You can use it to access the Google API.
          var token = result.credential.accessToken;
          // The signed-in user info.
          var user = result.user;
          // [START_EXCLUDE]
          document.getElementById('quickstart-oauthtoken').textContent = token;
          // [END_EXCLUDE]
        }).catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          // The email of the user's account used.
          var email = error.email;
          // The firebase.auth.AuthCredential type that was used.
          var credential = error.credential;
          // [START_EXCLUDE]
          if (errorCode === 'auth/account-exists-with-different-credential') {
            alert('You have already signed up with a different auth provider for that email.');
            // If you are using multiple auth providers on your app you should handle linking
            // the user's accounts here.
          } else {
            console.error(error);
          }
          // [END_EXCLUDE]
        });
        // [END signin]
      } else {
        // [START signout]
        firebase.auth().signOut();
        // [END signout]
      }
      // [START_EXCLUDE]
      document.getElementById('quickstart-sign-in').disabled = true;
      // [END_EXCLUDE]
    }
    // [END buttoncallback]

function initApp() {
      // Listening for auth state changes.
      // [START authstatelistener]
      firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          // User is signed in.
          var displayName = user.displayName;
          var email = user.email;
          var emailVerified = user.emailVerified;
          var photoURL = user.photoURL;
          var isAnonymous = user.isAnonymous;
          var uid = user.uid;
          var provider1 = user.providerData;
          var token=user.getToken();
          user.providerData.forEach(function (profile) {
		    //alert(profile.providerId);
		    ajax_login(email, uid, profile.providerId);
		  });
         firebase.auth().currentUser.getToken().then(function(data) {
		    console.log(data)
		});
         //	alert(provider_data.provider); 
          //ajax_login(email, uid, provider);
          // [START_EXCLUDE]
          document.getElementById('quickstart-sign-in-status').textContent = 'Signed in';
          document.getElementById('quickstart-sign-in').textContent = 'Sign out';
          document.getElementById('quickstart-account-details').textContent = JSON.stringify(user, null, '  ');

          // [END_EXCLUDE]
        } else {
          // User is signed out.
          // [START_EXCLUDE]
          document.getElementById('quickstart-sign-in-status').textContent = 'Signed out';
          document.getElementById('quickstart-sign-in').textContent = 'Sign in with Google';
          document.getElementById('quickstart-account-details').textContent = 'null';
          document.getElementById('quickstart-oauthtoken').textContent = 'null';
          // [END_EXCLUDE]
        }
        // [START_EXCLUDE]
        document.getElementById('quickstart-sign-in').disabled = false;
        // [END_EXCLUDE]
      });
      // [END authstatelistener]

      document.getElementById('quickstart-sign-in').addEventListener('click', toggleSignIn, false);
    }

    window.onload = function() {
      initApp();
    };
/*
    function ajax_login(email, uid, provider){
		var user_email = email;
		var uid = uid;
		jQuery.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>" + "adminpage/login/login_sosmed",
			dataType: 'JSON',
			data: {user_email: user_email, uid: uid, provider:provider},
			beforeSend: function() {
				$('#'+provider+'_button').button('loading');
				$('.error_message').css("display","none");
			},
			success: function(res) {
				if (res)
				{
					if(res.status === true) //if success tampilkan alert sukses
					{
						// redirect ke halaman admin
						window.location.replace("<?php echo base_url('adminpage');?>");
					}else if(res.status == 'not_found'){
						firebase.auth().signOut();
						$('#error_nessage').html("Tidak Terdaftar");
						$('#error_nessage').css('display','none')
						$('#'+provider+'_button').button('reset'); //set button enable
					}								 
				}							
			}									
		});		
    };*/
  </script>
</body>
</html>
