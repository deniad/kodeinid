<!doctype html>
<!--
  Copyright 2015 Google Inc. All rights reserved.
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
      https://www.apache.org/licenses/LICENSE-2.0
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Monitor transaksi rekening jadi lebih mudah">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="utf-8">
  <meta property='og:title' content='Kodein Indonesia' />     
  <meta property='og:site_name'  content='Kodein.id' />
  <meta property='og:image' content='https://kodein.id/images/kodein/kodein-cover.jpg' />
  <meta property='og:description' content='Monitor transaksi rekening jadi lebih mudah' />
  <meta property='og:url' content='https://kodein.id/kbank' />
  <link rel='canonical' href='https://kodein.id/kbank' />
  <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" type='text/css' rel="stylesheet">
  <link href="<?php echo base_url('assets/css/AdminLTE.css');?>" type='text/css' rel="stylesheet">
  <title>K-Bank</title>

  <!-- Disable tap highlight on IE -->
  <meta name="msapplication-tap-highlight" content="no">

  <!-- Web Application Manifest 
  <link rel="manifest" href="manifest.json">
  -->

  <!-- Add to homescreen for Chrome on Android -->
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="application-name" content="Friendly Chat">
  <meta name="theme-color" content="#303F9F">

  <!-- Add to homescreen for Safari on iOS -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
  <meta name="apple-mobile-web-app-title" content="Friendly Chat">
  <meta name="apple-mobile-web-app-status-bar-style" content="#303F9F">

  <!-- Tile icon for Win8 -->
  <meta name="msapplication-TileColor" content="#3372DF">
  <meta name="msapplication-navbutton-color" content="#303F9F">

  <!-- Material Design Lite -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.orange-indigo.min.css">
  <script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>

  <!-- App Styling -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
  <link rel="stylesheet" href="<?=base_url()?>assets/kbank/styles/main.css">

  <!-- Bootstrap -->
  <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/Adminlte.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

  <script src="https://www.gstatic.com/firebasejs/3.6.4/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyCnVIfc-Kf4dWBY4hSLI7F8g2JXYTMhRb8",
    authDomain: "kodein-1479046554109.firebaseapp.com",
    databaseURL: "https://kodein-1479046554109.firebaseio.com",
    storageBucket: "kodein-1479046554109.appspot.com",
    messagingSenderId: "616115798122"
  };
  firebase.initializeApp(config);
</script>
<style>
@media screen and (min-width: 480px){
.mdl-grid {
    padding: 2px;
}
.header-kbank{
  margin-left: 50px;

}
}
@media screen and (max-width: 760px){
  .mdl-layout__header-row {
    height: 50px;
  }
  .header-kbank{
      /*margin-left: auto;
      margin-right: auto;*/
      margin:auto;
  }
}

.menu-sidebar{
    font-size: 30px;
    background: transparent;
    color: white;

    margin-left: -10px;
    margin-top: auto;
    margin-bottom: auto;
}
</style>
</head>
<body>
<div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-header">

  <!-- Header section containing logo -->
  <header class="mdl-layout__header mdl-color-text--white mdl-color--light-blue-700">
    <div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet mdl-grid" style='padding:1px;'>
      <div class="mdl-layout__header-row mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
        <button class="btn pull-left menu-sidebar" data-toggle="modal" data-target=".side_bar_modal">
                    <i class="fa fa-bars"></i>
                </button>
        <h2 class='header-kbank' style="margin-bottom: auto;
  margin-top: auto;"><i class="fa fa-foursquare"></i><b>inbook</b></h2>
<!--
  <button class="btn pull-right menu-sidebar" data-toggle="modal" data-target=".side_bar_modal">
                    <i class="fa fa-user"></i>
                </button>
-->
      </div>
      <div id="user-container">
        <div hidden id="user-pic"></div>
        <div hidden id="user-name"></div>
        <button hidden id="sign-out" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-color-text--white">
          Sign-out
        </button>
        <button hidden id="sign-in" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-color-text--white">
          <i class="material-icons">account_circle</i>Sign-in with Google
        </button>
      </div>
    </div>
  </header>