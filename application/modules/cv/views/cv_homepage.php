
<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
   
<!--[if lt IE 9]>
	<script src="http://mycode.id/wp-content/themes/divergent/js/html5.js" type="text/javascript"></script>
<![endif]-->

<title>Kodein Indonesia</title>
<style type="text/css"> body,p,input,textarea {font-family: ralewayregular;font-weight: normal;}h1,h2,h3,h4,h5,h6,strong,label,.tooltipster-content,.cv-table-left,.cv-button,.skillbar,.cv-resume-title p, .cvfilters li,#home-slide-title span,#home-title p,blockquote .cite,.nav-numbers li a,.meta,.page-date,.cv-box-title,.cv-readmore,input[type="submit"] {font-family: ralewaybold;font-weight: normal;}body,p,input[type="text"], input[type="email"], input[type="number"], input[type="date"], input[type="password"], textarea,.cv-button,table,.widget_nav_menu div ul ul li a,#wp-calendar tfoot #prev,#wp-calendar tfoot #next,#wp-calendar caption, .cv-submenu ul ul li a, cv-table .cv-table-text,.skillbar-title span,.skill-bar-percent,.tooltipster-gototop .tooltipster-content {font-size: 16px;}h1 {font-size: 44px;}h2 {font-size: 38px;}h3 {font-size: 30px;}h4 {font-size: 26px;}h5 {font-size: 22px;}h6,.widget_nav_menu div ul li a,.cv-submenu ul li a,.cv-table li,.cv-resume-title p,.cvfilters li,.tooltipster-dark .tooltipster-content,.tooltipster-light .tooltipster-content,.tooltipster-red .tooltipster-content,.accordion-header,.blogcontainer .postdate, .page-date,.resp-tabs-list li,h2.resp-accordion,.cv-box-title {font-size: 18px;} blockquote p {font-size: 20px;}#home-title h1 span, #home-slide-title span {font-size: 60px;}#home-title p{font-size: 30px;}@media only screen and (max-width:1170px) {#home-title h1 span,#home-slide-title span {font-size: 44px;}#home-title p {font-size: 24px;}}@media only screen and (max-width: 425) {#home-title h1 span,#home-slide-title span {font-size: 38px;}}@media only screen and (max-width: 640px) {#home-title h1 span,#home-slide-title span {font-size: 34px;}h1{font-size: 38px;}h2 {font-size: 34px;}h3 {font-size: 28px;}h4 {font-size: 24px;}blockquote p {font-size: 18px;} }@media only screen and (max-width: 480px) {#home-title h1 span,#home-slide-title span {font-size: 30px;}#home-title p {font-size: 18px;}h1 {font-size: 32px;}h2 {font-size: 28px;}h3 {font-size: 24px;}h4 {font-size: 22px;}h5 {font-size: 20px;}h6,.cv-resume-title p,.cvfilters li,.accordion-header,.page-date,.resp-tabs-list li,h2.resp-accordion,.cv-box-title,blockquote p,.cv-submenu ul li a,.cv-table li,.blogcontainer .postdate {font-size: 16px;}}@media only screen and (max-height: 20em) {#home-title h1 span,#home-slide-title span {font-size: 26px;}#home-title p {font-size: 16px;} }body {background-color: #ffffff;color: #949494;}h1,h2,h3,h4,h5,h6 {color: #222222;}h1.border:after,h2.border:after,h3.border:after,h4.border:after,h5.border:after,h6.border:after {background-color: #de3926;}p {color: #949494;}a {color: #222222;}a:hover {color:#de3926;}.label {background-color: #f3f3f3;border-left: 3px solid #de3926;}blockquote, pre {background: #f3f3f3;}blockquote:before {background-color: #de3926;border:5px solid #ffffff;color:#ffffff;}hr {background-color: #f3f3f3;}.floor {background-color: #ffffff;}.cv-logo img{max-width:400px;}input,textarea {background-color: #ffffff;border: 1px solid #f3f3f3;color: #949494;}input:focus,textarea:focus {background-color: #f3f3f3;color: #222222;}.cv-button {background-color: #ffffff;border: 3px solid #222222;color: #222222;}.cv-button.primary {background-color: #222222;color:#ffffff;}input[type="submit"] {border: 3px solid #222222;background-color: #222222;color:#ffffff;}.cv-button:hover,input[type="submit"]:hover {background-color: #de3926;border: 3px solid #de3926;color:#ffffff;}#cv-sidebar input,#cv-sidebar textarea {color: #949494;}#cv-sidebar input:focus,#cv-sidebar textarea:focus {color: #ffffff;}#cv-sidebar .cv-button {color: #ffffff !important;}#cv-sidebar .cv-button:hover {background-color: #222222;}.searchbox .cv-button {border-left:1px solid #333333 !important;}.searchbox .cv-button:hover {border-left:1px solid #222222 !important;}div.wpcf7-mail-sent-ok,div.wpcf7-mail-sent-ng,div.wpcf7-spam-blocked,div.wpcf7-validation-errors {background-color: #f3f3f3;} #site-error{background-color: #222222; }#site-error h3{color: #ffffff;}#site-loading {background: url('<?php echo base_url('images/kodein/loading.gif') ?>') no-repeat scroll center center #222222;} .cv-page-content {border-bottom: 50px solid #ffffff;}#cv-page-right {background-color: #ffffff;}#cv-menu{background-color: #222222;}#cv-main-menu ul li a {color: #ffffff;}#cv-main-menu ul li.cv-menu-icon a {background: #de3926;}#cv-sidebar {background-color: #333333;}#cv-sidebar h1, #cv-sidebar h2, #cv-sidebar h3, #cv-sidebar h4, #cv-sidebar h5, #cv-sidebar h6{color:#ffffff;}#cv-sidebar, #cv-sidebar p{color:#949494;}.cv-panel-widget a{color:#ffffff;}.cv-panel-widget a:hover{color:#de3926;}.widget_recent_entries ul li a,.widget_categories ul li a,.widget_recent_comments ul li a,.widget_pages ul li a,.widget_meta ul li a,.widget_archive ul li a,.widget_recent-posts ul li a,.widget_rss ul li a,#recentcomments a {color: #949494;}.widget_recent_entries ul li a:hover,.widget_categories ul li a:hover,.widget_recent_comments ul li a:hover,.widget_pages ul li a:hover,.widget_meta ul li a:hover,.widget_archive ul li a:hover,.widget_archives ul li a:hover,.widget_recent-posts ul li a:hover,.widget_rss ul li a:hover,.recentcomments span a:hover{color: #ffffff;}#cv-sidebar .tagcloud a,#cv-sidebar a[class^="tag"] {color: #ffffff;}#cv-sidebar .tagcloud a:hover,#cv-sidebar a[class^="tag"]:hover{color: #ffffff;background-color: #de3926;}a.cv-sidebar-post-title{color: #949494;}a.cv-sidebar-post-title:hover{color: #ffffff;}.cv-sidebar-posts li img:hover {border:3px solid #ffffff;}.widget_nav_menu div ul li a {color: #949494;}.widget_nav_menu div ul li a:hover {color: #ffffff;}.widget_nav_menu div ul ul a{color:#949494;}.widget_nav_menu div ul > li > a.cvdropdown2 {color: #ffffff;}.cv-submenu ul li a {color: #949494;}.cv-submenu ul li a:hover {color: #ffffff;}.cv-submenu ul ul a{color:#949494;}.cv-submenu ul > li > a.cvdropdown2 {color: #ffffff;}.cv-flickr-box li img:hover {border:3px solid #ffffff;}#home-title h1 span{color:#ffffff;background-color: #222222;}#home-slide-title span, #home-title h1 .mobile-title{color:#ffffff;background-color: #de3926;}#home-title p{color: #222222;background-color: #ffffff;}#cv-home-social-bar ul li a {color: #222222;border-right: 1px solid #f3f3f3;background-color: #ffffff;}#cv-home-social-bar ul li a:hover {color: #de3926;}.cv-table li {color: #949494;}.cv-table li {border-bottom: 1px solid #f3f3f3;}.cv-box .cv-table li {border-bottom: 1px solid #ffffff;}.cv-table li:first-child {border-top: 1px solid #f3f3f3;}.cv-box .cv-table li:first-child {border-top: 1px solid #ffffff;}.cv-table .cv-table-title {color: #222222;}.cv-icon-container {background-color: #f3f3f3;}.cv-icon-container a {color:#949494;}.cv-icon-container a:before {color:#949494;}.skillbar {background-color: #ffffff;border:1px solid #f3f3f3;}.skillbar-title {color:#949494;}.skillbar-bar {background-color: #f3f3f3;}.skill-bar-percent {color:#949494;}.cv-resume-title {border-bottom: 1px solid #f3f3f3;}.cvgrid li figure figcaption {background-color: #f3f3f3;}.cvfilters li {color: #949494;background-color: #f3f3f3;}.cvfilters li:hover {color:#222222;}.cvfilters li.gridactive {color:#ffffff;background-color: #de3926;}.cvfilters li.gridactive:hover {color:#ffffff;}.cvgrid li figure figcaption .cvgrid-title {color: #949494;}.cvgrid > li > figure > a:after {color:#ffffff;background-color: #de3926;}.dvsquare > a:after {color:#ffffff;}.dvsquare {background-color:#de3926;}.lg-actions .lg-next, .lg-actions .lg-prev {background-color: rgba(34,34,34,0.5);color: #949494;}.lg-actions .lg-next:hover, .lg-actions .lg-prev:hover {color: #ffffff;}.lg-toolbar {background-color: rgba(34,34,34,0.5);}.lg-toolbar .lg-icon {color: #949494;}.lg-toolbar .lg-icon:hover {color: #ffffff;}.lg-sub-html {background-color: rgba(34,34,34,0.5);color: #ffffff;}#lg-counter {color: #949494;}.lg-outer .lg-item {background: url('<?php echo base_url('images/kodein/loading.gif') ?>') no-repeat scroll center center transparent;} .lg-outer .lg-thumb-outer {background-color: #333333;}.lg-outer .lg-toogle-thumb {background-color: #333333;color: #949494;}.lg-outer .lg-toogle-thumb:hover {color: #ffffff;}.lg-progress-bar {background-color: #333333;}.lg-progress-bar .lg-progress {background-color: #de3926;}.lg-backdrop {background-color: #222222;}.tooltipster-light {background: #ffffff;color: #222222;}.tooltipster-dark,.tooltipster-gototop {background: #222222;color: #ffffff;}.tooltipster-red {background: #de3926;color: #ffffff;}.quovolve-nav a {background: #f3f3f3;color: #949494;}.quovolve-nav a:hover {background: #f3f3f3;color:#222222;}.nav-numbers li a:hover {color:#222222;background: #f3f3f3;}.nav-numbers li.active a{color:#ffffff;background: #222222;}.accordion-container {border-top: 1px solid #f3f3f3;}.accordion-header {border-bottom: 1px solid #f3f3f3;}.accordion-header:hover {color:#222222;}.active-header {color:#222222;}.accordion-content {border-bottom: 1px solid #f3f3f3;}.blog-img {background-color: #f3f3f3;}.blog-img .blog-img-caption {margin-bottom: 120px;}.blog-img-caption h4{color:#ffffff;background: #222222;}.without-featured-title {background: #222222;}.without-featured-title h4{color:#ffffff;}.blog-img:hover .blog-img-caption h4, .without-featured-link:hover .without-featured-title{background: #de3926;}.blogcontainer .postdate {background-color: #f3f3f3;}.cv-readmore {color: #949494;}.cv-readmore:hover {color: #ffffff;background-color: #222222;}.blogpager .previous, .blogpager .next{background-color: #f3f3f3;}.blogpager .cv-button {background-color: #f3f3f3;border-color: #f3f3f3;color:#222222;}.blogpager .cv-button:hover {background-color: #f3f3f3;border: 3px solid #f3f3f3;color:#de3926;}.blogmetadata a{color: #949494;}.blogmetadata a:hover{color: #de3926;} .blogmetadata span{color: #ffffff;}.comments_content {background-color: #f3f3f3;}.comments_content:before {border-bottom-color: #f3f3f3 !important;}.reply:before {color: #222222;}.resp-tab-active {border-top: 3px solid #de3926 !important;}.resp-tabs-list li:hover {background-color: #f3f3f3;}.resp-tabs-list li.resp-tab-active {background-color: #f3f3f3;}.resp-tabs-container {background-color: #f3f3f3;}.resp-tab-active {background-color: #f3f3f3;}.resp-vtabs .resp-tabs-list li:hover {background-color: #f3f3f3;border-left: 3px solid #de3926;}.resp-vtabs .resp-tabs-list li.resp-tab-active {background-color: #f3f3f3;border-left: 3px solid #de3926;}h2.resp-tab-active {background-color: #f3f3f3;}@media only screen and (max-width: 640px) {.resp-tab-active {background-color: #de3926 !important;color: #ffffff !important;}}.caption-image img {border:10px solid #f3f3f3;}.caption-image figcaption {background-color:rgba(243,243,243,0.9);}.cv-box.cv-light,.blogmetadata {background-color: #f3f3f3;}.cv-box.cv-dark {background-color: #222222;}.cv-box.cv-red {background-color: #de3926;}.cv-box-title {color:#222222;}.cv-box.cv-dark .cv-box-title,.cv-box.cv-red .cv-box-title{color:#ffffff;}.cv-box.cv-red p{color:#ffffff;}.mb_YTPBar,.mb_YTPBar span.mb_YTPUrl a{color:#ffffff;}.mb_YTPlayer .loading{color:#ffffff;background:#333333;}.inline_YTPlayer{background:#333333;}.mb_YTPBar{background:#333333;}.mb_YTPBar:hover .buttonBar{background:#333333;}.mb_YTPBar .mb_YTPProgress{background:#222222;}.mb_YTPBar .mb_YTPseekbar{background:#de3926;}.mb_YTPBar .simpleSlider{border:1px solid #ffffff;}.mb_YTPBar .level{background-color:#ffffff;}#cv-page-left{background: url('<?php echo base_url('images/kodein/loading2.gif');?>') no-repeat scroll center center #333333;}#footer {background-color: rgba(243,243,243,0.9);}.cv-credits a {color: #949494;}.cv-credits a:hover {color:#222222;}#cv-back-to-top:before {color:#949494;}#cv-back-to-top:hover:before {color:#222222;}.slider-mobile-only {height: 400px;}@media only screen and (max-width: 640px) {.slider-mobile-only {height: 300px;}}@media only screen and (max-width: 480px) {.slider-mobile-only {height: 250px;}}</style><link rel='dns-prefetch' href='//s.w.org' />

		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/svg\/","svgExt":".svg","source":{"concatemoji":"<?php echo base_url('assets/homepage/js/wp-emoji-release.min.js');?>"}};
			!function(a,b,c){function d(a){var c,d,e,f,g,h=b.createElement("canvas"),i=h.getContext&&h.getContext("2d"),j=String.fromCharCode;if(!i||!i.fillText)return!1;switch(i.textBaseline="top",i.font="600 32px Arial",a){case"flag":return i.fillText(j(55356,56806,55356,56826),0,0),!(h.toDataURL().length<3e3)&&(i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,65039,8205,55356,57096),0,0),c=h.toDataURL(),i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,55356,57096),0,0),d=h.toDataURL(),c!==d);case"diversity":return i.fillText(j(55356,57221),0,0),e=i.getImageData(16,16,1,1).data,f=e[0]+","+e[1]+","+e[2]+","+e[3],i.fillText(j(55356,57221,55356,57343),0,0),e=i.getImageData(16,16,1,1).data,g=e[0]+","+e[1]+","+e[2]+","+e[3],f!==g;case"simple":return i.fillText(j(55357,56835),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode8":return i.fillText(j(55356,57135),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode9":return i.fillText(j(55358,56631),0,0),0!==i.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity","unicode9"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='divergent_nerveslider_styles-css'  href='<?php echo base_url('assets/homepage/css/style.css');?>' type='text/css' media='all' />
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/jquery.js');?>'></script>
<!--
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/jquery-migrate.min.js');?>'></script>
-->
<script type='text/javascript'>
/* <![CDATA[ */
var pirateFormsObject = {"errors":""};
/* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/scripts-general.js?ver=4.6.1');?>'></script>
<script id="wpcp_disable_selection" type="text/javascript">
//<![CDATA[
var image_save_msg='You Can Not Save images!';
	var no_menu_msg='Context Menu disabled!';
	var smessage = "Content is protected !!";

function disableEnterKey(e)
{
	if (e.ctrlKey){
     var key;
     if(window.event)
          key = window.event.keyCode;     //IE
     else
          key = e.which;     //firefox (97)
    //if (key != 17) alert(key);
     if (key == 97 || key == 65 || key == 67 || key == 99 || key == 88 || key == 120 || key == 26 || key == 85  || key == 86 || key == 83 || key == 43)
     {
          show_wpcp_message('You are not allowed to copy content or view source');
          return false;
     }else
     	return true;
     }
}

function disable_copy(e)
{	
	var elemtype = e.target.nodeName;
	var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
	elemtype = elemtype.toUpperCase();
	var checker_IMG = '';
	if (elemtype == "IMG" && checker_IMG == 'checked' && e.detail >= 2) {show_wpcp_message(alertMsg_IMG);return false;}
    if (elemtype != "TEXT" && elemtype != "TEXTAREA" && elemtype != "INPUT" && elemtype != "PASSWORD" && elemtype != "SELECT")
	{
		if (smessage !== "" && e.detail == 2)
			show_wpcp_message(smessage);
		
		if (isSafari)
			return true;
		else
			return false;
	}	
}
function disable_copy_ie()
{
	var elemtype = window.event.srcElement.nodeName;
	elemtype = elemtype.toUpperCase();
	if (elemtype == "IMG") {show_wpcp_message(alertMsg_IMG);return false;}
	if (elemtype != "TEXT" && elemtype != "TEXTAREA" && elemtype != "INPUT" && elemtype != "PASSWORD" && elemtype != "SELECT")
	{
		//alert(navigator.userAgent.indexOf('MSIE'));
			//if (smessage !== "") show_wpcp_message(smessage);
		return false;
	}
}	
function reEnable()
{
	return true;
}
document.onkeydown = disableEnterKey;
document.onselectstart = disable_copy_ie;
if(navigator.userAgent.indexOf('MSIE')==-1)
{
	document.onmousedown = disable_copy;
	document.onclick = reEnable;
}
function disableSelection(target)
{
    //For IE This code will work
    if (typeof target.onselectstart!="undefined")
    target.onselectstart = disable_copy_ie;
    
    //For Firefox This code will work
    else if (typeof target.style.MozUserSelect!="undefined")
    {target.style.MozUserSelect="none";}
    
    //All other  (ie: Opera) This code will work
    else
    target.onmousedown=function(){return false}
    target.style.cursor = "default";
}
//Calling the JS function directly just after body load
window.onload = function(){disableSelection(document.body);};
//]]>
</script>
	<script id="wpcp_disable_Right_Click" type="text/javascript">
	//<![CDATA[
	document.ondragstart = function() { return false;}
	/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	Disable context menu on images by GreenLava Version 1.0
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */
	    function nocontext(e) {
	       return false;
	    }
	    document.oncontextmenu = nocontext;
	//]]>
	</script>
<style>
.unselectable
{
-moz-user-select:none;
-webkit-user-select:none;
cursor: default;
}
html
{
-webkit-touch-callout: none;
-webkit-user-select: none;
-khtml-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
-webkit-tap-highlight-color: rgba(0,0,0,0);
}
</style>
<script id="wpcp_css_disable_selection" type="text/javascript">
var e = document.getElementsByTagName('body')[0];
if(e)
{
	e.setAttribute('unselectable',on);
}
</script>
		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
</head>
<body class="home page page-id-82 page-template page-template-homepage page-template-homepage-php unselectable">
    <!-- LOADING ANIMATION -->
    <div id="site-loading"></div>
    <div id="cv-menu">
		<nav id="cv-main-menu">
			<ul>
				<li class="cv-menu-icon">
					<a href="#" class="cv-menu-button fa fa-bars">Menu</a>
				</li>
				<li class="links-to-floor-li cv-active" data-id="1" data-slug="home">
					<a href="#home" class="fa fa-home tooltip-menu" title="HOME">HOME</a>
				</li>
				<li class="links-to-floor-li" data-id="2" data-slug="about">
					<a href="#about" class="fa fa-user tooltip-menu" title="ABOUT">ABOUT</a>
				</li>
				<li class="links-to-floor-li" data-id="3" data-slug="resume">
					<a href="#resume" class="fa fa-graduation-cap tooltip-menu" title="RESUME">RESUME</a>
				</li>
				<li class="links-to-floor-li" data-id="4" data-slug="portfolio">
					<a href="#portfolio" class="fa fa-briefcase tooltip-menu" title="PORTFOLIO">PORTFOLIO</a>
				</li>
				<li class="links-to-floor-li" data-id="5" data-slug="contact">
					<a href="#contact" class="fa fa-paper-plane tooltip-menu" title="CONTACT">CONTACT</a>
				</li>
			</ul>
		</nav>
	</div>    <!-- LEFT SLIDER -->
    <div class="cv-left-slider">
        <div id="cv-left-slider">
            <!-- 1. SLIDE -->
            <div>
                <img src="<?php echo base_url('images/kodein/img.jpg');?>" alt="">
                <div id="home-slide-title">
					<span>Kodein</span>
                </div>
            </div>
            <img src="<?php echo base_url('images/kodein/img3.jpg');?>" alt="">
            <img src="<?php echo base_url('images/kodein/img4.jpg');?>" alt="">
            <img src="<?php echo base_url('images/kodein/image.jpeg');?>" alt="">
            <!-- GOOGLE MAP -->
            <div class="google-map-container">
                <div id="google-map51" class="google-map"></div>
            </div>
		</div>
    </div>
    <!-- PAGES -->
    <div id="ascensorBuilding">
        <section class="floor floor-1">
            <div id="home-image">
                <div id="home-title">
                    <h1>
                        <span class="mobile-title">Kodein</span>
                        <span>Indonesia</span>
                    </h1>
                    <p>Web Developer</p>
                </div>
                <!-- SOCIAL ICONS -->
                <div id="cv-home-social-bar-container">
					<nav id="cv-home-social-bar">
                        <ul>
                            <li>
								<a href="#" class="fa fa-twitter tooltip-social" title="Twitter">Twitter</a>
                            </li>
                            <li>
                                <a href="#" class="fa fa-vimeo tooltip-social" title="Youtube">Youtube</a>
                            </li>
                                                    </ul>
                    </nav>
                </div>
            </div>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    "use strict";
                    jQuery('#home-image').backstretch("<?php echo base_url('images/kodein/img2.jpg');?>");
                });
            </script>
        </section>
        <section class="floor floor-2">
            <div class="img-mobile-only">
                <img src="<?php echo base_url('images/kodein/img3.jpg');?>" alt="" />
            </div>
            <div class="cv-page-content">
                <h2 class="border">ABOUT ME</h2>
				<p align=justify>Kodein.id adalah website personal yang dikelola oleh Deni Aditiya. Kodein memiliki arti mengkodekan atau membuat aplikasi. Website ini adalah wadah buat saya dalam berkarya. Membantu memecahkan masalah dengan membuat aplikasi sesuai bidang yang saya tekuni yaitu pemprograman web</p>
				<!--
				<ul class="cv-table">
					<li><div class="cv-table-left"><i class="cv-icon fa fa-user"></i>Date of birth</div><div class="cv-table-right">April 28th 1980</div></li>
					<li><div class="cv-table-left"><i class="cv-icon fa fa-language"></i>Languages</div><div class="cv-table-right">English, Bugies</div></li>
					<li><div class="cv-table-left"><i class="cv-icon fa fa-futbol-o"></i>Hobbies</div><div class="cv-table-right">Futsal, Muathai, Yoga</div></li>
					<li><div class="cv-table-left"><i class="cv-icon fa fa-external-link-square"></i>Website</div><div class="cv-table-right">www.faizone.net</div></li>
				</ul>
				
				<p>Senserit sed commodo. Ubi nescius a iudicem, non veniam si amet a tempor ad fabulas id ipsum excepteur relinqueret non fore commodo quibusdam, incidi imitaren do mentitum.</p>
				<p>Et enim offendit ingeniis. Dolor probant senserit si mandaremus do nulla laborum, hic aute iudicem expetendis id cupidatat tamen iudicem proident ut eram arbitror aut veniam enim, nostrud.</p>
				<a href="#contact" class="cv-button go-to-floor primary" data-id="5" data-slug="contact">Hire Me</a>
				<a href="#" class="cv-button ">Download Resume</a>
				<hr />-->
				<h2 class="border">WHAT CAN I DO</h2>
				<div class="cv-main-icon-container">
					<div class="cv-icon-block">
						<div class="cv-icon-container">
							<a href="#" class="fa fa-gears">Analis</a>
						</div>
						<div class="cv-icon-text">
							<h4>System Analyst</h4>
							<p>Melakukan analisa kebutuhan user dan merumuskannya menjadi spesifikasi kebutuhan sistem</p>
						</div>
					</div>
					<div class="cv-icon-block">
						<div class="cv-icon-container">
							<a href="#" class="fa fa-paint-brush">design</a>
						</div>
						<div class="cv-icon-text"><h4>System Design</h4>
							<p>Mengubah hasil analisa menjadi desain permodelan berorientasi objek dengan menggunakan tools Unified Modeling Language (UML)</p>
						</div>
					</div>
					<div class="cv-icon-block">
						<div class="cv-icon-container">
							<a href="#" class="fa fa-code">Coding</a>
						</div>
						<div class="cv-icon-text">
							<h4>Coding</h4>
							<p>Mengubah hasil rancangan menjadi logika pemprograman web menggunakan framework Code Igniter (CI) dan CSS Bootstrap</p>
						</div>
					</div>
					<div class="cv-icon-block">
						<div class="cv-icon-container">
							<a href="#" class="fa fa-cloud-upload">Implementasi</a>
						</div>
						<div class="cv-icon-text">
							<h4>Implement & Maintain</h4>
							<p>Melakukan pemasangan program yang sudah dihasilkan kedalam server dan memastikan program berjalan dengan baik</p>
						</div>
					</div>
				</div>
				<hr />
				<h2 class="border">Testimonials</h2>
				<div id="testimonials1784708694">
					<div>
						<blockquote>
							<p>Fugiat qui se amet nostrud eu fore imitar quam tamen quam, nostrud eram expet non ipsum possumus se relinq ut ut possumus.</p>
							<p class="cite">John Doe</p>
						</blockquote>
					</div>
					<div>
						<blockquote>
							<p>Offendit graviterq ne quem ea excepteur o lorem, amet non quibusdam ut expeten ad nostrud, laboris ita esse, an arbitror.</p>
							<p class="cite">Walter White</p>
						</blockquote>
					</div>
					<div>
						<blockquote>
							<p>Doctrina institue an quibusdam, possumus quid occaecat senserit, a cillum cernantur mandaremus aut ita quae varias.</p>
							<p class="cite">Mary Jennings</p>
						</blockquote>
					</div>
					<div>
						<blockquote>
							<p>Nulla ita quamquam non fugiat est possumus quem a admodum exquisita, ut esse officia, quorum expetendis o tempor, cernantur.</p>
							<p class="cite">Michael Stone</p>
						</blockquote>
					</div>
				</div>

				<script type="text/javascript">
					jQuery(document).ready(function() {
						jQuery('#testimonials1784708694').quovolver({
							transitionSpeed: 200,
							autoPlay: false,
							autoPlaySpeed: 4000,
							equalHeight: false,
							navPosition: 'below',
							navPrev: true,
							navNext: true,
							navPrevText:"<i class='fa fa-chevron-left'></i>",
							navNextText:"<i class='fa fa-chevron-right'></i>",
							navNum: true    });   
					});
				</script>
            </div>
        </section>
        <section class="floor floor-3">
            <div class="img-mobile-only">
                <img src="<?php echo base_url('images/kodein/img4.jpg');?>" alt="" />
            </div>
			
            <div class="cv-page-content">
			<!--
                <h2 class="border">EXPERIENCE</h2>
				<div class="cv-resume-box"><div class="cv-resume-title"><h5>Senior Developer - Google Inc</h5><p>2010 to present</p></div>
					<p>Nescius malis eram si malis, elit de officia, quibusdam summis pariatur appellat se litteris ad legam, irure ubi offendit do te irure nescius comprehenderit, mandaremus eram ullamco e eu esse malis in officia. Quae vidisse expetendis se quem litteris ubi despica.</p>
				</div>
				<div class="cv-resume-box"><div class="cv-resume-title"><h5>Art Director - Designs Ltd</h5><p>2008-2010</p></div>
					<p>Nescius malis eram si malis, elit de officia, quibusdam summis pariatur appellat se litteris ad legam, irure ubi offendit do te irure nescius comprehenderit, mandaremus eram ullamco e eu esse malis in officia. Quae vidisse expetendis se quem litteris ubi despica.</p>
				</div>
				<div class="cv-resume-box"><div class="cv-resume-title"><h5>Web Designer - Web Agency</h5><p>2005-2008</p></div>
					<p>Nescius malis eram si malis, elit de officia, quibusdam summis pariatur appellat se litteris ad legam, irure ubi offendit do te irure nescius comprehenderit, mandaremus eram ullamco e eu esse malis in officia. Quae vidisse expetendis se quem litteris ubi despica.</p>
				</div>
				<hr />
				<h2 class="border">EDUCATION</h2>
				<div class="cv-resume-box"><div class="cv-resume-title"><h5>The University of Texas</h5><p>1997-2002</p></div>
					<p>Nescius malis eram si malis, elit de officia, quibusdam summis pariatur appellat se litteris ad legam, irure ubi offendit do te irure nescius comprehenderit, mandaremus eram ullamco e eu esse malis in officia. Quae vidisse expetendis se quem litteris ubi despica.</p>
				</div>
				<div class="cv-resume-box"><div class="cv-resume-title"><h5>Institue of Design</h5><p>1994-1997</p></div>
					<p>Nescius malis eram si malis, elit de officia, quibusdam summis pariatur appellat se litteris ad legam, irure ubi offendit do te irure nescius comprehenderit, mandaremus eram ullamco e eu esse malis in officia. Quae vidisse expetendis se quem litteris ubi despica.</p>
				</div>
				<hr />-->
				<h2 class="border">SKILLS</h2>
				<p>Minim singulis pariatur. De eram exercitation, cillum admodum non expetendis. Te qui sunt mandaremus.</p>
				<div class="skillbar" data-percent="75%">
					<div class="skillbar-title">
						<span>Unified Modelling Language</span>
					</div>
					<div class="skillbar-bar"></div>
					<div class="skill-bar-percent">75%</div>
				</div>
				<div class="skillbar" data-percent="70%">
					<div class="skillbar-title">
						<span>HTML5</span>
					</div>
					<div class="skillbar-bar"></div>
					<div class="skill-bar-percent">70%</div>
				</div>
				<div class="skillbar" data-percent="90%">
					<div class="skillbar-title">
						<span>CSS Bootstrap</span>
					</div>
					<div class="skillbar-bar"></div>
					<div class="skill-bar-percent">90%</div>
				</div>
				<div class="skillbar" data-percent="70%">
					<div class="skillbar-title">
						<span>jQuery & Ajax</span>
					</div>
					<div class="skillbar-bar"></div>
					<div class="skill-bar-percent">70%</div>
				</div>
				<div class="skillbar" data-percent="80%">
					<div class="skillbar-title">
						<span>PHP</span>
					</div>
					<div class="skillbar-bar"></div>
					<div class="skill-bar-percent">80%</div>
				</div>
				<div class="skillbar" data-percent="75%">
					<div class="skillbar-title">
						<span>SEO</span>
					</div>
					<div class="skillbar-bar"></div>
					<div class="skill-bar-percent">75%</div>
				</div>
				<div class="skillbar" data-percent="80%">
					<div class="skillbar-title">
						<span>Code Igniter 3</span>
					</div>
					<div class="skillbar-bar"></div>
					<div class="skill-bar-percent">80%</div>
				</div>
            </div>
        </section>
        <section class="floor floor-4">
            <div class="img-mobile-only">
                <img src="<?php echo base_url('images/kodein/image.jpeg');?>" alt="" />
            </div>
            <div class="cv-page-content">
                <h2 class="border">PORTFOLIO</h2>
				<ul id="cvfilters489181717" class="cvfilters">
					<li data-filter="gridall" class="gridactive">All</li>
					<li data-filter="dvfilter19">Booth</li>
					<li data-filter="dvfilter25">Exterior</li>
					<li data-filter="dvfilter24">Graphic Design</li>
					<li data-filter="dvfilter16">Interior</li>
					<li data-filter="dvfilter17">Logo</li>
					<li data-filter="dvfilter23">Product Display</li>
					<li data-filter="dvfilter22">Sign</li>
					<li data-filter="dvfilter20">Stage &amp; Backdrop</li>
					<li data-filter="dvfilter21">Video</li>
				</ul>
				
            </div>
        </section>
                                                <section class="floor floor-5">
                        <div class="mobile-map-container">
                <div id="mobile-map51" class="mobile-map"></div>
            </div>
            <script type="text/javascript">
                    jQuery(document).ready(function(){    
                        jQuery("#google-map51").dvmap({
                            fullscreen: 'google-map51',
                            mobile: 'mobile-map51',
                            latitute: '-6.213010',
                            longitude: '106.629667',
                            mapmarker: '<?php echo base_url('images/kodein/pin.png') ?>',
                            zoom:17,
                            grayscale:true                        });
                    });    
                </script>
                        <div class="cv-page-content">
                <h2 class="border">CONTACT</h2>
<p>Quibusdam noster aut laborum despicationes. Malis appellat o enim tamen. O amet quibusdam. Magna occaecat ex coniunctione, quis in mentitum, multos excepteur offendit, id a dolore illum.</p>
<ul class="cv-table">
<li><div class="cv-table-left"><i class="cv-icon fa fa-envelope"></i>E-mail</div><div class="cv-table-right">hi@kodein.id</div></li>
<li><div class="cv-table-left"><i class="cv-icon fa fa-map"></i>Address</div><div class="cv-table-right">Cikokol, Tangerang ID</div></li>
<li><div class="cv-table-left"><i class="cv-icon fa fa-whatsapp"></i>Whatsapp</div><div class="cv-table-right">+62822 149 149 28</div></li>
</ul>
<p>Amet litteris ingeniis in eiusmod et legam aliquip, anim consequat ubi incididunt ubi eram cernantur adipisicing, ab fore illustriora, amet voluptate.</p>
<hr />
<h2 class="border">GET IN TOUCH</h2>
<p>E fugiat mentitum illustriora, te noster tractavissent. Eu aute occaecat domesticarum id si quem non enim. Ab velit nisi si commodo ne dolor domesticarum quamquam malis officia, et legam laboris ne qui cupidatat qui singulis, fabulas.</p>
<div role="form" class="wpcf7" id="wpcf7-f91-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/#wpcf7-f91-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="91" />
<input type="hidden" name="_wpcf7_version" value="4.5.1" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f91-o1" />
<input type="hidden" name="_wpnonce" value="8233cbceba" />
</div>
<p><strong>Your Name (required)</strong><br />
    <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span> </p>
<p><strong>Your Email (required)</strong><br />
    <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" /></span> </p>
<p><strong>Subject</strong><br />
    <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" /></span> </p>
<p><strong>Your Message</strong><br />
    <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span> </p>
<p style="margin:0px"><input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit" /></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
            </div>
        </section>
                        <!-- FOOTER -->
    <footer id="footer">
        <div class="cv-credits">
                        Copyright © 2016 Kodein Indonesia, All Rights Reserved        </div>
        <!-- BACK TO TOP BUTTON -->
        <div id="cv-back-to-top" class="tooltip-gototop" title="Go to top"></div>
    </footer>
</div>
    <!-- SIDEBAR -->
    <aside id="cv-sidebar">
        <div id="cv-sidebar-inner">
                        <div id="search-2" class="widget_search cv-panel-widget">	<form method="get" class="searchbox" id="searchform" action="/">
		<input type="text" class="searchtext" name="s" id="s" />
		<input type="submit" class="cv-button" name="submit" id="searchsubmit" value=""  />
	</form></div><div id="recent-comments-2" class="widget_recent_comments cv-panel-widget"><div class="cv-sidebar-title"><h5>Recent Comments</h5></div><ul id="recentcomments"></ul></div><div id="categories-2" class="widget_categories cv-panel-widget"><div class="cv-sidebar-title"><h5>Categories</h5></div>		<ul>
<!--
<li class="cat-item cat-item-2"><a href="http://mycode.id/category/another-category/" >Another Category</a>
</li>
	<li class="cat-item cat-item-3"><a href="http://mycode.id/category/category-1/" >Category 1</a>
</li>
	<li class="cat-item cat-item-4"><a href="http://mycode.id/category/category-2/" >Category 2</a>
</li>
	<li class="cat-item cat-item-1"><a href="http://mycode.id/category/uncategorized/" >Uncategorized</a>
</li>
-->
		</ul>
</div>        </div>
    </aside>
	<div id="wpcp-error-message" class="msgmsg-box-wpcp warning-wpcp hideme"><span>error: </span>Content is protected !!</div>
	<script>
	var timeout_result;
	function show_wpcp_message(smessage)
	{
		if (smessage !== "")
			{
			var smessage_text = '<span>Alert: </span>'+smessage;
			document.getElementById("wpcp-error-message").innerHTML = smessage_text;
			document.getElementById("wpcp-error-message").className = "msgmsg-box-wpcp warning-wpcp showme";
			clearTimeout(timeout_result);
			timeout_result = setTimeout(hide_message, 3000);
			}
	}
	function hide_message()
	{
		document.getElementById("wpcp-error-message").className = "msgmsg-box-wpcp warning-wpcp hideme";
	}
	</script>
	<style type="text/css">
	#wpcp-error-message {
	    direction: ltr;
	    text-align: center;
	    transition: opacity 900ms ease 0s;
	    z-index: 99999999;
	}
	.hideme {
    	opacity:0;
    	visibility: hidden;
	}
	.showme {
    	opacity:1;
    	visibility: visible;
	}
	.msgmsg-box-wpcp {
		border-radius: 10px;
		color: #555;
		font-family: Tahoma;
		font-size: 11px;
		margin: 10px;
		padding: 10px 36px;
		position: fixed;
		width: 255px;
		top: 50%;
  		left: 50%;
  		margin-top: -10px;
  		margin-left: -130px;
  		-webkit-box-shadow: 0px 0px 34px 2px rgba(242,191,191,1);
		-moz-box-shadow: 0px 0px 34px 2px rgba(242,191,191,1);
		box-shadow: 0px 0px 34px 2px rgba(242,191,191,1);
	}
	.msgmsg-box-wpcp span {
		font-weight:bold;
		text-transform:uppercase;
	}
	.error-wpcp {		background:#ffecec url('<?php echo base_url('images/kodein/error.png')?>') no-repeat 10px 50%;
		border:1px solid #f5aca6;
	}
	.success {
		background:#e9ffd9 url('<?php echo base_url('images/kodein/success.png')?>') no-repeat 10px 50%;
		border:1px solid #a6ca8a;
	}
	.warning-wpcp {
		background:#ffecec url('<?php echo base_url('images/kodein/warning.png')?>') no-repeat 10px 50%;
		border:1px solid #f5aca6;
	}
	.notice {
		background:#e3f7fc url('<?php echo base_url('images/kodein/notice.png')?>') no-repeat 10px 50%;
		border:1px solid #8ed9f6;
	}
    </style>
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/jquery.form.min.js');?>'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"<?php echo base_url('images/kodein/ajax-loader.gif')?>","recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}},"sending":"Sending ..."};
/* ]]> */
</script>
<!--
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/scripts.js');?>'></script>
-->
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/backstretch.min.js');?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/scrollbar.js');?>'></script>

<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/tooltips.js');?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/core.min.js');?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/widget.min.js');?>'></script>

<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/mouse.min.js');?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/draggable.min.js');?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/droppable.min.js');?>'></script>

<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/effect.min.js');?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/nerveslider.js');?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/ascensor.js');?>'></script>

<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/wookmark.js');?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/tabs.js');?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/lightgallery.js');?>'></script>

<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/accordion.js');?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/quovolver.js');?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/flickr.js');?>'></script>

<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/home-custom.js');?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/wp-embed.min.js');?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/dvmap.js');?>'></script>

<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/gmaps.js');?>'></script>

<script type="text/javascript">jQuery(document).ready(function () {"use strict";jQuery("body").find('.tooltip-menu').tooltipster({theme: 'tooltipster-dark',delay: 0,hideOnClick: true,touchDevices: false,position: 'right',animation: 'swing'});jQuery("body").find('.tooltip-social').tooltipster({theme: 'tooltipster-red',delay: 0,hideOnClick: true,touchDevices: false,position: 'top',animation: 'swing'}); jQuery("body").find('.tooltip-gototop').tooltipster({theme: 'tooltipster-gototop',delay: 0,hideOnClick: true,touchDevices: false,position: 'top',animation: 'grow'});});</script></body>

</html>