<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>CODE &#8211; CODE Indonesia</title>
<style type="text/css"> body,p,input,textarea {font-family: ralewayregular;font-weight: normal;}h1,h2,h3,h4,h5,h6,strong,label,.tooltipster-content,.cv-table-left,.cv-button,.skillbar,.cv-resume-title p, .cvfilters li,#home-slide-title span,#home-title p,blockquote .cite,.nav-numbers li a,.meta,.page-date,.cv-box-title,.cv-readmore,input[type="submit"] {font-family: ralewaybold;font-weight: normal;}body,p,input[type="text"], input[type="email"], input[type="number"], input[type="date"], input[type="password"], textarea,.cv-button,table,.widget_nav_menu div ul ul li a,#wp-calendar tfoot #prev,#wp-calendar tfoot #next,#wp-calendar caption, .cv-submenu ul ul li a, cv-table .cv-table-text,.skillbar-title span,.skill-bar-percent,.tooltipster-gototop .tooltipster-content {font-size: 16px;}h1 {font-size: 44px;}h2 {font-size: 38px;}h3 {font-size: 30px;}h4 {font-size: 26px;}h5 {font-size: 22px;}h6,.widget_nav_menu div ul li a,.cv-submenu ul li a,.cv-table li,.cv-resume-title p,.cvfilters li,.tooltipster-dark .tooltipster-content,.tooltipster-light .tooltipster-content,.tooltipster-red .tooltipster-content,.accordion-header,.blogcontainer .postdate, .page-date,.resp-tabs-list li,h2.resp-accordion,.cv-box-title {font-size: 18px;} blockquote p {font-size: 20px;}#home-title h1 span, #home-slide-title span {font-size: 60px;}#home-title p{font-size: 30px;}@media only screen and (max-width:1170px) {#home-title h1 span,#home-slide-title span {font-size: 44px;}#home-title p {font-size: 24px;}}@media only screen and (max-width: 1024px) {#home-title h1 span,#home-slide-title span {font-size: 38px;}}@media only screen and (max-width: 640px) {#home-title h1 span,#home-slide-title span {font-size: 34px;}h1{font-size: 38px;}h2 {font-size: 34px;}h3 {font-size: 28px;}h4 {font-size: 24px;}blockquote p {font-size: 18px;} }@media only screen and (max-width: 480px) {#home-title h1 span,#home-slide-title span {font-size: 30px;}#home-title p {font-size: 18px;}h1 {font-size: 32px;}h2 {font-size: 28px;}h3 {font-size: 24px;}h4 {font-size: 22px;}h5 {font-size: 20px;}h6,.cv-resume-title p,.cvfilters li,.accordion-header,.page-date,.resp-tabs-list li,h2.resp-accordion,.cv-box-title,blockquote p,.cv-submenu ul li a,.cv-table li,.blogcontainer .postdate {font-size: 16px;}}@media only screen and (max-height: 20em) {#home-title h1 span,#home-slide-title span {font-size: 26px;}#home-title p {font-size: 16px;} }body {background-color: #ffffff;color: #949494;}h1,h2,h3,h4,h5,h6 {color: #222222;}h1.border:after,h2.border:after,h3.border:after,h4.border:after,h5.border:after,h6.border:after {background-color: #de3926;}p {color: #949494;}a {color: #222222;}a:hover {color:#de3926;}.label {background-color: #f3f3f3;border-left: 3px solid #de3926;}blockquote, pre {background: #f3f3f3;}blockquote:before {background-color: #de3926;border:5px solid #ffffff;color:#ffffff;}hr {background-color: #f3f3f3;}.floor {background-color: #ffffff;}.cv-logo img{max-width:400px;}input,textarea {background-color: #ffffff;border: 1px solid #f3f3f3;color: #949494;}input:focus,textarea:focus {background-color: #f3f3f3;color: #222222;}.cv-button {background-color: #ffffff;border: 3px solid #222222;color: #222222;}.cv-button.primary {background-color: #222222;color:#ffffff;}input[type="submit"] {border: 3px solid #222222;background-color: #222222;color:#ffffff;}.cv-button:hover,input[type="submit"]:hover {background-color: #de3926;border: 3px solid #de3926;color:#ffffff;}#cv-sidebar input,#cv-sidebar textarea {color: #949494;}#cv-sidebar input:focus,#cv-sidebar textarea:focus {color: #ffffff;}#cv-sidebar .cv-button {color: #ffffff !important;}#cv-sidebar .cv-button:hover {background-color: #222222;}.searchbox .cv-button {border-left:1px solid #333333 !important;}.searchbox .cv-button:hover {border-left:1px solid #222222 !important;}div.wpcf7-mail-sent-ok,div.wpcf7-mail-sent-ng,div.wpcf7-spam-blocked,div.wpcf7-validation-errors {background-color: #f3f3f3;} #site-error{background-color: #222222; }#site-error h3{color: #ffffff;}#site-loading {background: url('http://mycode.id/wp-content/themes/divergent/images/loading.gif') no-repeat scroll center center #222222;} .cv-page-content {border-bottom: 50px solid #ffffff;}#cv-page-right {background-color: #ffffff;}#cv-menu{background-color: #222222;}#cv-main-menu ul li a {color: #ffffff;}#cv-main-menu ul li.cv-menu-icon a {background: #de3926;}#cv-sidebar {background-color: #333333;}#cv-sidebar h1, #cv-sidebar h2, #cv-sidebar h3, #cv-sidebar h4, #cv-sidebar h5, #cv-sidebar h6{color:#ffffff;}#cv-sidebar, #cv-sidebar p{color:#949494;}.cv-panel-widget a{color:#ffffff;}.cv-panel-widget a:hover{color:#de3926;}.widget_recent_entries ul li a,.widget_categories ul li a,.widget_recent_comments ul li a,.widget_pages ul li a,.widget_meta ul li a,.widget_archive ul li a,.widget_recent-posts ul li a,.widget_rss ul li a,#recentcomments a {color: #949494;}.widget_recent_entries ul li a:hover,.widget_categories ul li a:hover,.widget_recent_comments ul li a:hover,.widget_pages ul li a:hover,.widget_meta ul li a:hover,.widget_archive ul li a:hover,.widget_archives ul li a:hover,.widget_recent-posts ul li a:hover,.widget_rss ul li a:hover,.recentcomments span a:hover{color: #ffffff;}#cv-sidebar .tagcloud a,#cv-sidebar a[class^="tag"] {color: #ffffff;}#cv-sidebar .tagcloud a:hover,#cv-sidebar a[class^="tag"]:hover{color: #ffffff;background-color: #de3926;}a.cv-sidebar-post-title{color: #949494;}a.cv-sidebar-post-title:hover{color: #ffffff;}.cv-sidebar-posts li img:hover {border:3px solid #ffffff;}.widget_nav_menu div ul li a {color: #949494;}.widget_nav_menu div ul li a:hover {color: #ffffff;}.widget_nav_menu div ul ul a{color:#949494;}.widget_nav_menu div ul > li > a.cvdropdown2 {color: #ffffff;}.cv-submenu ul li a {color: #949494;}.cv-submenu ul li a:hover {color: #ffffff;}.cv-submenu ul ul a{color:#949494;}.cv-submenu ul > li > a.cvdropdown2 {color: #ffffff;}.cv-flickr-box li img:hover {border:3px solid #ffffff;}#home-title h1 span{color:#ffffff;background-color: #222222;}#home-slide-title span, #home-title h1 .mobile-title{color:#ffffff;background-color: #de3926;}#home-title p{color: #222222;background-color: #ffffff;}#cv-home-social-bar ul li a {color: #222222;border-right: 1px solid #f3f3f3;background-color: #ffffff;}#cv-home-social-bar ul li a:hover {color: #de3926;}.cv-table li {color: #949494;}.cv-table li {border-bottom: 1px solid #f3f3f3;}.cv-box .cv-table li {border-bottom: 1px solid #ffffff;}.cv-table li:first-child {border-top: 1px solid #f3f3f3;}.cv-box .cv-table li:first-child {border-top: 1px solid #ffffff;}.cv-table .cv-table-title {color: #222222;}.cv-icon-container {background-color: #f3f3f3;}.cv-icon-container a {color:#949494;}.cv-icon-container a:before {color:#949494;}.skillbar {background-color: #ffffff;border:1px solid #f3f3f3;}.skillbar-title {color:#949494;}.skillbar-bar {background-color: #f3f3f3;}.skill-bar-percent {color:#949494;}.cv-resume-title {border-bottom: 1px solid #f3f3f3;}.cvgrid li figure figcaption {background-color: #f3f3f3;}.cvfilters li {color: #949494;background-color: #f3f3f3;}.cvfilters li:hover {color:#222222;}.cvfilters li.gridactive {color:#ffffff;background-color: #de3926;}.cvfilters li.gridactive:hover {color:#ffffff;}.cvgrid li figure figcaption .cvgrid-title {color: #949494;}.cvgrid > li > figure > a:after {color:#ffffff;background-color: #de3926;}.dvsquare > a:after {color:#ffffff;}.dvsquare {background-color:#de3926;}.lg-actions .lg-next, .lg-actions .lg-prev {background-color: rgba(34,34,34,0.5);color: #949494;}.lg-actions .lg-next:hover, .lg-actions .lg-prev:hover {color: #ffffff;}.lg-toolbar {background-color: rgba(34,34,34,0.5);}.lg-toolbar .lg-icon {color: #949494;}.lg-toolbar .lg-icon:hover {color: #ffffff;}.lg-sub-html {background-color: rgba(34,34,34,0.5);color: #ffffff;}#lg-counter {color: #949494;}.lg-outer .lg-item {background: url('http://mycode.id/wp-content/themes/divergent/images/loading.gif') no-repeat scroll center center transparent;} .lg-outer .lg-thumb-outer {background-color: #333333;}.lg-outer .lg-toogle-thumb {background-color: #333333;color: #949494;}.lg-outer .lg-toogle-thumb:hover {color: #ffffff;}.lg-progress-bar {background-color: #333333;}.lg-progress-bar .lg-progress {background-color: #de3926;}.lg-backdrop {background-color: #222222;}.tooltipster-light {background: #ffffff;color: #222222;}.tooltipster-dark,.tooltipster-gototop {background: #222222;color: #ffffff;}.tooltipster-red {background: #de3926;color: #ffffff;}.quovolve-nav a {background: #f3f3f3;color: #949494;}.quovolve-nav a:hover {background: #f3f3f3;color:#222222;}.nav-numbers li a:hover {color:#222222;background: #f3f3f3;}.nav-numbers li.active a{color:#ffffff;background: #222222;}.accordion-container {border-top: 1px solid #f3f3f3;}.accordion-header {border-bottom: 1px solid #f3f3f3;}.accordion-header:hover {color:#222222;}.active-header {color:#222222;}.accordion-content {border-bottom: 1px solid #f3f3f3;}.blog-img {background-color: #f3f3f3;}.blog-img .blog-img-caption {margin-bottom: 120px;}.blog-img-caption h4{color:#ffffff;background: #222222;}.without-featured-title {background: #222222;}.without-featured-title h4{color:#ffffff;}.blog-img:hover .blog-img-caption h4, .without-featured-link:hover .without-featured-title{background: #de3926;}.blogcontainer .postdate {background-color: #f3f3f3;}.cv-readmore {color: #949494;}.cv-readmore:hover {color: #ffffff;background-color: #222222;}.blogpager .previous, .blogpager .next{background-color: #f3f3f3;}.blogpager .cv-button {background-color: #f3f3f3;border-color: #f3f3f3;color:#222222;}.blogpager .cv-button:hover {background-color: #f3f3f3;border: 3px solid #f3f3f3;color:#de3926;}.blogmetadata a{color: #949494;}.blogmetadata a:hover{color: #de3926;} .blogmetadata span{color: #ffffff;}.comments_content {background-color: #f3f3f3;}.comments_content:before {border-bottom-color: #f3f3f3 !important;}.reply:before {color: #222222;}.resp-tab-active {border-top: 3px solid #de3926 !important;}.resp-tabs-list li:hover {background-color: #f3f3f3;}.resp-tabs-list li.resp-tab-active {background-color: #f3f3f3;}.resp-tabs-container {background-color: #f3f3f3;}.resp-tab-active {background-color: #f3f3f3;}.resp-vtabs .resp-tabs-list li:hover {background-color: #f3f3f3;border-left: 3px solid #de3926;}.resp-vtabs .resp-tabs-list li.resp-tab-active {background-color: #f3f3f3;border-left: 3px solid #de3926;}h2.resp-tab-active {background-color: #f3f3f3;}@media only screen and (max-width: 640px) {.resp-tab-active {background-color: #de3926 !important;color: #ffffff !important;}}.caption-image img {border:10px solid #f3f3f3;}.caption-image figcaption {background-color:rgba(243,243,243,0.9);}.cv-box.cv-light,.blogmetadata {background-color: #f3f3f3;}.cv-box.cv-dark {background-color: #222222;}.cv-box.cv-red {background-color: #de3926;}.cv-box-title {color:#222222;}.cv-box.cv-dark .cv-box-title,.cv-box.cv-red .cv-box-title{color:#ffffff;}.cv-box.cv-red p{color:#ffffff;}.mb_YTPBar,.mb_YTPBar span.mb_YTPUrl a{color:#ffffff;}.mb_YTPlayer .loading{color:#ffffff;background:#333333;}.inline_YTPlayer{background:#333333;}.mb_YTPBar{background:#333333;}.mb_YTPBar:hover .buttonBar{background:#333333;}.mb_YTPBar .mb_YTPProgress{background:#222222;}.mb_YTPBar .mb_YTPseekbar{background:#de3926;}.mb_YTPBar .simpleSlider{border:1px solid #ffffff;}.mb_YTPBar .level{background-color:#ffffff;}#cv-page-left{background: url('http://mycode.id/wp-content/themes/divergent/images/loading2.gif') no-repeat scroll center center #333333;}#footer {background-color: rgba(243,243,243,0.9);}.cv-credits a {color: #949494;}.cv-credits a:hover {color:#222222;}#cv-back-to-top:before {color:#949494;}#cv-back-to-top:hover:before {color:#222222;}.slider-mobile-only {height: 400px;}@media only screen and (max-width: 640px) {.slider-mobile-only {height: 300px;}}@media only screen and (max-width: 480px) {.slider-mobile-only {height: 250px;}}</style><link rel='dns-prefetch' href='//s.w.org' />

<script type="text/javascript">
	window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/mycode.id\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.6.1"}};
	!function(a,b,c){function d(a){var c,d,e,f,g,h=b.createElement("canvas"),i=h.getContext&&h.getContext("2d"),j=String.fromCharCode;if(!i||!i.fillText)return!1;switch(i.textBaseline="top",i.font="600 32px Arial",a){case"flag":return i.fillText(j(55356,56806,55356,56826),0,0),!(h.toDataURL().length<3e3)&&(i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,65039,8205,55356,57096),0,0),c=h.toDataURL(),i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,55356,57096),0,0),d=h.toDataURL(),c!==d);case"diversity":return i.fillText(j(55356,57221),0,0),e=i.getImageData(16,16,1,1).data,f=e[0]+","+e[1]+","+e[2]+","+e[3],i.fillText(j(55356,57221,55356,57343),0,0),e=i.getImageData(16,16,1,1).data,g=e[0]+","+e[1]+","+e[2]+","+e[3],f!==g;case"simple":return i.fillText(j(55357,56835),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode8":return i.fillText(j(55356,57135),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode9":return i.fillText(j(55358,56631),0,0),0!==i.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity","unicode9"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
body {
    overflow: auto !important;
}
#site-loading, #cv-left-slider{
	display: none !important;
}
.floor {
    padding-left: 5em !important;
}
@media only screen and (max-width: 480px) {
    .floor {
        padding-left: 3.5em !important;
    }
}
</style>
<!--<link href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">-->
<link rel='stylesheet' id='divergent_nerveslider_styles-css'  href='<?php echo base_url('assets/homepage/css/style.css');?>' type='text/css' media='all' />
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/jquery.js?ver=1.12.4');?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/jquery-migrate.min.js?ver=1.4.1');?>'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var pirateFormsObject = {"errors":""};
/* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url('assets/homepage/js/scripts-general.js?ver=4.6.1');?>'></script>
<script id="wpcp_disable_selection" type="text/javascript">
//<![CDATA[
var image_save_msg='You Can Not Save images!';
	var no_menu_msg='Context Menu disabled!';
	var smessage = "Content is protected !!";

function disableEnterKey(e)
{
	if (e.ctrlKey){
     var key;
     if(window.event)
          key = window.event.keyCode;     //IE
     else
          key = e.which;     //firefox (97)
    //if (key != 17) alert(key);
     if (key == 97 || key == 65 || key == 67 || key == 99 || key == 88 || key == 120 || key == 26 || key == 85  || key == 86 || key == 83 || key == 43)
     {
          show_wpcp_message('You are not allowed to copy content or view source');
          return false;
     }else
     	return true;
     }
}

function disable_copy(e)
{	
	var elemtype = e.target.nodeName;
	var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
	elemtype = elemtype.toUpperCase();
	var checker_IMG = '';
	if (elemtype == "IMG" && checker_IMG == 'checked' && e.detail >= 2) {show_wpcp_message(alertMsg_IMG);return false;}
    if (elemtype != "TEXT" && elemtype != "TEXTAREA" && elemtype != "INPUT" && elemtype != "PASSWORD" && elemtype != "SELECT")
	{
		if (smessage !== "" && e.detail == 2)
			show_wpcp_message(smessage);
		
		if (isSafari)
			return true;
		else
			return false;
	}	
}
function disable_copy_ie()
{
	var elemtype = window.event.srcElement.nodeName;
	elemtype = elemtype.toUpperCase();
	if (elemtype == "IMG") {show_wpcp_message(alertMsg_IMG);return false;}
	if (elemtype != "TEXT" && elemtype != "TEXTAREA" && elemtype != "INPUT" && elemtype != "PASSWORD" && elemtype != "SELECT")
	{
		//alert(navigator.userAgent.indexOf('MSIE'));
			//if (smessage !== "") show_wpcp_message(smessage);
		return false;
	}
}	
function reEnable()
{
	return true;
}
document.onkeydown = disableEnterKey;
document.onselectstart = disable_copy_ie;
if(navigator.userAgent.indexOf('MSIE')==-1)
{
	document.onmousedown = disable_copy;
	document.onclick = reEnable;
}
function disableSelection(target)
{
    //For IE This code will work
    if (typeof target.onselectstart!="undefined")
    target.onselectstart = disable_copy_ie;
    
    //For Firefox This code will work
    else if (typeof target.style.MozUserSelect!="undefined")
    {target.style.MozUserSelect="none";}
    
    //All other  (ie: Opera) This code will work
    else
    target.onmousedown=function(){return false}
    target.style.cursor = "default";
}
//Calling the JS function directly just after body load
window.onload = function(){disableSelection(document.body);};
//]]>
</script>
	<script id="wpcp_disable_Right_Click" type="text/javascript">
	//<![CDATA[
	document.ondragstart = function() { return false;}
	/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	Disable context menu on images by GreenLava Version 1.0
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */
	    function nocontext(e) {
	       return false;
	    }
	    document.oncontextmenu = nocontext;
	//]]>
	</script>
<style>
.unselectable
{
-moz-user-select:none;
-webkit-user-select:none;
cursor: default;
}
html
{
-webkit-touch-callout: none;
-webkit-user-select: none;
-khtml-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
-webkit-tap-highlight-color: rgba(0,0,0,0);
}
</style>
<script id="wpcp_css_disable_selection" type="text/javascript">
var e = document.getElementsByTagName('body')[0];
if(e)
{
	e.setAttribute('unselectable',on);
}
</script>
		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
</head>