<!-- FlatFy Theme - Andrea Galanti /-->
<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<meta property='og:title' content='Kodein Indonesia' />			
	<meta property='og:site_name'  content='Kodein.id' />
	<meta property='og:image' content='https://kodein.id/images/kodein/kodein-cover.jpg' />
	<meta property='og:description' content='Program Your Idea' />
	<meta property='og:url' content='https://kodein.id' />
	<meta name='Description' content='Program Your Idea' content='true' />
	<link rel='canonical' href='https://kodein.id' />

    <title>Kodein Indonesia</title>
    <link href="<?php echo base_url(); ?>images/kodein/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
 
    <!-- Custom Google Web Font -->
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    
	
    <!-- Custom CSS-->
    <link href="<?php echo base_url(); ?>assets/css/general.css" rel="stylesheet">
	
	 <!-- Owl-Carousel -->
    <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/owl.theme.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">
	
	<!-- Magnific Popup core CSS file -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css"> 
		<!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/Adminlte.min.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>assets/js/modernizr-2.8.3.min.js"></script>  <!-- Modernizr /-->
	<!--[if IE 9]>
		<script src="assets/js/PIE_IE9.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
		<script src="assets/js/PIE_IE678.js"></script>
	<![endif]-->

	<!--[if lt IE 9]>
		<script src="assets/js/html5shiv.js"></script>
	<![endif]-->
<style>
/* latin */
@font-face {
  font-family: 'Arvo';
  font-style: normal;
  font-weight: 400;
  src: local('Arvo'), url(<?php echo base_url() ?>assets/fonts/J0GYVYTizO1mjpT3aOcSbQ.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}
/* latin */
@font-face {
  font-family: 'Arvo';
  font-style: normal;
  font-weight: 700;
  src: local('Arvo Bold'), local('Arvo-Bold'), url(<?php echo base_url() ?>assets/fonts/Ya11CJGBCnKoJHvb8B2EOw.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}
.navbar-toggle {
    border: none;
    color: #34495e;
    margin: 0 0 0 21px;
    padding: 0 21px;
    height: 53px;
    line-height: 53px;
}
.profile-user-img {
	position: relative;
	top:-90px;
	width:160px;
	background: #efefef;
	border:none;
}
.profile-username {
	margin-top: -70px;
}
.profile{
	margin-top: 65px;
}
.box-profile{
	padding:20px;
}
.list-group-item{
	text-align: left;
}
</style>
</head>

<body id="home" style="background-color:#efefef">