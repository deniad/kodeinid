<?php
$this->load->view('header');
?>
  <main class="mdl-layout__content" style="margin-top: 15px">
    <div class="col-xs-12" style="margin-bottom: 30px;" >
      <h2 align=center class="wow bounceInDown" data-wow-delay="1s">Your Digital Financial Assistant</h2>
    </div>
    <div class="col-xs-12" style="margin-top: 10px;">
      <img src="<?=base_url('images/finbook/transaksi.png')?>" class="wow bounceInLeft" width="100%" data-wow-delay="1s">
      <p style="text-align: justify; margin-top:10px" class="wow bounceInRight" data-wow-delay="0.5s">
        Saat ini untuk melakukan pembayaran tagihan, belanja online dan transfer sudah sangat dimudahkan dengan adanya layanan perbankan. Berbagai macam fasilitas disediakan, mulai dari Automatic Teller Machine (ATM), SMS banking, mobile banking, hingga internet banking.
      </p>
    </div>    
    <div class="col-xs-12" style="margin-top: 20px;">
      <img src="<?=base_url('images/finbook/transaksi-2.png')?>" width="100%">
      <p style="text-align: justify; margin-top:10px">
       Semakin banyaknya data transaksi membuat kita kesulitan untuk memonitornya. Fasilitas mutasi transaksi pada internet banking saat ini hanya mampu menampilkan transaksi dalam rentang maksimal 7 hari dengan maksimum periode 1 bulan kebelakang.
      </p>
      <p style="text-align: justify; margin-top:10px">
       Jika melebihi periode tersebut, kita harus mencetak rekening koran dikantor cabang bank. Selain harus meluangkan waktu pada hari dan jam kerja, kita juga dikenakan biaya cetak perlembarnya.
      </p>
      <p style="text-align: justify; margin-top:10px">
       Belum lagi tidak ada fasilitas untuk mencari data transaksi dengan tanggal, nominal atau berita tertentu membuat kita harus memeriksa satu per satu data transaksi rekening.
      </p>
    </div>
    <div class="col-xs-12" style="margin-bottom: 10px;" id="features">
      <h2 align=center>Features</h2>
    </div>
    <div class="col-xs-12" style="margin-top: 10px;text-align: center;">
      <img src="<?=base_url('images/finbook/transaksi-3.png')?>" width="50%"><br>
      <p align=center>Mr. Asoka</p>
    </div>
    <div class="col-xs-12">
      <p style="text-align: justify; margin-top:10px">
        Hai, perkenalkan nama saya asoka. Saya adalah asisten digital yang akan membantu anda memonitor data transaksi perbankan anda. Berikut yang dapat saya lakukan untuk anda.
      </p>
      <h4><b>Dashboard interaktif</b></h4>
      <p align="justify">
        Saya sajikan data perbankan anda dalam bentuk diagram interaktif agar lebih mudah untuk anda.
      </p> 
      <h4><b>Mencari data transaksi</b></h4>
      <p align="justify">
        Mencari data transaksi berdasarkan periode tanggal, nominal, atau berita transaksi menjadi lebih mudah dan cepat.
      </p>   
      <h4><b>Unduh dalam bentuk PDF</b></h4>
      <p align="justify">
        Ingin mencetak data transaksi anda? cukup menggeneratenya dalam bentuk file pdf.
      </p> 
      <h4><b>Notifikasi transaksi</b></h4>
      <p align="justify">
        Sedang menunggu transfer masuk dengan nominal tertentu?. Saya kirimkan notifikasi ke email anda.
      </p> 
    </div>
	
	<div class="col-xs-12" style="margin-bottom: 10px;" id="features">
      <h2 align=center>How It Works</h2>
    </div>
	<div class="col-xs-12">
		<p align="justify">
		Server Finbook akan melakukan penarikan data satu hari mutasi rekening anda setiap harinya dan menyimpannya dalam database finbook.
		Dengan demikian user dapat mencari data transaksi yang sudah disimpan selama apapun. Untuk itu sistem ini memerlukan akun internet banking. Username dan password akan disimpan dalam database finbook yang terenskripsi atau diacak,
		user tidak perlu khawatir karena sistem hanya dapat membaca, karena untuk transaksi finansial dibutuhkan TOKEN.</p>
	</div>
	<div class="col-xs-12" style="margin-bottom: 10px;" id="features">
      <h2 align=center>Pricing</h2>
    </div>
	<div class="col-xs-12">
		<p align="justify">
		Pricing ?? It's Free to use. Life is beneficial to others</p>
	</div>
  </main>  

  <div id="must-signin-snackbar" class="mdl-js-snackbar mdl-snackbar">
     <div class="mdl-snackbar__text"></div>
     <button class="mdl-snackbar__action" type="button"></button>
  </div>
</div>
<!-- modal menu sidebar -->
    <div class="modal side_bar_modal left fade" data-easein="bounceLeftIn" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="min-width:430px;">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header bg-primary" align=center style="padding:5px;">
            <button type="button" class="btn pull-right" data-dismiss="modal" style="background:transparent; font-size:25px; color: white;"><i class="fa fa-bars"></i></button>           
            <h4 class="modal-title pull-left" style="margin-top:2%; margin-left:25px; font-size:20px;">
              MENU
            </h4>
          </div>
          <div class="modal-body" style="max-height:100vh;">
            <div class="row">
              <div class="col-xs-12">
                <img src="<?php echo base_url('images/finbook/transaksi-3.png'); ?>" class="img-rounded" style="width:150px; margin-left:35px;">
              </div>  
            </div> <!-- end row -->
            <div class="row">
                <div class="col-xs-12" style="border-bottom: 1px solid #efefef;">
                  <h4 align=center>Mr. Asoka</h4>
                  <p style="font-style:italic; font-size: small" align=center>
                  "Digital Financial Assistant"
                  </p>
                </div>
              <div class="col-xs-12" style="padding: 0px;">
                <ul class='sort-product__list active' id='sort-product-list'>
                  <a href="#features"><li><i class='fa fa-dashboard'></i> &nbsp features</li></a>
                </ul>
              </div>
            </div> <!-- end row -->
          </div>  <!-- end modal body -->
        </div> <!-- end modal content -->
      </div> <!-- end modal dialog -->
    </div> <!-- end modal -->
<?php
$this->load->view('footer');
?>

<script type='text/javascript'> 
        // Triggers when the auth state change for instance when the user signs-in or signs-out.
        
        finbook.prototype.onAuthStateChanged = function(user) {
          if (user) { // User is signed in!
            // Get profile pic and user's name from the Firebase user object.
            var profilePicUrl = user.photoURL;
            var userName = user.displayName;
            //tempat sign in

            // Set the user's profile pic and name.
            this.userPic.style.backgroundImage = 'url(' + (profilePicUrl || '/images/profile_placeholder.png') + ')';
            this.userName.textContent = userName;

            // Show user's profile and sign-out button.
            this.userName.removeAttribute('hidden');
            this.userPic.removeAttribute('hidden');
            this.signOutButton.removeAttribute('hidden');

            // Hide sign-in button.
            this.signInButton.setAttribute('hidden', 'true');    
            // We load currently existing chant messages.
            //this.loadMessages();
          
              
          name = user.displayName;
          email = user.email;
          photoUrl = user.photoURL;
          uid = user.uid; 
          token=user.refreshToken;

          /*
            console.log("name: "+name);
            console.log("  Provider-specific UID: "+uid);
            console.log("  Email: "+email);
            console.log("  Photo URL: "+photoUrl);
            console.log("  Token: "+token);
            */

            window.location.assign("<?=base_url('fb/adminpage')?>");

          } else { // User is signed out!
            // Hide user's profile and sign-out button.
            //window.location ="fb"; 

            this.userName.setAttribute('hidden', 'true');
            this.userPic.setAttribute('hidden', 'true');
            this.signOutButton.setAttribute('hidden', 'true');    
            // Show sign-in button.
            this.signInButton.removeAttribute('hidden');        
          }
        };

        
  $(function(){    

    new WOW().init();              
    // browser window scroll (in pixels) after which the "back to top" link is shown
    var offset = 300,
      //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
      offset_opacity = 1200,
      //duration of the top scrolling animation (in ms)
      scroll_top_duration = 700,
      //grab the "back to top" link
      $back_to_top = $('.cd-top');

    //hide or show the "back to top" link
    $(window).scroll(function(){
      ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
      if( $(this).scrollTop() > offset_opacity ) { 
        $back_to_top.addClass('cd-fade-out');
      }
    });

    //smooth scroll to top
    $back_to_top.on('click', function(event){
      event.preventDefault();
      $('body,html').animate({
        scrollTop: 0 ,
        }, scroll_top_duration
      );
    });
  }); 

</script>