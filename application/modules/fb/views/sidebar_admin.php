<!-- modal menu sidebar -->
    <div class="modal side_bar_modal left fade" data-easein="bounceLeftIn" style="min-width:430px;">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header bg-primary" align=center style="padding:5px;">
            <button type="button" class="btn pull-right" data-dismiss="modal" style="background:transparent; font-size:25px; color: white;"><i class="fa fa-bars"></i></button>           
            <h4 class="modal-title pull-left" style="margin-top:2%; margin-left:25px; font-size:20px;">
              MENU
            </h4>
          </div>
          <div class="modal-body" style="max-height:100vh;">
            <div class="row">
              <div class="col-xs-12">
                <img src="<?php echo base_url('images/finbook/transaksi-3.png'); ?>" class="img-rounded" style="width:120px; margin-left:35px;">
              </div>  
            </div> <!-- end row -->
            <div class="row">
                <div class="col-xs-12" style="border-bottom: 1px solid #efefef;">
                  <h4 align=center>Mr. Asoka</h4>
                  <p style="font-style:italic; font-size: small;margin-bottom:0px;" align=center>
                  "Digital Financial Assistant"
                  </p>
                </div>
              <div class="col-xs-12" style="padding: 0px;">
                <ul class='sort-product__list active' id='sort-product-list'>
                  <a href="<?=base_url('fb/adminpage');?>"><li><i class='fa fa-dashboard'></i> &nbsp Dashboard</li></a>				  
                </ul>
                  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="margin-bottom:0px;">
                    <div class="panel panel-white">
                      <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#butab" aria-expanded="true" aria-controls="butab" onclick="reload_butab()" style="color:black;">
                            <i class="fa fa-book"></i> &nbsp Buku Tabungan
                          </a>
                        </h4>
                      </div>
                      <div id="butab" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body" id="butab_list" style="padding:0px;">
                        </div>
                      </div>
                    </div>
                  </div>
				  <ul class='sort-product__list active' id='sort-product-list'>
                  <a href="javascript:$('.sub_sidebar').modal('show');"><li><i class='fa fa-gear'></i> &nbsp Pengaturan</li></a>				  
                </ul>
              </div>
            </div> <!-- end row -->
          </div>  <!-- end modal body -->
        </div> <!-- end modal content -->
      </div> <!-- end modal dialog -->
    </div> <!-- end modal -->
	
	<!-- modal menu sidebar 
    <div class="modal sub_sidebar left fade" data-easein="bounceLeftIn" style="min-width:430px;">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header bg-primary" align=center style="padding:5px;">
            <button type="button" class="btn pull-right" data-dismiss="modal" style="background:transparent; font-size:25px; color: white;"><i class="fa fa-bars"></i></button>           
            <h4 class="modal-title title_sub_sidebar pull-left" style="margin-top:2%; margin-left:25px; font-size:20px;">
              MENU
            </h4>
          </div>
          <div class="modal-body" style="max-height:100vh;">
            <div class="row">
              <div class="col-xs-12">
                <img src="<?php echo base_url('images/finbook/transaksi-3.png'); ?>" class="img-rounded" style="width:120px; margin-left:35px;">
              </div>  
            </div> 
            <div class="row">
                <div class="col-xs-12" style="border-bottom: 1px solid #efefef;">
                  <h4 align=center>Mr. Asoka</h4>
                  <p style="font-style:italic; font-size: small;margin-bottom:0px;" align=center>
                  "Digital Financial Assistant"
                  </p>
                </div>
              <div class="col-xs-12" style="padding: 0px;">
                <ul class='sort-product__list active' id='sort-product-list'>
                  <a href="<?=base_url('fb/adminpage');?>"><li><i class='fa fa-dashboard'></i> &nbsp Dashboard</li></a>				  
                </ul>
                  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="margin-bottom:0px;">
                    <div class="panel panel-white">
                      <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#butab" aria-expanded="true" aria-controls="butab" onclick="reload_butab()" style="color:black;">
                            <i class="fa fa-book"></i> &nbsp Buku Tabungan
                          </a>
                        </h4>
                      </div>
                      <div id="butab" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body" id="butab_list" style="padding:0px;">
                        </div>
                      </div>
                    </div>
                  </div>
				  <ul class='sort-product__list active' id='sort-product-list'>
                  <a href="<?=base_url('fb/config');?>"><li><i class='fa fa-gear'></i> &nbsp Pengaturan</li></a>				  
                </ul>
              </div>
            </div> 
          </div> 
        </div> 
      </div> 
    </div> <!-- end modal -->