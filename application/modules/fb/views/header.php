<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Monitor transaksi rekening jadi lebih mudah">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="utf-8">
  <meta property='og:title' content='Digital Financial Assistant' />     
  <meta property='og:site_name'  content='Kodein.id/fb' />
  <meta property='og:image' content='https://kodein.id/images/finbook/transaksi-3.png' />
  <meta property='og:description' content='Monitor transaksi rekening jadi lebih mudah' />
  <meta property='og:url' content='https://kodein.id/fb' />
  <link rel='canonical' href='https://kodein.id/kbank' />
  <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" type='text/css' rel="stylesheet">
  <link href="<?php echo base_url('assets/css/AdminLTE.css');?>" type='text/css' rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
  <title>Finbook</title>

  <!-- Disable tap highlight on IE -->
  <meta name="msapplication-tap-highlight" content="no">

  <!-- Web Application Manifest 
  <link rel="manifest" href="manifest.json">
  -->

  <!-- Add to homescreen for Chrome on Android -->
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="application-name" content="Finbook">
  <meta name="theme-color" content="#303F9F">

  <!-- Add to homescreen for Safari on iOS -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
  <meta name="apple-mobile-web-app-title" content="Finbook">
  <meta name="apple-mobile-web-app-status-bar-style" content="#303F9F">

  <!-- Tile icon for Win8 -->
  <meta name="msapplication-TileColor" content="#3372DF">
  <meta name="msapplication-navbutton-color" content="#303F9F">

  <!-- Material Design Lite -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.orange-indigo.min.css">
  <script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>

  <!-- App Styling -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
  <link rel="stylesheet" href="<?=base_url()?>assets/kbank/main.css">

  <!-- Bootstrap -->
  <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/Adminlte.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">


<style>
@media screen and (min-width: 480px){
.mdl-grid {
    padding: 2px;
}
.header-kbank{
  margin-left: 50px;

}
}
@media screen and (max-width: 760px){
  .mdl-layout__header-row {
    height: 50px;
  }
  .header-kbank{
      /*margin-left: auto;
      margin-right: auto;*/
      margin:auto;
  }
}

.menu-sidebar{
    font-size: 30px;
    background: transparent;
    color: white;

    margin-left: -10px;
    margin-top: auto;
    margin-bottom: auto;
}
body, h2, h4{
  font-family: 'Merriweather', serif;
}
.modal.left.fade .modal-dialog{
  left: -320px;
  -webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
     -moz-transition: opacity 0.3s linear, left 0.3s ease-out;
       -o-transition: opacity 0.3s linear, left 0.3s ease-out;
          transition: opacity 0.3s linear, left 0.3s ease-out;
}

.modal.left.fade.in .modal-dialog{
  left: 0;
}
.modal.left .modal-dialog {
    position: fixed;
    margin: auto;
    height: 100%;
    width:250px;
    max-width:100%;
    -webkit-transform: translate3d(0%, 0, 0);
        -ms-transform: translate3d(0%, 0, 0);
         -o-transform: translate3d(0%, 0, 0);
            transform: translate3d(0%, 0, 0);
  }

  .modal.left .modal-content{
    margin-left:0px;
    height:100%;
  }
  .btn.focus, .btn:focus, .btn:hover {
    color: white;
    text-decoration: none;
}
.cd-top {
      display: inline-block;
      height: 40px;
      width: 40px;
      position: fixed;
      bottom: 40px;
      right: 10px;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.05);
      /* image replacement properties */
      overflow: hidden;
      text-indent: 100%;
      white-space: nowrap;
      background: rgba(232, 98, 86, 0.8) url(<?php echo base_url('images/cd-top-arrow.svg');?>) no-repeat center 50%;
      visibility: hidden;
      opacity: 0;
      -webkit-transition: opacity .3s 0s, visibility 0s .3s;
      -moz-transition: opacity .3s 0s, visibility 0s .3s;
      transition: opacity .3s 0s, visibility 0s .3s;
    }
    .cd-top.cd-is-visible, .cd-top.cd-fade-out, .no-touch .cd-top:hover {
      -webkit-transition: opacity .3s 0s, visibility 0s 0s;
      -moz-transition: opacity .3s 0s, visibility 0s 0s;
      transition: opacity .3s 0s, visibility 0s 0s;
    }
    .cd-top.cd-is-visible {
      /* the button becomes visible */
      visibility: visible;
      opacity: 1;
      margin-bottom: 50px;
      z-index:3;
    }
    .cd-top.cd-fade-out {
      /* if the user keeps scrolling down, the button is out of focus and becomes less visible */
      opacity: .5;
    }
    .no-touch .cd-top:hover {
      background-color: #e86256;
      opacity: 1;
    }
    
    ul, ol {
    padding: 0;
      margin: 0;
    }
    ol, ul {
      list-style: none;
    }
    ul.sort-product__list li {
      padding: 10px;
      color: #333;
      border-bottom: 1px solid #efefef;
      background-color: #fff;
      margin-left: 0px;
      padding-left: 5px;
      position: relative;
      font-size: larger;
      padding-left: 20px;
    }
    ul.sort-product__list li:before {
      width: 10px;
      height: 100%;
      content: "";
      background-color: #fff;
      position: absolute;
      top: 0;
      left: -10px;
      border-bottom: 1px solid #fff;
    }

</style>

</head>
<body>
<div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-header">

  <!-- Header section containing logo -->
  <header class="mdl-layout__header mdl-color-text--white mdl-color--light-blue-700">
    <div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet mdl-grid" style='padding:1px;'>
      <div class="mdl-layout__header-row mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
        <button class="btn pull-left menu-sidebar" data-toggle="modal" data-target=".modal">
                    <i class="fa fa-bars"></i>
        </button>
        <h2 class='header-kbank' style="margin-bottom: auto;
  margin-top: auto;"><i class="fa fa-foursquare"></i><b>inbook</b></h2>
<!--
  <button class="btn pull-right menu-sidebar" data-toggle="modal" data-target=".side_bar_modal">
                    <i class="fa fa-user"></i>
                </button>
-->
      </div>
      <div id="user-container">
        <div hidden id="user-pic"></div>
        <div hidden id="user-name"></div>
        <button hidden id="sign-out" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-color-text--white">
          Sign-out
        </button>
        <button hidden id="sign-in" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-color-text--white">
          <i class="material-icons">account_circle</i>Sign-in with Google
        </button>
      </div>
    </div>
  </header>
  