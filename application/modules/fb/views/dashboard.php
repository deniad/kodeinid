
<?php
$this->load->view('sidebar_admin');
$this->load->view('header');
?>
<input type="hidden" name="uid" value="" id="uid">
<main class="mdl-layout__content" style="margin-top: 15px">
	<div class="col-xs-12" style="margin-bottom: 30px;" >
	      <h2 align=center> Halaman Admin </h2>
	</div>
</main>

<?php
$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/finbook/adminpage.js"></script>
<script>
var butab_status=false;
function reload_butab(){
  if(butab_status==false){
    var uid= $('#uid').val();
    $.ajax({
        url: "<?php echo base_url('fb/butab/butab_list');?>",
        type: "POST",
        data:{uid:uid},
        dataType: 'html',
        success: function(res)
        {
          $('#butab_list').html(res);
        },
        error: function(e) 
        {
          alert(e);
        }           
    });
    butab_status=true;
  }
  
}
</script>