
<?php
$this->load->view('sidebar_admin');
$this->load->view('header');
echo $this->db->last_query();
?>
<link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" type='text/css' rel="stylesheet">
<link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css" type='text/css' rel="stylesheet">
<input type="hidden" name="uid" value="" id="uid">
<main class="mdl-layout__content" style="margin-top: 15px">
	<div class="col-xs-12" style="margin-bottom: 30px;" >
	      <h2 align=center> <?= $nama_butab ?> </h2>
		  
        <?php
        if($dana_masuk!=0 || $dana_keluar!=0){
          $dana_masuk_rp=number_format($dana_masuk,0,'','.');
          $dana_keluar_rp=number_format($dana_keluar,0,'','.');
          echo"
		  <h4 align=center class='text-warning'>Grafik 1 bulan terakhir</h4>
		  <canvas id=\"myChart\" style=\"height:250px;margin-bottom:10px;\"></canvas><br>
          <span class='text-success' style='margin-left:20%'>
            <i class='fa fa-square'></i> &nbsp Dana Masuk: Rp. $dana_masuk_rp
          </span><br>
          <span class='text-danger' style='margin-left:20%'>
            <i class='fa fa-square'></i> &nbsp Dana Keluar: Rp. $dana_keluar_rp
          </span>
          ";
        }else{
          echo "<h4 align=center> Tidak dana masuk dan keluar bulan ini</h4>";
        }
        ?>
        
        <h3 align=center class="text-primary">SALDO <br> Rp <?=$saldo ?> </h3>
        <h5 align=center class="text-warning"><?=$updated ?> </h5><br>
      <table id="table-transaksi" class="table table-bordered dt-responsive wrap" style="width:100%">
            <thead>
            <tr>                
              <th class="text-center">Tanggal</th>
              <th class="text-center">Type</th>
              <th class="text-center">Berita</th>
              <th class="text-center">Nominal (Rp)</th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
          </table>
          Ket:<br> <b class="text-success">CR= Dana Masuk</b> &nbsp <b class="text-danger">CR= Dana Keluar</b>
	</div>
</main>

<?php
$this->load->view('footer');
?>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>    
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script> 
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<!--
<script src="<?php echo base_url(); ?>assets/js/finbook/admin.js"></script>
-->


<script src="<?=base_url('assets')?>/plugins/chartjs/Chart.min.js"></script>
<script>
finbook.prototype.onAuthStateChanged = function(user) {
  if (user) { // User is signed in!
    // Get profile pic and user's name from the Firebase user object.
    var profilePicUrl = user.photoURL;
    var userName = user.displayName;
    //tempat sign in

    // Set the user's profile pic and name.
    this.userPic.style.backgroundImage = 'url(' + (profilePicUrl) + ')';
    this.userName.textContent = userName;

    // Show user's profile and sign-out button.
    this.userName.removeAttribute('hidden');
    this.userPic.removeAttribute('hidden');
    this.signOutButton.removeAttribute('hidden');

    // Hide sign-in button.
    this.signInButton.setAttribute('hidden', 'true');    
    // We load currently existing chant messages.
    //this.loadMessages();
  uid = user.uid; 
    $('#uid').val(uid);
  } else { // User is signed out!
    // Hide user's profile and sign-out button.

    window.location.href="https://kodein.id/fb";

    this.userName.setAttribute('hidden', 'true');
    this.userPic.setAttribute('hidden', 'true');
    this.signOutButton.setAttribute('hidden', 'true');    
    // Show sign-in button.
    this.signInButton.removeAttribute('hidden');        
  }
};


var butab_status=false;
function reload_butab(){
  if(butab_status==false){
    var uid= $('#uid').val();
    $.ajax({
        url: "<?php echo base_url('fb/butab/butab_list/');?>",
        type: "POST",
        data:{uid:uid},
        dataType: 'html',
        success: function(res)
        {
          $('#butab_list').html(res);
        },
        error: function(e) 
        {
          alert(e);
        }           
    });
    butab_status=true;
  }
  
}
$(document).ready(function() {
    tabel_trx = $('#table-transaksi').DataTable({ 
      "responsive": true,
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
     // "order": [], //Initial no order.

      // Load data for the table's content from an Ajax source
      "ajax": {
        "url": "<?php echo site_url("fb/butab/trxbybutab/$id")?>",
        "type": "POST"
      },

      //Set column definition initialisation properties.
	  aoColumnDefs:[
	  {bSortable:false, aTargets:[-2]},
	  { className: 'text-right', "targets": [ -1 ] },
      { className: 'text-center', "targets": [ 0 ] },
	  { responsivePriority: 1, targets: 0 },
      { responsivePriority: 2, targets: 1 },
      { responsivePriority: 3, targets: -1 }
	  ]
    });
} );
$(function () {
    "use strict";
    /*
    var donut = new Morris.Donut({
      element: 'sales-chart',
      resize: true,
      colors: ["#f56954", "#00a65a"],
      data: [
        {label: "Dana Keluar", value: 12},
        {label: "Dana Masuk", value: 30}
      ],
      hideHover: 'auto'
    }); */

    var pieChartCanvas = $("#myChart").get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas);
    var PieData = [
      {
        value: <?=$dana_keluar?>,
        color: "#f56954",
        highlight: "#f56954",
        label: "Dana Keluar"
      },
      {
        value: <?=$dana_masuk?>,
        color: "#00a65a",
        highlight: "#00a65a",
        label: "Dana Masuk"
      }
    ];
    var pieOptions = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke: true,
      //String - The colour of each segment stroke
      segmentStrokeColor: "#fff",
      //Number - The width of each segment stroke
      segmentStrokeWidth: 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps: 100,
      //String - Animation easing effect
      animationEasing: "easeOutBounce",
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);
});
</script>