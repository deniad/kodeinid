<?php 
class Butab_model extends Fb_model {

		
        public function __construct()
        {
                // Call the CI_Model constructor
        	/*
				ketika digunakan online
				1. where tambahkan id_user untuk memastikan
        	*/
                parent::__construct();
        }

        public function butab_byuid($uid){
            $query=$this->db_fb
                            ->select('b.*')
                            ->from('butab b')
                            ->join('fb_user u', 'u.id_user=b.id_user')
                            ->where('u.uid',$uid)
                            ->get();
            return $query;
        }	
        
        
        public function insert($data)
        {   
            $this->db_fb->insert($this->table_name,$data);
        }
        
        public function update($id_ongkir,$data)
        {   
            $this->db_fb->where('id_ongkir',$id_ongkir)->update($this->table_name,$data);
        }

        public function butab_byid($id_butab){
            $query=$this->db_fb
                        ->select('nama_butab, saldo, date_update')
                        ->from('butab')
                        ->where('id_butab',$id_butab)
                        ->get();
            return $query;
        }


        /*
		public function butab_byid($id_butab)
        {	
            $query=$this->db_fb_fb
            			->select('butab.*, bank.nama_bank')
            			->from('butab')
            			->join('bank','bank.id_bank=butab.id_bank')
            			->where('butab.id_butab',$id_butab)
            			->get();
            return $query;
        }

		public function insert_trx($data)
        {	
            $query=$this->cek_trx($data['id_butab'],$data['detail'],$data['nominal'],$data['type']);
        	$cek=$query->num_rows();
        	if($cek==0){
            	$this->db_fb_fb->insert('transaksi',$data);
        	}else{
                foreach ($query->result_array() as $cetak) {
                    if($cetak['tanggal']=='PEND'){
                        $this->update_trx_pend($cetak['id_trx'],$data['tanggal']);
                    }       
                }                
            }
        	//echo $this->db_fb->last_query();
        }

        private function cek_trx($id_butab,$detail,$nominal,$type){
        	$query=$this->db_fb_fb
	        			->where('id_butab',$id_butab)
	        			->where('detail',$detail)
                        ->where('type',$type)
	        			->where('nominal',$nominal)
	        			->get('transaksi');
	        return $query;
        }

        private function update_trx_pend($id_trx,$tanggal){
            $this->db_fb_fb   ->set('tanggal',$tanggal)
                        ->where('id_trx',$id_trx)
                        ->update('transaksi');
        }
		*/
}