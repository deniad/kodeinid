<?php 
class Transaksi_model extends Fb_model {
		
		private $table_name='transaksi';
		
		var $column_order = array('tanggal','type','detail','nominal'); //set column field database for datatable orderable
		var $column_search = array('tanggal','nominal','detail','type'); //set column field database for datatable searchable just firstname , lastname , address are searchable
		var $order = array('date_update' => 'desc'); // default order 
		
		
        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
		
		private function _get_datatables_query($id_butab)
		{
		
			$this->db_fb->select("t.*")
					->from('transaksi t')
					->join('butab b','b.id_butab=t.id_butab')
					->where('b.id_butab',$id_butab);

			$i = 0;
			$s = 0;
			foreach ($this->column_search as $item) // loop column 
			{
				if($_POST['search']['value']) // if datatable send POST for search
				{					
					if($i===0) // first loop
					{
						$this->db_fb->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
						$this->db_fb->like($item, $_POST['search']['value']);
					}
					else
					{
						$this->db_fb->or_like($item, $_POST['search']['value']);
					}
						if(count($this->column_search) - 1 == $i) //last loop
						$this->db_fb->group_end(); //close bracket
				}
					$i++;
			}
		
			if(!isset($_POST['order'])) // here order processing
			{
				$this->db_fb->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			} 
			else if(isset($this->order))
			{
				$order = $this->order;
				$this->db_fb->order_by(key($order), $order[key($order)]);
			}
		}	

		function count_filtered($id_butab)
		{
			$this->_get_datatables_query($id_butab);
			$query = $this->db_fb->get();
			return $query->num_rows();
		}
	
		public function count_all($id_butab)
		{
			$this->db_fb->from($this->table_name)->where('id_butab',$id_butab);
			return $this->db_fb->count_all_results();
		}		
		
		function trx_list($id_butab)
		{
			$this->_get_datatables_query($id_butab);
			if($_POST['length'] != -1)
			$this->db_fb->limit($_POST['length'], $_POST['start']);
			$query = $this->db_fb->get();
			//echo $this->db_fb->last_query();
			return $query->result();
		}	

		public function trx_bulan_ini($id_butab){
			$tgl1=date("1/m/Y", time());
			$masuk=$this->db_fb
						->select("sum(nominal) from transaksi where type='CR' and id_butab='$id_butab' and date_update BETWEEN NOW() - INTERVAL 30 DAY AND NOW()")
						//->select("sum(nominal) from transaksi where type='CR' and id_butab='$id_butab' and date_update <= DATE_SUB(NOW(), INTERVAL 1 MONTH)")
						->get();		
			$masuk1=$masuk->result_array();
			foreach ($masuk1 as $cetak) {
				if($cetak['sum(nominal)']!=''){
					$nominal=$cetak['sum(nominal)'];
				}else{
					$nominal=0;
				}
				$data['dana_masuk']= $nominal;
			}

			$keluar=$this->db_fb
						->select("sum(nominal) from transaksi where type='DB' and id_butab='$id_butab' and date_update BETWEEN NOW() - INTERVAL 30 DAY AND NOW()")
						//->select("sum(nominal) from transaksi where type='DB' and id_butab='$id_butab' and date_update <= DATE_SUB(NOW(), INTERVAL 1 MONTH)")
						->get();
			$keluar1=$keluar->result_array();
			foreach ($keluar1 as $cetak) {
				if($cetak['sum(nominal)']!=''){
					$nominal=$cetak['sum(nominal)'];
				}else{
					$nominal=0;
				}
				$data['dana_keluar']= $nominal;
			}
			return $data;
		}
		
	}