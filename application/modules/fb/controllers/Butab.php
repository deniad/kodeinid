<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Butab extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
		parent::__construct();
		$this->load->model('butab_model');		
		$this->load->model('transaksi_model');				
	}	
	 
	public function index()
	{		
		$this->load->view('dashboard');
	}

	public function butab_list(){
		$uid=$this->input->post('uid');		
		$butab=$this->butab_model->butab_byuid($uid);
		echo "<ul class='list-group' style='margin-bottom:0px'>
			<li class='list-group-item' style='background-color:whitesmoke;color:black;'><i class='fa fa-plus'></i> &nbsp Tambah buku </li>";
		if($butab->num_rows()>0){
			foreach ($butab->result_array() as $cetak) {
				$id_butab=urlencode(base64_encode($cetak['id_butab'].'-'.$uid).'-'.md5($cetak['id_butab'].'-'.$uid));
				$link=base_url("fb/butab/detail/$id_butab");
				echo "<a href='$link'>
				<li class='list-group-item' style='background-color:whitesmoke;color:black;'>$cetak[nama_butab]</li>
				</a>";
			}
		}else{
			echo "
				<li class='list-group-item' style='background-color:whitesmoke'>Tidak ada buku tabungan</li>
				";
		}			
		echo "
		</ul>
		";
	}

	public function detail($id){
		$data['id']=$id;
		$url=explode('-',urldecode($id));
		$base64=base64_decode($url[0]);
		$hash=$url[1];
		if(md5($base64)==$hash){
			$enskrip=explode('-',$base64);
			$id_butab=$enskrip[0];
			$uid=$enskrip[1];
			$butab=$this->butab_model->butab_byid($id_butab);
			$report=$this->transaksi_model->trx_bulan_ini($id_butab);
			$data['dana_masuk']=$report['dana_masuk'];
			$data['dana_keluar']=$report['dana_keluar'];
			foreach($butab->result_array() as $cetak){
				$data['nama_butab']=$cetak['nama_butab'];
				$data['updated']=date("d M Y - H:i", strtotime($cetak['date_update']));
				$data['saldo']=number_format($cetak['saldo'],0,'','.');
			}			
		}else{
			echo "<h3 align=center> Data Salah </h3>";
		}
		$this->load->view('butab',$data);
	}

	public function trxbybutab($id){
		$url=explode('-',urldecode($id));
		$base64=base64_decode($url[0]);
		$hash=$url[1];		
		if(md5($base64)==$hash){
			$enskrip=explode('-',$base64);
			$id_butab=$enskrip[0];

			$list = $this->transaksi_model->trx_list($id_butab);
			$data = array();
			foreach ($list as $trx) {
				if($trx->type=='CR'){
					$class="text-success";
				}else{
					$class="text-danger";
				}

				$detail="<span class='$class'><b>".$trx->type."</b></span>";			
				//$no++;
				$row = array();
				$row[] = $trx->tanggal;
				$row[] = $trx->type;
				$row[] = $trx->detail;
				$row[] = number_format($trx->nominal,0,'','.');			
				$data[] = $row;
			}

			$output = array(
							"draw" => $_POST['draw'],
							"recordsTotal" => $this->transaksi_model->count_all($id_butab),
							"recordsFiltered" => $this->transaksi_model->count_filtered($id_butab),
							"data" => $data,
					);
			//output to json format
			echo json_encode($output);
		}		
	}

	

}