<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fb extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
		parent::__construct();
	}	
	 
	public function index()
	{
		$base_url=base_url();
		$menu="
		<a href='#features' onclick=\"$('.side_bar_modal').modal('hide')\"><li><i class='fa fa-question'></i> &nbsp Features</li></a>
		";
		$data['side_menu']=$menu;
		$this->load->view('homepage',$data);
	}

	public function admin(){
		echo "halaman admin";
	}

	public function login(){
		echo"halaman login";
	}

	public function BCA(){    
       	$this->load->library('ibParser'); // library bank parser    
        $this->load->model('butab_model');
        $id_butab="1";
        $data=$this->butab_model->butab_byid($id_butab);
        foreach ($data->result_array() as $cetak) {
        	$username=base64_decode($cetak["username"]);
			$password=base64_decode($cetak["password"]);
        	$bank=$cetak['nama_bank'];
        	$id_user=$cetak['id_user'];
        }        
        //$data=simpan_BCA($bank,$username,$password,$id_user,$id_butab);

        $data=cek_bca($bank,$username,$password);
	}

	function mandiri(){     
        $this->load->library('ibParser'); // library bank parser
        $bank="Mandiri";
        $username="asokanato1";
        $password="558855";
        $data=cek_mandiri($bank,$username,$password);
    }

}
