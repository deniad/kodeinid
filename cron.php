<?php 


class IbParser
{

    function __construct()
    {
        $this->conf['ip']       = json_decode( file_get_contents( 'http://myjsonip.appspot.com/' ) )->ip;
        $this->conf['time']     = time();
        $this->conf['path']     = dirname( __FILE__ );
    }


    function instantiate( $bank )
    {
        $class = $bank . 'Parser';
        $this->bank = new $class( $this->conf ) or trigger_error( 'Undefined parser: ' . $class, E_USER_ERROR );
    }


    function getBalance( $bank, $username, $password )
    {

        $this->instantiate( $bank );
        $this->bank->login( $username, $password );
        $balance = $this->bank->getBalance();
        $balance = number_format($balance,0,"",".");
        $this->bank->logout();
        return $balance;
    }
/*
    function getBNI( $bank, $username, $password )
    {

        $this->instantiate( $bank );
        $menu=$this->bank->login( $username, $password );
        $balance = $this->bank->getBalance();
        $balance = number_format($balance,0,"",".");
        $this->bank->logout();
        return $balance;
    }
*/
    function getTransactions( $bank, $username, $password )
    {

        $this->instantiate( $bank );
        $this->bank->login( $username, $password );
        $data['transactions'] = $this->bank->getTransactions();
        //$saldo = number_format($this->bank->getBalance(),0,"",".");
        $data['balance'] = $this->bank->getBalance();
        $this->bank->logout();
        return $data;

    }

}




class BCAParser
{
    function __construct( $conf )
    {

        $this->conf = $conf;

        $d          = explode( '|', date( 'Y|m|d|H|i|s', $this->conf['time'] ) );
        $start      = mktime( $d[3], $d[4], $d[5], $d[1], ( $d[2]-3 ), $d[0] );

        $this->post_time['end']['y'] = $d[0];
        $this->post_time['end']['m'] = $d[1];
        $this->post_time['end']['d'] = $d[2];
        $this->post_time['start']['y'] = date( 'Y', $start );
        $this->post_time['start']['m'] = date( 'm', $start );
        $this->post_time['start']['d'] = date( 'd', $start );
    }




    function curlexec()
    {
        curl_setopt( $this->ch, CURLOPT_RETURNTRANSFER, 1 );
        return curl_exec( $this->ch );
    }




    function login( $username, $password )
    {

        $this->ch = curl_init();
        $browser='Mozilla/5.0 (Linux; U; Android 2.3.7; en-us; Nexus One Build/GRK39F) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1';
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt( $this->ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Linux; U; Android 2.3.7; en-us; Nexus One Build/GRK39F) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1' );
        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/login.jsp' );
        curl_setopt( $this->ch, CURLOPT_COOKIEFILE, $this->conf['path'] . '/cookie' );
        curl_setopt( $this->ch, CURLOPT_COOKIEJAR, $this->conf['path'] . '/cookiejar' );

        $this->curlexec();

        $params = implode( '&', array( 'value(user_id)=' . $username, 'value(pswd)=' . $password, 'value(Submit)=LOGIN', 'value(actions)=login', 'value(user_ip)=' . $this->conf['ip'], 'user_ip=' . $this->conf['ip'],'value(browser_info)=' .$browser, 'value(mobile)=false') );

        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/authentication.do' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/login.jsp' );
        curl_setopt( $this->ch, CURLOPT_POSTFIELDS, $params );
        curl_setopt( $this->ch, CURLOPT_POST, 1 );

        $this->curlexec();
    }




    function logout()
    {
        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/authentication.do?value(actions)=logout' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/authentication.do?value(actions)=menu' );
        $this->curlexec();
        return curl_close( $this->ch );
    }




    function getBalance()
    {

        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/nav_bar_indo/account_information_menu.htm' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/authentication.do' );

        $this->curlexec();

        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/balanceinquiry.do' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/accountstmt.do?value(actions)=menu' );

        $src = $this->curlexec();

        $parse = explode( "<td align='right'><font size='1' color='#0000a7'><b>", $src );

        if ( empty( $parse[1] ) )
            return false;

        $parse = explode( '</td>', $parse[1] );

        if ( empty( $parse[0] ) )
            return false;

        $parse = str_replace( ',', '', $parse[0] );

        return ( is_numeric( $parse ) )? $parse: false;

    }




    function getTransactions()
    {

        //curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/accountstmt.do?value(actions)=menu' );
        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/nav_bar_indo/account_information_menu.htm');
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/authentication.do' );

        $this->curlexec();

        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/accountstmt.do?value(actions)=acct_stmt' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/accountstmt.do?value(actions)=menu' );
        
        //curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/accountstmt.do?value(actions)=acct_stmt' );
        //curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/nav_bar_indo/menu_bar.htm');

        //$this->curlexec();

        $params = implode( '&', array( 'r1=1', 'value(D1)=0', 'value(startDt)=' . $this->post_time['start']['d'], 'value(startMt)=' . $this->post_time['start']['m'], 'value(startYr)=' . $this->post_time['start']['y'],'value(endDt)=' . $this->post_time['end']['d'], 'value(endMt)=' . $this->post_time['end']['m'], 'value(endYr)=' . $this->post_time['end']['y'] ) );

        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/accountstmt.do?value(actions)=acctstmtview' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/accountstmt.do?value(actions)=acct_stmt' );
        curl_setopt( $this->ch, CURLOPT_POSTFIELDS, $params );
        curl_setopt( $this->ch, CURLOPT_POST, 1 );

        $src = $this->curlexec();

        $parse = explode( '<table width="100%" class="blue">', $src );

        if ( empty( $parse[1] ) )
            return false;

        $parse = explode( '</table>', $parse[1] );
        $parse = explode( '<tr', $parse[0] );
        $rows = array();

        foreach( $parse as $val )
            if ( substr( $val, 0, 8 ) == ' bgcolor' )
                $rows[] = $val;
        foreach( $rows as $key => $val )
        {
            $rows[$key]     = explode( '</td>', $val );
            $rows[$key][0]  = substr( $rows[$key][0], -5 );
            if ( stristr( $rows[$key][0], 'pend' ) ){
                $rows[$key][0] = 'PEND';
            }else{
                $rows[$key][0]  .='/'.date('Y');
            }
            $detail         = explode( "<td valign='top'>", $rows[$key][1] );
            $rows[$key][2]  = $detail[1];
            $rows[$key][1]  = explode( '<br>', $detail[0] );
            $nominal= explode('.',str_replace( ',', '', $rows[$key][1][count($rows[$key][1])-1] ));
            $rows[$key][3]  = $nominal[0];
            unset( $rows[$key][1][count($rows[$key][1])-1] );
            foreach( $rows[$key][1] as $k => $v )
                $rows[$key][1][$k] = trim( strip_tags( $v ) );
            $rows[$key][1] = implode( " ", $rows[$key][1] );
        }
        //print_r($rows);
        return ( !empty( $rows ) )? $rows: false;

    }
}

class MandiriParser
{
    function __construct( $conf )
    {

        $this->conf = $conf;

        $d          = explode( '|', date( 'Y|m|d|H|i|s', $this->conf['time'] ) );
        $start      = mktime( $d[3], $d[4], $d[5], $d[1], ( $d[2] - 19 ), $d[0] );

        $this->post_time['end']['y'] = $d[0];
        $this->post_time['end']['m'] = $d[1];
        $this->post_time['end']['d'] = $d[2];
        $this->post_time['start']['y'] = date( 'Y', $start );
        $this->post_time['start']['m'] = date( 'm', $start );
        $this->post_time['start']['d'] = date( 'd', $start );
    }




    function curlexec()
    {
        curl_setopt( $this->ch, CURLOPT_RETURNTRANSFER, 1 );
        return curl_exec( $this->ch );
    }




    function login( $username, $password )
    {

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt( $this->ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Linux; U; Android 2.3.7; en-us; Nexus One Build/GRK39F) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1' );
        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/Login.do?action=form' );
        curl_setopt( $this->ch, CURLOPT_COOKIEFILE, $this->conf['path'] . '/cookie' );
        curl_setopt( $this->ch, CURLOPT_COOKIEJAR, $this->conf['path'] . '/cookiejar' );

        $this->curlexec();

        $params = implode( '&', array( 'userID=' . $username, 'password=' . $password, 'action=result') );

        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/Login.do' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/Login.do?action=form');
        curl_setopt( $this->ch, CURLOPT_POSTFIELDS, $params );
        curl_setopt( $this->ch, CURLOPT_POST, 1 );

        $this->curlexec();
    }




    function logout()
    {
        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/Logout.do?action=result' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/common/menu.jsp' );
        $this->curlexec();
        return curl_close( $this->ch );
    }




    function getBalance()
    {

        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/AccountList.do?action=acclist' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/Redirect.do?action=forward' );

        $this->curlexec();

        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/AccountDetail.do?action=result' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/Redirect.do?action=forward' );

        $parse = $this->curlexec();
         $data = explode('<td height="25" width="304" id="accbal">',$parse);
         $data = explode('</td>',$data[1]);
         $data = str_replace(array('Rp.&nbsp;'),'',$data[0]);
         $data = explode(',',$data);
         $saldo = str_replace(array("\t","\r","\n"," ","."),'',$data[0]);

        return $saldo;

    }




    function getTransactions()
    {

        //menu mutasi rekening
        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/TrxHistoryInq.do?action=form' );

        // menu informasi rekening
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/Redirect.do?action=forward' ); 
        
        /*
        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/accountstmt.do?value(actions)=acct_stmt' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/accountstmt.do?value(actions)=menu' );
        */

        $parse=$this->curlexec();

        $data = explode( '<select name="fromAccountID">', $parse );
        $data = explode("<option value=",$data[1]);
        $data = explode('"',$data[2]);
        $fromAccountID=$data[1];
        $data = explode('>',$data[2]);
        $data = explode(' ',$data[1]);
        $norekening=$data[0];
        

        //echo "fromAccountID: $value_rek, $start_day/$start_month/$start_year - $end_day/$end_month/$end_year";
        $params = implode( '&', array( 'action=result', 'fromAccountID='.$fromAccountID , 'searchType=R', 'fromDay=' . $this->post_time['start']['d'], 'fromMonth=' . $this->post_time['start']['m'], 'fromYear=' . $this->post_time['start']['y'],'toDay=' . $this->post_time['end']['d'], 'toMonth=' . $this->post_time['end']['m'], 'toYear=' . $this->post_time['end']['y'] ) );

        //print_r($params);

        // ../TrxHistoryInq.do?action=form

        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/TrxHistoryInq.do' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/Redirect.do?action=forward' );
        curl_setopt( $this->ch, CURLOPT_POSTFIELDS, $params );
        curl_setopt( $this->ch, CURLOPT_POST, 1 );

         $parse = $this->curlexec();
         $data = explode('<table border="0" cellpadding="2" cellspacing="1" width="100%">',$parse);
         $data = explode('</table>',$data[1]);
         $data = explode('<tr height="25">',$data[0]);

         //detail nih
         $jmldata=count($data);

         $rows=array();
         $no=0;
         for($i=1;$i<$jmldata;$i++){
         $data1 = explode('<td height="25" class="tabledata" bgcolor="',$data[$i]); // row
         $rows[$i][1]=str_replace(array('#DDF2FA">','">','</td>'),'',$data1[1]);
         $datadb = explode('<td height="25" class="tabledata" align="right" bgcolor="',$data1[2]);
         $berita = str_replace(array('<br>','</td>'), ' ', $datadb[0]);
         $rows[$i][2]=str_replace(array('#DDF2FA">','">'),'',$berita);
         $data1 = explode('>',$datadb[1]);
         $debet =explode(',', str_replace(array('</td','.'), '', $data1[1]));
         $rows[$i][3]=$debet[0];
         $data1 = explode('>',$datadb[2]);      
         $kredit = explode(',', str_replace(array('</td','.'), '', $data1[1]));
         $rows[$i][4]=$kredit[0];      
        }
        //print_r($data1);
        return ( !empty( $rows ) )? $rows: false;

    }
}

class BNIParser
{
    function __construct( $conf )
    {

        $this->conf = $conf;

        $d          = explode( '|', date( 'Y|m|d|H|i|s', $this->conf['time'] ) );
        $start      = mktime( $d[3], $d[4], $d[5], $d[1], ( $d[2] - 7 ), $d[0] );

        $this->post_time['end']['y'] = $d[0];
        $this->post_time['end']['m'] = $d[1];
        $this->post_time['end']['d'] = $d[2];
        $this->post_time['start']['y'] = date( 'Y', $start );
        $this->post_time['start']['m'] = date( 'm', $start );
        $this->post_time['start']['d'] = date( 'd', $start );
    }




    function curlexec()
    {
        curl_setopt( $this->ch, CURLOPT_RETURNTRANSFER, 1 );
        return curl_exec( $this->ch );
    }




    function login( $username, $password )
    {

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt( $this->ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Linux; U; Android 2.3.7; en-us; Nexus One Build/GRK39F) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1' );
        curl_setopt( $this->ch, CURLOPT_URL, 'https://ibank.bni.co.id/MBAWeb/FMB' );
        curl_setopt( $this->ch, CURLOPT_COOKIEFILE, $this->conf['path'] . '/cookie' );
        curl_setopt( $this->ch, CURLOPT_COOKIEJAR, $this->conf['path'] . '/cookiejar' );

         $parse=$this->curlexec();
         $data = explode('<td id="RetailUser_td"  ><a id="RetailUser" class="lgnaln" href="',$parse);
         $url = explode('"',$data[1]);
         curl_setopt( $this->ch, CURLOPT_URL, $url[0] );
         $parse=$this->curlexec();
         //echo $parse;die;
         $url1= explode('?',$url[0]);
        $params = implode( '&', array( 'Num_Field_Err="Please enter digits only!"','Mand_Field_Err="Mandatory field is empty!"','CorpId=' . $username, 'PassWord=' . $password,'__AUTHENTICATE__=Login', 'CancelPage=HomePage.xml','USER_TYPE=1','MBLocale=bh','language=bh','AUTHENTICATION_REQUEST=True','__JS_ENCRYPT_KEY__=','JavaScriptEnabled=N','deviceID=','machineFingerPrint=','deviceType=','browserType=','uniqueURLStatus=disabled','imc_service_page=SignOnRetRq','Alignment=LEFT','page=SignOnRetRq','locale=en','PageName=Thin_SignOnRetRq.xml','formAction='.$url1,'mConnectUrl=FMB','serviceType=Dynamic') );
       // echo $url[0]."<br>";
       
        
        //echo $params;die;
        curl_setopt( $this->ch, CURLOPT_URL, $url1[0] );
        curl_setopt( $this->ch, CURLOPT_REFERER, $url[0]);
        curl_setopt( $this->ch, CURLOPT_POSTFIELDS, $params );
        curl_setopt( $this->ch, CURLOPT_POST, 1 );

        $parse=$this->curlexec();
        $data = explode('<td id="MBMenuList_td"  ><a id="MBMenuList" class="MainMenuStyle" href="',$parse);
        $data = explode('"',$data[1]);
        $menu_rek=$data[0];
        return $menu_rek;
    }




    function logout()
    {
        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/Logout.do?action=result' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/common/menu.jsp' );
        $this->curlexec();
        return curl_close( $this->ch );
    }




    function getBalance()
    {
        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/AccountList.do?action=acclist' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/Redirect.do?action=forward' );

        $this->curlexec();

        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/AccountDetail.do?action=result' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/Redirect.do?action=forward' );

        $parse = $this->curlexec();
         $data = explode('<td height="25" width="304" id="accbal">',$parse);
         $data = explode('</td>',$data[1]);
         $data = str_replace(array('Rp.&nbsp;'),'',$data[0]);
         $data = explode(',',$data);
         $saldo = str_replace(array("\t","\r","\n"," ","."),'',$data[0]);

        return $saldo;

    }




    function getTransactions()
    {

        //menu mutasi rekening
        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/TrxHistoryInq.do?action=form' );

        // menu informasi rekening
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/Redirect.do?action=forward' ); 
        
        /*
        curl_setopt( $this->ch, CURLOPT_URL, 'https://m.klikbca.com/accountstmt.do?value(actions)=acct_stmt' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://m.klikbca.com/accountstmt.do?value(actions)=menu' );
        */

        $parse=$this->curlexec();

        $data = explode( '<select name="fromAccountID">', $parse );
        $data = explode("<option value=",$data[1]);
        $data = explode('"',$data[2]);
        $fromAccountID=$data[1];
        $data = explode('>',$data[2]);
        $data = explode(' ',$data[1]);
        $norekening=$data[0];
        

        //echo "fromAccountID: $value_rek, $start_day/$start_month/$start_year - $end_day/$end_month/$end_year";
        $params = implode( '&', array( 'action=result', 'fromAccountID='.$fromAccountID , 'searchType=R', 'fromDay=' . $this->post_time['start']['d'], 'fromMonth=' . $this->post_time['start']['m'], 'fromYear=' . $this->post_time['start']['y'],'toDay=' . $this->post_time['end']['d'], 'toMonth=' . $this->post_time['end']['m'], 'toYear=' . $this->post_time['end']['y'] ) );

        //print_r($params);

        // ../TrxHistoryInq.do?action=form

        curl_setopt( $this->ch, CURLOPT_URL, 'https://ib.bankmandiri.co.id/retail/TrxHistoryInq.do' );
        curl_setopt( $this->ch, CURLOPT_REFERER, 'https://ib.bankmandiri.co.id/retail/Redirect.do?action=forward' );
        curl_setopt( $this->ch, CURLOPT_POSTFIELDS, $params );
        curl_setopt( $this->ch, CURLOPT_POST, 1 );

         $parse = $this->curlexec();
         $data = explode('<table border="0" cellpadding="2" cellspacing="1" width="100%">',$parse);
         $data = explode('</table>',$data[1]);
         $data = explode('<tr height="25">',$data[0]);

         //detail nih
         $jmldata=count($data);

         $rows=array();
         $no=0;
         for($i=1;$i<$jmldata;$i++){
         $data1 = explode('<td height="25" class="tabledata" bgcolor="',$data[$i]); // row
         $rows[$i][1]=str_replace(array('#DDF2FA">','">','</td>'),'',$data1[1]);
         $datadb = explode('<td height="25" class="tabledata" align="right" bgcolor="',$data1[2]);
         $berita = str_replace(array('<br>','</td>'), ' ', $datadb[0]);
         $rows[$i][2]=str_replace(array('#DDF2FA">','">'),'',$berita);
         $data1 = explode('>',$datadb[1]);
         $debet =explode(',', str_replace(array('</td','.'), '', $data1[1]));
         $rows[$i][3]=$debet[0];
         $data1 = explode('>',$datadb[2]);      
         $kredit = explode(',', str_replace(array('</td','.'), '', $data1[1]));
         $rows[$i][4]=$kredit[0];      
        }

        return ( !empty( $rows ) )? $rows: false;

    }
}

function cek_BCA($bank,$username,$password,$id_user,$id_butab,$conn){
    $parserbca = new IbParser;
        
    if ( $data = $parserbca->getTransactions( $bank, $username, $password ) )
    {

        // echo '<pre>' . print_r( $transactions, true ) . '</pre>';
        $hasil="<table border=1 cellspacing=2 cellpadding=2>
                    <tr>                          
                        <td align=center>Date</td>
                        <td align=center>Detail</td>
                        <td align=center>Type</td>
                        <td align=center>Nominal</td>
                    </tr>";

        // loop
        $found=0;
        //print_r($data);
    //   if($data['transactions']){
            foreach( $data['transactions'] as $transaction )
            {
                $tanggal    =$transaction[0];
                $detail     =$transaction[1];
                $type       =$transaction[2];
                $nominal    =$transaction[3];
                $trx = array(
                        'tanggal' => $transaction[0], 
                        'detail' => $transaction[1], 
                        'type' => $transaction[2], 
                        'nominal' => $transaction[3],
                        'id_butab' => $id_butab,
                        'id_user' => $id_user,
                        'date_create' => date('Y-m-d h:i:s')
                        );  
                $nominal = number_format($transaction[3],0,"",".");
                $hasil.="
                        <tr>                            
                            <td>$transaction[0]</td>
                            <td>$transaction[1]</td>
                            <td>$transaction[2]</td>
                            <td>$nominal</td>
                        </tr>"; 
                $found++; 
                insert_trx($trx,$conn); 
                //echo $this->db->last_query();     
        }
        if($found<1){
            echo "Transaksi tidak ditemukan";
        }else{
            echo "</br> $found Transaksi ditemukan </br>";
        }
        $hasil.="</table>";
        echo $hasil;
        $saldo_rp=number_format($data['balance'],0,"",".");
        echo "saldo anda saat ini: ".$saldo_rp;
        update_saldo($id_butab, $data['balance'], $conn);
        }
        else{
            echo "Username or password $bank invalid";                          
        }
 //   }
}
function cek_mandiri($bank,$username,$password,$id_user,$id_butab,$conn){
    $parser = new IbParser;
    
    // Ambil transaksi
    if ( $data = $parser->getTransactions( $bank, $username, $password ) )
    {

        $hasil="<table border=1 cellspacing=2 cellpadding=2>
                    <tr>                          
                        <td align=center>Date</td>
                        <td align=center>Detail</td>
                        <td align=center>Type</td>
                        <td align=center>Kredit</td>
                    </tr>";

        // loop
        $found=0;
        foreach( $data['transactions'] as $transaction )
        {
            $debet = $transaction[3];
            $kredit = $transaction[4];

            if($debet!=0){$type="DB";$nominal=$debet;}
            if($kredit!=0){$type="CR";$nominal=$kredit;}
            $trx = array(
                        'tanggal' => $transaction[1], 
                        'detail' => $transaction[2], 
                        'type' => $type, 
                        'nominal' => $nominal,
                        'id_butab' => $id_butab,
                        'id_user' => $id_user
                        ); 
            insert_trx($trx,$conn);
            $nominalrp=number_format($nominal,0,"",".");
                $hasil.="
                        <tr>                            
                            <td>$transaction[1]</td>
                            <td>$transaction[2]</td>
                            <td>$type</td>
                            <td>$nominalrp</td>
                        </tr>";
                $found++;         
        }
        if($found<1){
            echo "Transaksi tidak ditemukan";
        }else{
            echo "</br> $found Transaksi ditemukan </br>";
        }
        $hasil.="</table>";
        echo $hasil;
        $saldo_rp=number_format($data['balance'],0,"",".");
        $saldo = $data['balance'];
        update_saldo($id_butab, $saldo, $conn);
        echo "saldo anda saaat ini: $saldo_rp";
    }

}

function cek_saldo($bank,$user,$pass){    
    $parser = new IbParser;
    $data= $parser->getBalance( $bank, $user, $pass );
    return $data;
}

function cek_BNI($bank,$user,$pass){    
    $parser = new IbParser;
    $data= $parser->getBalance( $bank, 'Deniaditiya28', 'konichiwa28' );
    return $data;
}

function update_trx_pend($id_trx,$tanggal,$conn){
    mysqli_query($conn,"update transaksi set tanggal='$tanggal' where id_trx='$id_trx'");
}

function insert_trx($data,$conn)
{   
    $cek=cek_trx($data['id_butab'],$data['detail'],$data['nominal'],$data['type'],$conn);
    $jml=mysqli_num_rows($cek);
    if($jml==0){
        mysqli_query($conn,"insert into transaksi (tanggal,detail,type, nominal, id_butab, id_user, date_create) values ('$data[tanggal]',' $data[detail]','$data[type]','$data[nominal]','$data[id_butab]','$data[id_user]', now()')");
    }else{
        while ($cetak=mysqli_fetch_assoc($cek)) {
            if($cetak['tanggal']=='PEND'){
                update_trx_pend($cetak['id_trx'],$data['tanggal'],$conn);
            }       
        }                
    }
    //echo $this->db->last_query();
}

function update_saldo($id_butab, $saldo, $conn){
    mysqli_query($conn,"update butab set saldo='$saldo', date_update=now() where id_butab='$id_butab'");
}

function cek_trx($id_butab,$detail,$nominal,$type,$conn){

    $query=mysqli_query($conn,"select * from transaksi where id_butab='$id_butab' and detail=' $detail' and nominal='$nominal' and type='$type'");
    return $query;
}

/*
$servername = "mysql.idhostinger.com";
$username = "u377862564_fb";
$password = "konichiwa";
$database="u377862564_fb";
*/

$servername = "localhost";
$username = "root";
$password = "";
$database="finbook";

// Create connection
$conn = mysqli_connect($servername, $username, $password,$database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
echo "Connected successfully";

$sql="select butab.*, bank.nama_bank from butab join bank on bank.id_bank=butab.id_bank";
$result = mysqli_query($conn, $sql);	

while($cetak = mysqli_fetch_assoc($result)) {
	$username=base64_decode($cetak["username"]);
	$password=base64_decode($cetak["password"]);
	$bank=$cetak["nama_bank"];
	$id_user=$cetak["id_user"];
    $id_butab=$cetak['id_butab'];

    if($bank=='BCA'){
        cek_BCA($bank,$username,$password,$id_user,$id_butab,$conn);
    }
    
    if($bank=='Mandiri'){
        cek_mandiri($bank,$username,$password,$id_user,$id_butab,$conn);
    }  
}



mysqli_close($conn);