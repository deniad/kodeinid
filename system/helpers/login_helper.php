<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
if ( ! function_exists('login'))
{
	
	function timer(){
		$CI = & get_instance();  //get instance, access the CI superobject
		$time=5000; // set 30 menit
		//$this->session->sess_expiration(time()+$time);
		$data_session = array(
			'timeout' => time()+$time
		);				
		$CI->session->set_userdata($data_session);
	}
	function cek_login(){	
		$CI = & get_instance();  //get instance, access the CI superobject
		$timeout=$CI->session->userdata('timeout');
		if(time()<$timeout){
			timer();
			return true;
		}else{		
			$CI->session->set_userdata('timeout');
			return false;
		}
	}	
}